var Harness = Siesta.Harness.Browser.ExtJS;

Harness.configure({
    title       : 'WG Messenger Test Suite',
    loaderPath  : {
    	'Ext': 'http://localhost/messengerci/assets/ext-4.0/src',
		'Ext.ux': 'http://localhost/messengerci/assets/ext-4.0/examples/ux',
		'WG': 'http://localhost/messengerci/assets/app'
    },
    
    preload : [
        "http://localhost/messengerci/assets/css/ext/resources/css/ext-all.css",
        "http://localhost/messengerci/assets/css/ext/ux/statusbar/css/statusbar.css",
        "http://localhost/messengerci/assets/css/ext/tabs/tabs-adv.css",
        "http://localhost/messengerci/assets/css/ext/ux/css/TabScrollerMenu.css",
        "http://localhost/messengerci/assets/css/app.css",
        "http://localhost/messengerci/assets/css/preloader.css",
        
        // Global Var Mockup script!
        "http://localhost/messengerci/assets/siesta/mytest/mockup/global.js",
        
        "http://localhost/messengerci/assets/app-assets/lib/jquery/jquery-1.7.1.min.js",
        "http://localhost/messengerci/assets/app-assets/lib/jquery/jquery.cookie.js",
        "http://localhost/messengerci/assets/app-assets/lib/soundmanager/script/soundmanager2-nodebug-jsmin.js",
        "http://localhost/messengerci/assets/app-assets/lib/strophe/strophe.js",
        "http://localhost/messengerci/assets/app-assets/lib/strophe/wgmessengerstrophe.js",
        "http://localhost/messengerci/assets/ext-4.0/ext-all-debug.js",
        "http://localhost/messengerci/assets/app-assets/lib/ext/examples.js",
        "http://localhost/messengerci/assets/app-precond.js",
        // "http://localhost/messengerci/assets/app.js",
        "http://localhost/messengerci/assets/app-assets/lib/xmppApi.js"
    ]
    
});

Harness.start(
    {
        group               : 'Sanity',
        items               : [
            'tests/010_sanity.t.js'
        ]
    },
    {
        group               : 'XMPP',
        defaultTimeout		: 120000,
        items               : [
        	'tests/xmpp/001_strophebackend.t.js',
        	'tests/xmpp/002_xmppapi.t.js'
        ]
    },
    {
        group               : 'Utility',
        items               : [
            'tests/util/001_messengerglobal.t.js'
        ]
    },
    {
        group               : 'Model',
        items               : [
            'tests/model/001_chatcategorymodel.t.js',
            'tests/model/002_metadatamodel.t.js',
            'tests/model/003_personalmacro_model.t.js',
            'tests/model/004_transferrequest_model.t.js',
            'tests/model/005_userchattranscript_model.t.js'
        ]
    },
    {
        group               : 'Views',
        items               : [
            'tests/012_userlist_view.t.js',
            'tests/013_useredit_view.t.js'
        ]
    },
    {
        group               : 'Application',
        
        // need to set the `preload` to empty array - to avoid the double loading of dependencies
        preload             : [],
        
        items : [
            {
                hostPageUrl         : 'app.html',
                url                 : 'tests/014_app.t.js'
            }
        ]
    }
);

