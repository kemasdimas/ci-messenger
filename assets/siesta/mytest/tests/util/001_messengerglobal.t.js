var testMethods = {
	checkUTCDate: function(next, t) {
		t.diag('Testing WGMessenger_GLOBAL.parseUTCDate');
		t.ok(WGMessenger_GLOBAL.parseUTCDate, 'WGMessenger_GLOBAL.parseUTCDate is here');
		
		var dateString = '19891105T01:27:27';
		var result = WGMessenger_GLOBAL.parseUTCDate(dateString);
		
		console.log(result);
		
		t.diag('Check in GMT + 7');
		t.is(result.getDate(), 5, 'Date is 5');
		t.is(result.getMonth(), 10, 'Month is November');
		t.is(result.getFullYear(), 1989, 'Year is 1989');
		
		t.is(result.getHours(), 8, 'Hours is 8');
		t.is(result.getMinutes(), 27, 'Minutes is 27');
		t.is(result.getSeconds(), 27, 'Seconds is 27');
		
		next(t);
	},
	
	checkPageTitleNotif: function(next, t) {
		t.diag('Testing WGMessenger_GLOBAL.addPageTitleNotification');
		t.ok(WGMessenger_GLOBAL.addPageTitleNotification, 'WGMessenger_GLOBAL.addPageTitleNotification is here');
		
		WGMessenger_GLOBAL.addPageTitleNotification(5);
		t.is(document.title, '(5) ' + WGMessenger_GLOBAL.defaultPageTitle, 'Document title is (5) ' + WGMessenger_GLOBAL.defaultPageTitle);
		
		WGMessenger_GLOBAL.addPageTitleNotification(-100);
		t.is(document.title, WGMessenger_GLOBAL.defaultPageTitle, 'Document title is ' + WGMessenger_GLOBAL.defaultPageTitle);
		
		next(t);
	},
	
	checkIsDemoAccount: function(next, t) {
		t.diag('Testing WGMessenger_GLOBAL.isDemoAccount');
		t.ok(WGMessenger_GLOBAL.isDemoAccount, 'WGMessenger_GLOBAL.isDemoAccount is here');
		
		var workgroupname = 'demo';
		var workgroupname2 = 'wgchat';
		var username = 'kemas';
		
		t.is(WGMessenger_GLOBAL.isDemoAccount(workgroupname, username), true, "Workgroup 'demo' is demo account");
		t.is(WGMessenger_GLOBAL.isDemoAccount(workgroupname2, username), false, "Workgroup 'wgchat' is not demo account");
		
		next(t);
	},
	
	checkGetRequestRecord: function(next, t) {
		t.diag('Testing WGMessenger_GLOBAL.getRequestRecord');
		t.ok(WGMessenger_GLOBAL.getRequestRecord, 'WGMessenger_GLOBAL.getRequestRecord is here');
		t.requireOk('WG.store.UserRequests');
		t.requireOk('WG.store.TransferRequests');
		
		var prevFeatures = WGMessenger_GLOBAL.features;

		// Mockup the feature
		WGMessenger_GLOBAL.features.conference = {
			jid: 'testconference'
		};
		
		var sessId = 'abc';
		
		// No record, return null
		t.is(WGMessenger_GLOBAL.getRequestRecord(sessId), null, 'No record found!');
		
		// return the state
		WGMessenger_GLOBAL.features = prevFeatures;
		next(t);
	},
	
	checkSetTimedLoading: function(next, t) {
		t.diag('Testing WGMessenger_GLOBAL.setTimedLoading');
		t.ok(WGMessenger_GLOBAL.setTimedLoading, 'WGMessenger_GLOBAL.setTimedLoading is here');
		
		// Create sample component
		var button = Ext.create('Ext.Button', {
		    text: 'Click me',
		    renderTo: Ext.getBody()
		});
		
		t.chain(
			function(innerNext) {
				t.diag('Test loading mask timeout for 1 second');
				var loadingMask = WGMessenger_GLOBAL.setTimedLoading(button, true, 1000);
				var maskId = loadingMask.id;
				
				setTimeout(function() {
					var lookupMask = Ext.getCmp(maskId);
					
					t.notOk(lookupMask, 'Loading mask diasppeared after 1 second');
					
					setTimeout(innerNext, 500);
				}, 1001);
			},
			
			function(innerNext) {
				t.diag('Test loading mask default timeout (3 seconds)');
				var loadingMask = WGMessenger_GLOBAL.setTimedLoading(button);
				var maskId = loadingMask.id;
				
				setTimeout(function() {
					var lookupMask = Ext.getCmp(maskId);
					
					t.ok(lookupMask, 'Mask still exist after 2.8 seconds');
					
					setTimeout(innerNext, 500);
				}, 2800);
			},
			
			function(innerNext) {
				t.diag('Test loading mask no timeout (-1)');
				var loadingMask = WGMessenger_GLOBAL.setTimedLoading(button, true, -1);
				var maskId = loadingMask.id;
				
				setTimeout(function() {
					var lookupMask = Ext.getCmp(maskId);
					
					t.ok(lookupMask, 'Mask still exist after 3.5 seconds');
					
					setTimeout(innerNext, 500);
				}, 3500);
			},
			
			function() {
				button.destroy();
				
				next(t);
			}
		);
	}
};

StartTest(function(t) {
	WGMessenger_GLOBAL.isTesting = true;

	t.ok(WGMessenger_GLOBAL, 'WGMessenger_GLOBAL is here');
	
	t.diag("Testing needs XMPP & PUNJAB, please run OPENFIRE & PUNJAB locally!");
	
	t.chain(
		function(next) { next(t); },
		
		testMethods.checkUTCDate,
		testMethods.checkPageTitleNotif,
		testMethods.checkIsDemoAccount,
		testMethods.checkGetRequestRecord,
		testMethods.checkSetTimedLoading,
		
		function() {
			t.done();
		}
 	);
});