StartTest(function(t) {

    t.requireOk('WG.model.ChatCategory', function() {
        var mod = Ext.create('WG.model.ChatCategory', {
            title : 'Slack', 
            body : 'Jocum'
        });
        
        t.is(mod.get('title'), 'Slack', 'title works fine');
        t.is(mod.get('body'), 'Jocum', 'email works fine');
        t.is(mod.get('isChecked'), false, 'isChecked default to false');
    });
});
