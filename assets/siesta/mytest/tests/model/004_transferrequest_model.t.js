StartTest(function(t) {

    t.requireOk('WG.model.TransferRequest', function() {
        var mod = Ext.create('WG.model.TransferRequest', {});
        
        t.is(mod.get('isActive'), false, 'isActive defaults to false');
    });
});
