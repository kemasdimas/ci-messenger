StartTest(function(t) {

    t.requireOk('WG.model.PersonalMacro', function() {
        var mod = Ext.create('WG.model.PersonalMacro', {});
        
        t.is(mod.get('group'), '', 'default group is empty');
    });
});
