StartTest(function(t) {
	t.ok(Strophe, 'Strophe is here');
	t.ok(StropheBackend, 'StropheBackend is here');
	t.is(StropheBackend.xmppDomain, 'kemass-macbook-air.local', 'XMPP domain correct');
	t.is(StropheBackend.boshUrl, 'http://kemass-macbook-air.local:8080/http-bind/', 'BOSH URL correct');
	
	t.diag("Testing XMPP connector, please run OPENFIRE & PUNJAB locally!");
	
	var username = 'test1';
	var password = 'test1';
	var passwordFalse = 'a';
	
	// t.wait('connect');
	
	t.chain(
		function(next) {
			
			// Test Connection OK
			var isFirst = true;
			
			StropheBackend.openConnection(username, password, function(status, msg) {
				if (isFirst && status === Strophe.Status.CONNECTED || status === Strophe.Status.AUTHFAIL) {
					isFirst = false;
					
					t.is(Strophe.Status.CONNECTED, status, 'Successfully Connected!');
					StropheBackend.closeConnection();
					
					next();
				}
			});
		},
		
		function(next) {
			
			// Test password wrong
			var isFirst = true;
			
			StropheBackend.openConnection(username, passwordFalse, function(status, msg) {
				if (isFirst && status === Strophe.Status.CONNECTED || status === Strophe.Status.AUTHFAIL) {
					isFirst = false;
					
					t.is(Strophe.Status.AUTHFAIL, status, 'Invalid account, can\'t login!');
					StropheBackend.closeConnection();
					
					next();
				}
			});
		},
		
		function(next) {
			StropheBackend.resetConnectionCookie();
			
			// Test disconnection
			StropheBackend.openConnection(username, password, function(status, msg) {
				if (status === Strophe.Status.CONNECTED) {
					StropheBackend.closeConnection();
				} else if (status === Strophe.Status.DISCONNECTED) {
					t.is(Strophe.Status.DISCONNECTED, status, 'Sucessfully disconnected!');
					next();
				}
			});
		},
		
		function(next) {
			StropheBackend.resetConnectionCookie();
			
			// Test sending PING
			StropheBackend.openConnection(username, password, function(status, msg) {
				if (status === Strophe.Status.CONNECTED) {
					var prevRid = StropheBackend.connection.rid;
					var id = StropheBackend.getUniqueId();
					StropheBackend.sendPing(id);
					
					StropheBackend.connection.addHandler(function() {
						t.isGreater(StropheBackend.connection.rid, prevRid, 'send PING sucess');
						
						setTimeout(function() {
							StropheBackend.closeConnection();
							next();
						}, 250);
					}, null, null, 'result', id);
				}
			});
		},
		
		function(next) {
			StropheBackend.resetConnectionCookie();
			var isLoggingOut = false;
			
			// Test ATTACH cookie not updated
			StropheBackend.openConnection(username, password, function(status, msg) {
				if (status === Strophe.Status.CONNECTED) {
					var id = StropheBackend.getUniqueId();
					
					StropheBackend.connection.addHandler(function() {
						// Break the RID
						StropheBackend.connection.rid -= 5;
						StropheBackend.sendPing();
					}, null, null, 'result', id);
					
					StropheBackend.sendPing(id);
					
				} else if (status === Strophe.Status.DISCONNECTED) {
					if (!isLoggingOut) {
						setTimeout(function() {
							StropheBackend.attachConnection(function(status, msg) {
								if (status === Strophe.Status.ATTACHED || status === 99) {
									t.is(Strophe.Status.ATTACHED, status, 'Sucessfully attached (cookie NOT UPDATED)!');
									
									isLoggingOut = true;
									StropheBackend.closeConnection();
									
									next();
								}
							});
						}, 500);
					}
				}
			});
		},
		
		function(next) {
			
			StropheBackend.resetConnectionCookie();
			var isLoggingOut = false;
			
			// Test ATTACH cookie updated
			StropheBackend.openConnection(username, password, function(status, msg) {
				if (status === Strophe.Status.CONNECTED) {
					var id = StropheBackend.getUniqueId();
					
					StropheBackend.connection.addHandler(function() {
						// Break the RID
						StropheBackend.connection.rid -= 5;
						
						// Update connection cookie!
						StropheBackend.handleRidUpdate();
						
						StropheBackend.sendPing();
					}, null, null, 'result', id);
					
					StropheBackend.sendPing(id);
					
				} else if (status === Strophe.Status.DISCONNECTED) {
					if (!isLoggingOut) {
						setTimeout(function() {
							StropheBackend.attachConnection(function(status, msg) {
								if (status === Strophe.Status.ATTACHED || status === 99) {
									t.is(Strophe.Status.ATTACHED, status, 'Sucessfully attached (cookie UPDATED)!');
									
									isLoggingOut = true;
									StropheBackend.closeConnection();
									
									next();
								}
							});
						}, 500);
					}
				}
			});
		},
		
		function(next) {
			
			StropheBackend.resetConnectionCookie();
			var isLoggingOut = false;
			
			// Test ATTACH FAILED
			StropheBackend.openConnection(username, password, function(status, msg) {
				if (status === Strophe.Status.CONNECTED) {
					var id = StropheBackend.getUniqueId();
					
					StropheBackend.connection.addHandler(function() {
						// Break the RID
						StropheBackend.connection.rid -= 30;
						
						// Update connection cookie!
						StropheBackend.handleRidUpdate();
						
						StropheBackend.sendPing();
					}, null, null, 'result', id);
					
					StropheBackend.sendPing(id);
					
				} else if (status === Strophe.Status.DISCONNECTED) {
					if (!isLoggingOut) {
						setTimeout(function() {
							StropheBackend.attachConnection(function(status, msg) {
								if (status === Strophe.Status.ATTACHED || status === 99) {
									t.is(99, status, 'Failed to attach, invalid RID!');
									
									isLoggingOut = true;
									StropheBackend.closeConnection();
									
									next();
								}
							});
						}, 500);
					}
				}
			});
		},
		
		function() {
			// t.endWait('connect');
			t.done();
		}
	);
});