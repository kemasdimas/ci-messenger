StartTest(function(t) {
	WGMessenger_GLOBAL.isTesting = true;
	
	t.ok(Strophe, 'Strophe is here');
	t.ok(StropheBackend, 'StropheBackend is here');
	t.ok(xmppApi, 'xmppApi is here');
	t.is(StropheBackend.xmppDomain, 'kemass-macbook-air.local', 'XMPP domain correct');
	t.is(StropheBackend.boshUrl, 'http://kemass-macbook-air.local:8080/http-bind/', 'BOSH URL correct');
	
	t.diag("Testing API of XMPP connector, please run OPENFIRE & PUNJAB locally!");
	
	var username = 'test2';
	var workgroup = 'kemas';
	var password = 'test2';
	var passwordFalse = 'a';
	
	t.diag("Testing xmppApi.login() function");
	
	t.chain(
		function(next) {
			
			// Login Success
			xmppApi.login(username, workgroup, password);
			
			t.waitForEvent(WGMessenger_GLOBAL.messageBus, WGMessenger_GLOBAL.eventType.ui.loginSuccess, function() {
				xmppApi.logout();
				
				setTimeout(next, 250);
			}, this, 2000);
		},
		
		function(next) {
			
			// Login failed
			xmppApi.login(username, workgroup, passwordFalse);
			
			t.waitForEvent(WGMessenger_GLOBAL.messageBus, WGMessenger_GLOBAL.eventType.ui.loginFailed, function() {
				xmppApi.logout();
				
				setTimeout(next, 250);
			}, this, 2000);
		},
		
		function(next) {
			
			// Roster query Success
			xmppApi.login(username, workgroup, password);
			
			t.waitForEvent(WGMessenger_GLOBAL.messageBus, WGMessenger_GLOBAL.eventType.xmpp.rosterResultReceived, function() {
				xmppApi.logout();
				
				setTimeout(next, 250);
			}, this, 2000);
		},
		
		function(next) {
			StropheBackend.resetConnectionCookie();
			
			t.diag("Testing glogalIQHandler");
			xmppApi.login(username, workgroup, password);
			
			t.waitForEvent(WGMessenger_GLOBAL.messageBus, WGMessenger_GLOBAL.eventType.xmpp.rosterResultReceived, function() {
				
				t.waitForEvent(WGMessenger_GLOBAL.messageBus, WGMessenger_GLOBAL.eventType.ui.userReady, function() {
					var id = StropheBackend.getUniqueId();
					
					t.waitForEvent(WGMessenger_GLOBAL.messageBus, id, function() {
						xmppApi.logout();
						
						setTimeout(next, 250);
					}, this, 2000);
					
					StropheBackend.sendPing(id);
				}, this, 1000);
				
				// MOCK: fireup userready event!
				WGMessenger_GLOBAL.messageBus.fireEvent(
					WGMessenger_GLOBAL.eventType.ui.userReady
				);
			}, this, 2000);
		},
		
		function(next) {
			t.diag("Testing reattachment sucess");
			
			StropheBackend.resetConnectionCookie();
			
			xmppApi.login(username, workgroup, password);
			t.waitForEvent(WGMessenger_GLOBAL.messageBus, WGMessenger_GLOBAL.eventType.xmpp.rosterResultReceived, function() {
				// Break the RID
				StropheBackend.connection.rid -= 5;
				
				// Update connection cookie!
				StropheBackend.handleRidUpdate();
				StropheBackend.sendPing();
				
				t.waitForEvent(WGMessenger_GLOBAL.messageBus, WGMessenger_GLOBAL.eventType.ui.reattachSuccess, function() {
					xmppApi.logout();
					
					setTimeout(next, 250);
				}, this, 30000);
			}, this, 5000);
		},
		
		function(next) {
			t.diag("Testing reattachment failure");
			
			StropheBackend.resetConnectionCookie();
			
			xmppApi.login(username, workgroup, password);
			t.waitForEvent(WGMessenger_GLOBAL.messageBus, WGMessenger_GLOBAL.eventType.xmpp.rosterResultReceived, function() {
				// Break the RID
				StropheBackend.connection.rid -= 20;
				
				// Update connection cookie!
				StropheBackend.handleRidUpdate();
				StropheBackend.sendPing();
				
				t.waitForEvent(WGMessenger_GLOBAL.messageBus, WGMessenger_GLOBAL.eventType.ui.autoLogin, function() {
					xmppApi.logout();
					
					setTimeout(next, 250);
				}, this, 30000);
			}, this, 5000);
		},
		
		function() {
			t.done();
		}
	);
});