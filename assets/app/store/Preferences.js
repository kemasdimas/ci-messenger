Ext.define('WG.store.Preferences', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.Preference',
	autoLoad: true,
	
	data: [],
	
	// set default values for user preferences
	defaultValues: {
		acceptChatRequest: true
	},
	
	load: function() {
		this.callParent(arguments);
		
		// load preference data from cookie
		var savedPreferences = Ext.JSON.decode($.cookie('wgchat-preferences')) || { };
		
		var tempPrefs = [];
		Ext.Object.each(this._setDefaultValues(savedPreferences), function(key, value, pref) {
			tempPrefs.push({
				key: key,
				value: value
			});
		});
		
		this.loadData(tempPrefs);
	},
	
	_setDefaultValues: function(prefs) {
		return Ext.merge(this.defaultValues, prefs);
	},
	
	updatePreferences: function(prefs) {
		var savedPreferences = Ext.JSON.decode($.cookie('wgchat-preferences')) || { };
		
		savedPreferences = Ext.merge(savedPreferences, prefs);
		
		$.cookie('wgchat-preferences',  Ext.JSON.encode(savedPreferences), 365);
		
		this.load();
	}
});
