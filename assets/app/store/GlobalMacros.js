Ext.define('WG.store.GlobalMacros', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.PersonalMacro',
	autoLoad: true,
	data: [],
	
	groupField: 'group',
	
	sorters: [
		{
			property: 'title',
			direction: 'ASC'
		}
	],
	
	constructor: function() {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.globalMacroResultReceived,
			this.handleGlobalMacroResult, 
			this
		);
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.globalMacroResultReceived,
			this.handleGlobalMacroResult
		);
		
		this.callParent(arguments);
	},
	
	handleGlobalMacroResult: function(packet) {
    	var $packet = $(packet);
    	var type = $packet.attr('type');
    	var data = [];
    	
    	if (type === 'result') {
    		var modelRaw = $packet.find('model').text();
    		var $model = $(modelRaw);
    		var groups = $model.find('macrogroup');
    		
    		groups.each(function(index, group) {
    			var $group = $(group);
    			var groupTitle = $group.find('title').last().text();
    			if (groupTitle === 'Tags') {
    				return;
    			}
    			
    			var macros = $group.find('macro');
    		
	    		macros.each(function(index, macro) {
	    			var $macro = $(macro);
	    			var macroTitle = $macro.find('title').text();
	    			var macroResponse = $macro.find('response').text();
	    			var macroType = $macro.find('type').text();
	    			
	    			data.push({
	    				title: macroTitle,
	    				response: macroResponse,
	    				type: macroType,
	    				group: groupTitle
	    			});
	    		});
    		});
    		
    		try {
				this.loadData(data);
    			this.sort();
			} catch (err) {
				console.error(err);
			}	
    	}
    }
});
