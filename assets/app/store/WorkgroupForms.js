Ext.define('WG.store.WorkgroupForms', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.WorkgroupForm',
	autoLoad: true,
	data: [],
	
	sorters: [
		{
			property: 'label',
			direction: 'ASC'
		}
	],
	
	constructor: function(config) {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupFormReceived,
			this.handleWorkgroupFormReceived,
			this
		);
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupFormReceived,
			this.handleWorkgroupFormReceived
		);
		
		this.callParent(arguments);
	},
	
	handleWorkgroupFormReceived: function(packet) {
		var $packet = $(packet);
		var fields = $packet.find('field');
		var data = [];
		
		fields.each(function(index, elem) {
			var $field = $(elem);
			var name = $field.attr('var');
			var label = $field.attr('label') || '';
			
			data.push({
				name: name,
				label: label
			});
		});
		
		this.loadData(data, false);
	}
});
