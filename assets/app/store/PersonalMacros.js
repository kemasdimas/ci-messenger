Ext.define('WG.store.PersonalMacros', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.PersonalMacro',
	autoLoad: true,
	data: [],
	
	sorters: [
		{
			property: 'title',
			direction: 'ASC'
		}
	],
	
	constructor: function() {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.personalMacroResultReceived,
			this.handlePersonalMacroResult, 
			this
		);
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.personalMacroResultReceived,
			this.handlePersonalMacroResult
		);
		
		this.callParent(arguments);
	},
	
	handlePersonalMacroResult: function(packet) {
    	var $packet = $(packet);
    	var type = $packet.attr('type');
    	var data = [];
    	
    	if (type === 'result') {
    		var modelRaw = $packet.find('model').text();
    		var $model = $(modelRaw);
    		var macros = $model.find('macro');
    		
    		macros.each(function(index, macro) {
    			var $macro = $(macro);
    			var macroTitle = $macro.find('title').text();
    			var macroResponse = $macro.find('response').text();
    			var macroType = $macro.find('type').text();
    			
    			data.push({
    				title: macroTitle,
    				response: macroResponse,
    				type: macroType
    			});
    		});
    		
    		try {
				this.loadData(data);
    			this.sort();
			} catch (err) {
				console.error(err);
			}
    	}
    }
});
