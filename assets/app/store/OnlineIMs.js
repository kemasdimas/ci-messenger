Ext.define('WG.store.OnlineIMs', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.OnlineIM',
	autoLoad: true,
	sorters: [
		{
			property: 'type',
			direction: 'DESC'
		},
		{
			property: 'username',
			direction: 'ASC'
		}
	],
	
	data: []
});
