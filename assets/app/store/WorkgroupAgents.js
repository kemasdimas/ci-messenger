Ext.define('WG.store.WorkgroupAgents', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.Roster',
	autoLoad: true,
	data: [],
	
	sorters: [
		{
			property: 'status',
			direction: 'DESC'
		},
		{
			property: 'name',
			direction: 'ASC'
		}
	],
		
	constructor: function(config) {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupRosterPresenceChange,
			this.handleWorkgroupRosterPresenceChange,
			this
		);
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupRosterPresenceChange,
			this.handleWorkgroupRosterPresenceChange
		);
		
		this.callParent(arguments);
	},
	
	handleWorkgroupRosterPresenceChange: function(packet) {
		var $packet = $(packet);
		var from = Strophe.getBareJidFromJid($packet.attr('from'));
		var name = Strophe.getNodeFromJid(from);
		var show = $packet.find('show').text() || 'online';
		var type = $packet.attr('type');
		var isOnline = (type === 'unavailable' ? false : true);
		var agentStatus = $packet.find('agent-status');
		
		var roster = this.findRecord('jid', from);
		if (roster !== null) {
			// Modify / delete roster
			if (!isOnline) {
				this.remove(roster);	
			} else {
				roster.set('status', show);
			}
		} else {
			// Create new roster donk..
			this.add({
				name: name,
				jid: from,
				status: show,
				isOnline: isOnline
			});
		}
	}
});
