Ext.define('WG.store.Queues', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.Queue',
	autoLoad: true,
	data: [],
	
	sorters: [
		{
			property: 'jid',
			direction: 'ASC'
		}
	],
	
	proxy: {
		type: 'memory'
	},
	
	constructor: function(config) {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupQueuePresence,
			this.handleWorkgroupQueuePresence,
			this
		);
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupQueuePresence,
			this.handleWorkgroupQueuePresence
		);
		
		this.callParent(arguments);
	},
	
	handleWorkgroupQueuePresence: function(packet) {
		var $packet = $(packet);
		var jid = $packet.attr('from');
		var queueName = Strophe.getResourceFromJid(jid);
		var chatCount = parseInt($packet.find('count').text(), 10);
		var averageTime = parseInt($packet.find('time').text(), 10);
		var lastActive = new Date();
		if ($packet.find('oldest').size() > 0) {
			lastActive = WGMessenger_GLOBAL.parseUTCDate($packet.find('oldest').text());
		}
		var status = $packet.find('status').text();
		
		var record = this.findRecord('jid', jid);
		if (record) {
			record.set('chatCount', chatCount);
			record.set('lastActive', lastActive);
			record.set('averageTime', averageTime);
			record.set('status', status);
		} else {
			record = this.add({
				name: queueName,
				jid: jid,
				chatCount: chatCount,
				lastActive: lastActive,
				averageTime: averageTime,
				status: status
			});
		}
		
		this.sort();
	}
});
