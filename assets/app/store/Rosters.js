Ext.define('WG.store.Rosters', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.Roster',
	autoLoad: true,
	groupField: 'isOnline',
	data: [],
	isFirstPayload: true,
	
	sorters: [
		{
			property: 'isOnline',
			direction: 'DESC'
		},
		{
			property: 'name',
			direction: 'ASC'
		}
	],
	
	proxy: {
		type: 'memory',
		reader: {
			type: 'json',
			root: 'rosters'
		}
	},
	
	constructor: function(config) {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.rosterResultReceived,
			this.rosterResultHandler,
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.presenceChange,
			this.rosterPresenceHandler,
			this
		);
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.rosterResultReceived,
			this.rosterResultHandler
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.presenceChange,
			this.rosterPresenceHandler
		);
		
		this.callParent(arguments);
	},
	
	rosterPresenceHandler: function(packet) {
		var $packet = $(packet);
		var from = Strophe.getBareJidFromJid($packet.attr('from'));
		var show = $packet.find('show').text() || 'online';
		var type = $packet.attr('type');
		var isOnline = true;
		
		var roster = this.findRecord('jid', from);
		if (roster !== null) {
			if (type === 'unavailable') {
				show = type;
				isOnline = false;
			}
			
			roster.set('status', show);
			roster.set('isOnline', isOnline);
			
			this.sort();
			this.group();
		}
	},
	
	rosterResultHandler: function(packet) {
		var proxy = this.getProxy();
		
		var $packet = $(packet);
		var items = $packet.find('item');
		var data = {
			rosters: []
		};
		
		items.each(function(index, elem) {
			var $elem = $(elem);
			var jid = $elem.attr('jid');
			var name = $elem.attr('name') || '';
			var groups = $elem.find('group');
			var group = '';
			
			if (groups.size() > 0) {
				group = $(groups[0]).text();
			}
			
			data.rosters.push({
				id: jid,
				jid: jid,
				name: name,
				group: group,
				status: 'unavailable',
				isOnline: false
			});
		});
		
		proxy.data = data;
		
		this.load();
		
		// if (this.isFirstPayload) {
			this.isFirstPayload = false;
			WGMessenger_GLOBAL.messageBus.fireEvent(
				WGMessenger_GLOBAL.eventType.ui.userReady
			);	
		// }
	}
});
