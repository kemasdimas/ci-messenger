Ext.define('WG.store.GlobalChatHistories', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.ChatHistory',
	autoLoad: true,
	sorters: [
		{
			property: 'date',
			direction: 'DESC'
		}
	],
	
	data: [
		{
			visitorsName: 'None'
		}
	]
});
