Ext.define('WG.store.Accounts', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.Account',
	autoLoad: true,
	sorters: [
		{
			property: 'workgroup',
			direction: 'ASC'
		},
		{
			property: 'username',
			direction: 'ASC'
		}
	],
	
	data: []
});
