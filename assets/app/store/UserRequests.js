Ext.define('WG.store.UserRequests', {
	extend: 'Ext.data.Store',
	
	model: 'WG.model.UserRequest',
	autoLoad: true,
	data: [],
	
	sorters: [
		{
			property: 'sessionId',
			direction: 'ASC'
		}
	],
	
	constructor: function(config) {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupChatOfferReceived,
			this.handleWorkgroupChatOfferReceived,
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupOfferRevokeReceived,
			this.handleWorkgroupOfferRevokeReceived,
			this
		);
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupChatOfferReceived,
			this.handleWorkgroupChatOfferReceived
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupOfferRevokeReceived,
			this.handleWorkgroupOfferRevokeReceived
		);
		
		this.callParent(arguments);
	},
	
	_removeInactiveRequest: function(sessionId) {
		var record = this.findRecord('sessionId', sessionId);
		if (record && !record.get('isActive')) {
			this.remove(record);
		}
	},
	
	/**
	 * Workgroup offer revoke IQ will remove specified user request, so that removed request must not
	 * be processed any further.
	 */
	handleWorkgroupOfferRevokeReceived: function(packet) {
		var $packet = $(packet);
		var revoke = $packet.find('offer-revoke');
		var sessionId = revoke.attr('id');
		
		this._removeInactiveRequest(sessionId);
	},
	
	/**
	 * Workgroup offer must be appended to this store, before processing a request any further, must check 
	 * the sessionId availability in the store
	 */
	handleWorkgroupChatOfferReceived: function(packet) {
		var $packet = $(packet);
		var offer = $packet.find('offer');
		var userJid = offer.attr('jid');
		var sessionId = offer.attr('id');
		var timeout = parseInt($packet.find('timeout').text(), 10);
		
		var metadataArr = [];
		var metadatas = $packet.find('value');
		metadatas.each(function(index, metadata) {
			var $metadata = $(metadata);
			var name = $metadata.attr('name');
			var value = $metadata.text();
			
			// Hide userID and channel metadata value
			if (name !== 'channel') {
				metadataArr.push({
					name: name,
					value: value
				});	
			}
		});
		
		var record = this.add({
			sessionId: sessionId,
			userJid: userJid,
			timeout: timeout,
			offerStart: (new Date()),
			
			metadatas: metadataArr
		});
		
		WGMessenger_GLOBAL.messageBus.fireEvent(
			WGMessenger_GLOBAL.eventType.ui.userOfferReceived,
			this.findRecord('sessionId', sessionId)
		);
		
		// Remove this new request after timeout exceeded
		var me = this;
		Ext.defer(function() {
			me._removeInactiveRequest(sessionId);
		}, timeout * 1000);
	}
});
