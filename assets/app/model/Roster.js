Ext.define('WG.model.Roster', {
	extend: 'Ext.data.Model',
	
	fields: ['name', 'jid', 'group', 'status', 'isOnline'],
	
	proxy: {
		type: 'memory'
	}
});
