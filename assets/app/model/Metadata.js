Ext.define('WG.model.Metadata', {
	extend: 'Ext.data.Model',
	requires: [
		'Ext.data.SequentialIdGenerator'
	],
	idgen: 'sequential',
	
	fields: [
		{ name: 'id', type: 'int' },
		'name', 
		'label',
		'value',
		{ name: 'userrequest_id', type: 'int' }
	],
	
	proxy: {
		type: 'memory'
	}
});
