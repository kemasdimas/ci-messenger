Ext.define('WG.model.OnlineIM', {
	extend: 'Ext.data.Model',
	
	fields: ['username', 'type', 'handler'],
	
	proxy: {
		type: 'memory'
	}
});
