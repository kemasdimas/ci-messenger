Ext.define('WG.model.UserRequest', {
	extend: 'Ext.data.Model',
	requires: [
		'Ext.data.SequentialIdGenerator',
		'WG.model.Metadata'
	],
	idgen: 'sequential',
	
	fields: [
		{ name: 'id', type: 'int' },
		'sessionId', 
		'userJid',
		{ name: 'timeout', type: 'int' },
		{ name: 'offerStart', type: 'date'},
		{ name: 'isActive', type: 'boolean', defaultValue: false }
	],
	
	hasMany: [
		{ model: 'WG.model.Metadata', name: 'metadatas', primaryKey: 'id', foreignKey: 'userrequest_id' }
	],
	
	proxy: {
		type: 'memory'
	}
});
