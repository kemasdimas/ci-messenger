Ext.define('WG.model.UserChatTranscript', {
	extend: 'Ext.data.Model',
	
	fields: [
		'sessionId',
		'agentNames',
		{ name: 'date', type: 'date', defaultValue: new Date() },
		{ name: 'duration', type: 'integer', defaultValue: 0}
	],
	
	proxy: {
		type: 'memory'
	}
});
