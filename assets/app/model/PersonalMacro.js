Ext.define('WG.model.PersonalMacro', {
	extend: 'Ext.data.Model',
	
	fields: [
		'title', 
		'response', 
		'type',
		{ name: 'group', type: 'string', defaultValue: '' }
	],
	
	proxy: {
		type: 'memory'
	}
});
