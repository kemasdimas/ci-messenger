Ext.define('WG.model.Account', {
	extend: 'Ext.data.Model',
	
	fields: ['id', 'workgroup', 'username', 'password'],
	
	proxy: {
		type: 'memory'
	}
});
