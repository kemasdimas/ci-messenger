Ext.define('WG.model.Preference', {
	extend: 'Ext.data.Model',
	
	fields: [
		'key', 
		'value'
	],
	
	proxy: {
		type: 'memory'
	}
});
