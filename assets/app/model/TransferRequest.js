Ext.define('WG.model.TransferRequest', {
	extend: 'Ext.data.Model',
	
	fields: [
		'sessionId', 
		'userJid',
		'roomJid',
		'inviterJid',
		'reason',
		{ name: 'timeout', type: 'int' },
		{ name: 'offerStart', type: 'date'},
		{ name: 'isActive', type: 'boolean', defaultValue: false }
	],
	
	proxy: {
		type: 'memory'
	}
});
