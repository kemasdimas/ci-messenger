Ext.define('WG.model.Queue', {
	extend: 'Ext.data.Model',
	
	fields: [
		'name',
		'jid',
		{ name: 'chatCount', type: 'integer' },
		{ name: 'lastActive', type: 'date' },
		{ name: 'averageTime', type: 'integer' },
		'status'
	],
	
	proxy: {
		type: 'memory'
	}
});
