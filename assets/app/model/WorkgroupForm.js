Ext.define('WG.model.WorkgroupForm', {
	extend: 'Ext.data.Model',
	
	fields: ['name', 'label'],
	
	proxy: {
		type: 'memory'
	}
});
