Ext.define('WG.model.ChatCategory', {
	extend: 'Ext.data.Model',
	
	fields: [
		'title',
		'body', 
		{ name: 'isChecked', type: 'boolean', defaultValue: false }
	],
	
	proxy: {
		type: 'memory'
	}
});
