Ext.define('WG.model.ChatHistory', {
	extend: 'Ext.data.Model',
	
	fields: ['sessionId', 'visitorsName', 'visitorsEmail', 'question', 'date', 'duration'],
	
	proxy: {
		type: 'memory'
	}
});
