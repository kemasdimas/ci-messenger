Ext.define('WG.proxy.Strophe', {
	extend: 'Ext.data.proxy.Proxy',
	alias: 'proxy.stropheproxy',
	
	requestPacket: $iq(),
	
	additionalHandlers: [],
	
	reader: {
		type: 'json'
	},
	
	processPacket: Ext.emptyFn,
	
	setAdditionalHandlers: function(handlers) {
		this.additionalHandlers = handlers;
	},
	
	read: function(operation, callback, scope) {
		
		if (!StropheBackend.connection) {
			throw "Strophe backend connection not found.";
		}
		
		var me = this;
		
		// Add additional handler
		Ext.each(me.additionalHandlers, function(value) {
			StropheBackend.connection.addHandler(
				function(packet) {
					return value.handler(packet, me);
				},
				null || value.ns,
				null || value.name,
				null || value.type,
				null || value.id,
				null || value.from,
				null || value.options
			);
		}, me);
		
		// Add packet handler based on request id
		var id = $(this.requestPacket.tree()).attr('id');
		StropheBackend.connection.addHandler(function(packet) {
			var data = me.processPacket(packet);
			var reader = me.getReader(),
	            result = reader.read(data);
	
	        Ext.apply(operation, {
	            resultSet: result
	        });
	
	        operation.setCompleted();
	        operation.setSuccessful();
	        
			Ext.callback(callback, scope || me, [operation])
		}, null, null, null, id);
		StropheBackend.send(this.requestPacket);
	},
	
	update: function(operation, callback, scope) {
		operation.setStarted();
		operation.setCompleted();
        operation.setSuccessful();
        
        if (typeof callback == 'function') {
            callback.call(scope || this, operation);
        }
	}
});
