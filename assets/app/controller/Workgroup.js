/**
 * Workgroup.js
 * 
 * Controller to operate workgroup sidebar related functionality
 */
Ext.define('WG.controller.Workgroup', {
    extend: 'Ext.app.Controller',

	stores: [
		'WorkgroupAgents', 
		'GlobalChatHistories', 
		'WorkgroupForms',
		'UserRequests',
		'TransferRequests',
		'Queues',
		'PersonalMacros',
		'GlobalMacros'
	],
	models: [
		'Roster', 
		'ChatHistory', 
		'WorkgroupForm',
		'UserRequest',
		'TransferRequest',
		'Queue',
		'PersonalMacro'
	],
	views: [
		'Sidebar',
		'workgroup.HistoryList',
		'workgroup.OnlineAgentList',
		'workgroup.InitiateMessageWindow',
		'workgroup.TransferDestinationWindow',
		'workgroup.PersonalMacro',
		'workgroup.HistoryContextMenu'
	],
    
    componentBuffer: {},
    
    init: function() {
    	WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.ui.workgroupServiceError,
			this.handleWorkgroupServiceError, 
			this
		);
    	
    	WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupPresenceChange,
			this.handleWorkgroupPresenceChange, 
			this
		);
    	
    	WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.wgchatSlotNotificationReceived,
			this.handleSlotNotification, 
			this
		);
    	
    	this.control({
    		'sidebar button[action=messagetemplate]': {
    			click: this.handleMessageTemplateClick
    		},
    		
    		'sidebar button[action=sendimmessage]': {
    			click: this.handleSendIMMessageClick
    		},
    		
    		'sidebar chathistorylist': {
    			itemcontextmenu: this.handleChatHistoryItemContextMenu
    		}
    	});
    	
    	this.callParent(arguments);
    },
    
    destroy: function() {
    	WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.ui.workgroupServiceError,
			this.handleWorkgroupServiceError
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupPresenceChange,
			this.handleWorkgroupPresenceChange
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.wgchatSlotNotificationReceived,
			this.handleSlotNotification
		);
    	
    	this.callParent(arguments);
    },
    
    handleSlotNotification: function(packet) {
    	Ext.example.msg(
    		'Slot Layanan Pelanggan Penuh!', 
    		'Ada pelanggan yang sedang mengantri karena slot layanan percakapan telah penuh, Anda dapat:' +
    		'<ul>' + 
				'<li>Menutup jendela percakapan pelanggan yang sudah tidak aktif</li>' + 
				'<li>Membaca artikel <a href="https://wgchat.uservoice.com/knowledgebase/articles/112703" target="_blank">Mengatur Jumlah Maksimal Chat / CS</a></li>' +
			'</ul>', 
    		-1, 
    		function() {
			}
		);
		
		// Buzz to create more notificable notification :P
		soundManager.onready(function() {
			soundManager.play('buzz');
		});
    },
    
    handleWorkgroupPresenceChange: function(packet) {
    	var $packet = $(packet);
    	var notifyQueue = $packet.find('notify-queue');
    	
    	var workgroupTool = Ext.ComponentManager.get('workgrouptoolpanel');
    	if (!workgroupTool) {
    		if (notifyQueue.size() === 0) {
				// This user is not registered to any workgroup, show an alert!
    			Ext.Msg.alert('Not In Workgroup', 
    				'User: ' + StropheBackend.username + ' is not registered in workgroup ' + StropheBackend.workgroup + 
    				', please register this user via <a href="https://manage.wgchat.com/" target="_blank">WGchat Manage</a>');
    		} else {
    			// This user in in workgroup
    			WGMessenger_GLOBAL.messageBus.fireEvent(
					WGMessenger_GLOBAL.eventType.ui.workgroupServiceConfirmed
				);
				
				// Add new panel to sidebar laa..
				var sidebar = Ext.ComponentManager.get('main-sidebar');
				sidebar.add([
					{
						xtype: 'panel',
						id: 'workgrouptoolpanel',
						title: 'Workgroup Tools',
						iconCls: 'wgchat-icon-fastpath',
						items: [
							{
								xtype: 'onlineagentlist'
							}
						],
						listeners: {
							afterrender: {
								fn: function(comp) {
									WGMessenger_GLOBAL.messageBus.fireEvent(
										WGMessenger_GLOBAL.eventType.ui.queueTutorial,
										'workgroup-tool',
										
										'Workgroup Tool',
										
										'Menampilkan customer service yang sedang online. Anda dapat: ' + 
										'<ul>' + 
											'<li>Klik ganda pada salah satu member untuk memulai percakapan</li>' + 
											'<li>Menekan tombol "Edit Template" untuk mengubah template pesan Anda (gunakan fitur ini untuk mempercepat pelayanan pada pelanggan)</li>' +
											'<li>Menekan tombol "Send IM" untuk mengirimkan pesan 1 arah ke pelanggan</li>' +  
										'</ul>',
										
										function() {
											comp.expand(false);
										},
										
										comp
									);
								}
							}
						},
						tbar: [
							'->',
							{
								text: 'Edit Template',
								action: 'messagetemplate',
								iconCls: 'wgchat-icon-entries'
							},
							{
								text: 'Send IM',
								action: 'sendimmessage',
								iconCls: 'wgchat-icon-sendim'
							}
						]
					},
					{
						xtype: 'chathistorylist',
						title: 'Chat Histories',
						iconCls: 'wgchat-icon-history',
						listeners: {
							afterrender: {
								fn: function(comp) {
									WGMessenger_GLOBAL.messageBus.fireEvent(
										WGMessenger_GLOBAL.eventType.ui.queueTutorial,
										'customer-service-chat-histories',
										
										'Chat Histories',
										
										'Menampilkan riyawat percakapan yang pernah Anda layani, Anda dapat: ' + 
										'<ul>' + 
											'<li>Klik ganda pada salah satu riwayat untuk menampilkan detail riwayat</li>' +
											'<li>Klik kanan pada salah satu riwayat untuk menampilkan menu</li>' +
											'<li>Memilih menu "Send IM to Customer" untuk mengirimkan pesan 1 arah ke pelanggan</li>' +
										'</ul>',
										
										function() {
											comp.expand(false);
										},
										
										comp
									);
								}
							}
						},
					}
				]);
    		}	
    	}
    },
    
    handleWorkgroupServiceError: function() {
    	Ext.Msg.alert('Workgroup Error', 'Workgroup service can\'t be found, please contact our support (<a href="mailto:support@wgchat.com" target="_blank">support@wgchat.com</a>)');
    },
    
    handleMessageTemplateClick: function(comp) {
    	// Check whether a macro windows is opened
    	var macroWindow = Ext.ComponentManager.get('personalmacro');
    	if (macroWindow) {
    		macroWindow.focus();
    		return;
    	}
    	
    	var queryId = StropheBackend.getUniqueId();
    	var macroQuery = $iq({
    		id: queryId,
    		to: StropheBackend.workgroupFullJid,
    		type: 'get'
    	}).c('macros', { xmlns: 'http://jivesoftware.com/protocol/workgroup' }).
    	c('personal', {}, 'true');

		WGMessenger_GLOBAL.messageBus.addListener(
			queryId,
			function(packet) {
				try {
					var macroWindow = Ext.widget('personalmacro', {
			    		id: 'personalmacro'
			    	});
			    	macroWindow.show();	
				} catch (err) {
					// console.error(err);
				}
		    	
		    	comp.setLoading(false);
			}, 
			this,
			{single: true}
		);

		StropheBackend.send(macroQuery);
		
		WGMessenger_GLOBAL.setTimedLoading(comp);
		this.componentBuffer['messagetemplatebutton'] = comp;
    },
    
    handleSendIMMessageClick: function(comp, evt, opts) {
    	var imWindow = Ext.ComponentManager.get('initiatemessagewindow');
    	if (imWindow) {
    		imWindow.focus();
    		return;
    	}
    	
    	var queryId = StropheBackend.getUniqueId();
    	var imQuery = $iq({
    		id: queryId,
    		to: StropheBackend.workgroupFullJid,
    		type: 'get'
    	}).c('online-im-list', {
    		xmlns: 'http://wgchat.com/providers',
    		workgroupJID: StropheBackend.workgroupFullJid
    	});
    	
    	WGMessenger_GLOBAL.messageBus.addListener(
			queryId,
			function(packet) { 
				comp.setLoading(false);
				
				Ext.widget('initiatemessagewindow', {
					packet: packet,
					initialValue: opts.initialValue
				});
			},
			this,
			{ single: true }
		);
    	
    	StropheBackend.send(imQuery);
    	
    	if (comp.xtype === 'button') {
    		WGMessenger_GLOBAL.setTimedLoading(comp);
    	}
    },
    
    handleChatHistoryItemContextMenu: function(comp, record, item, index, evt) {
    	evt.stopEvent();
		
		var menu = Ext.widget('historycontextmenu', {
			record: record,
			grid: comp
		});
		menu.showAt(evt.getX(), evt.getY());
		
		return false;
    }
});