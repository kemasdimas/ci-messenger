/**
 * ConferenceChats.js
 * 
 * Controller to operate conference chat related functionality
 */
Ext.define('WG.controller.ConferenceChats', {
    extend: 'WG.controller.AbstractConferenceChats',
    
    views: [
		'user.FriendList',
		'user.RosterContextMenu',
		'chat.ConferenceChat',
		'chat.conference.ConferenceInvitation'
	],
	
	idPrefix: 'chatwindow-conf-',
	windowXtype: 'conferencechat',
	
	init: function() {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.conferenceInvitationReceived,
			this.handleConferenceInvitation, 
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.conferencePresenceChange,
			this.handleConferencePresence, 
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.groupMessageReceived,
			this.handleIncomingGroupMessage, 
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.groupNotificationReceived,
			this.handleIncomingGroupNotification, 
			this
		);
		
		this.control({
    		'rostercontextmenu [action=initiateconference]': {
    			click: this.initiateConferenceClick
    		},
			
			'conferenceinvitation button[action=accept]': {
				click: this.handleAcceptClicked
			},
			
			'conferenceinvitation button[action=reject]': {
				click: this.handleRejectClicked
			},
			
			'conferencechat roomparticipantlist': {
    			itemdblclick: this.startPrivateConference
    		},
			
			'conferencechat roomparticipantlist button[action=inviteother]': {
    			click: this.handleInviteOtherClick
    		},
			
			'conferencechat': {
    			activate: this.handlePanelActivate,
    			beforeclose: this.handlePanelClose
    		},
    		
    		'conferencechat textareafield': {
    			keypress: this.handleTextAreaKeypress
    		},
    		
    		'conferencechat button[action=send]': {
    			click: this.handleButtonSendClick
    		}
		});
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.conferenceInvitationReceived,
			this.handleConferenceInvitation
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.conferencePresenceChange,
			this.handleConferencePresence
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.groupMessageReceived,
			this.handleIncomingGroupMessage
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.groupNotificationReceived,
			this.handleIncomingGroupNotification
		);
		
		this.callParent(arguments);
	},
    
    startConference: function(roomJid, name, xtype) {
		if (xtype !== this.windowXtype)
			return;
		
		var chatscontainer = Ext.ComponentManager.get('chatscontainer');
    	
    	return chatscontainer.startConference(roomJid, name);
	},
    
	handleConferenceInvitation: function(packet) {
		var $packet = $(packet);
		var roomJid = $packet.attr('from');
		var invite = $packet.find('invite');
		var inviterJid = invite.attr('from');
		var reason = invite.find('reason').text();
		
		var name = Strophe.getNodeFromJid(inviterJid);
		
		var invitation = Ext.widget('conferenceinvitation', {
			inviterJid: inviterJid,
			inviterName: name,
			roomJid: roomJid,
			reason: reason,
			
			data: {
				name: name,
				reason: reason
			}
		});
		invitation.show();
	},
	
	handleAcceptClicked: function(button) {
		WGMessenger_GLOBAL.setStatusBarLoading('Preparing conference...');
		
		var window = button.up().up();
		var myNode = Strophe.getNodeFromJid(StropheBackend.connection.jid);
		
		var acceptPres = $pres({
			id: StropheBackend.connection.getUniqueId(),
			to: window.roomJid + '/' + myNode
		}).c('x', {
			xmlns: 'http://jabber.org/protocol/muc'
		});
		
		StropheBackend.send(acceptPres);
		
		window.close();
		
		// Add conference chat window!
		this.startConference(window.roomJid, window.inviterName, this.windowXtype);
	},
	
	handleRejectClicked: function(button) {
		var window = button.up().up();
		
		var rejectMsg = $msg({
			id: StropheBackend.connection.getUniqueId(),
			to: window.roomJid
		}).c('x', {
			xmlns: 'http://jabber.org/protocol/muc#user'
		}).c('decline', {
			to: window.inviterJid
		}).c('reason', {}, 'No thank you');
		
		StropheBackend.send(rejectMsg);
		
		window.close();
	},
	
	sendConferenceInvitation: function(inviteeJid) {
		if (!WGMessenger_GLOBAL.features.conference) {
			throw 'Server doesn\'t support conference service.';
		}
		
		WGMessenger_GLOBAL.setStatusBarLoading('Preparing conference...');
		
		var serviceJid = WGMessenger_GLOBAL.features.conference.jid;
		var randomId = StropheBackend.connection.getUniqueId();
		var initiatorName = Strophe.getNodeFromJid(StropheBackend.connection.jid);
		var initiatorJid = Strophe.getBareJidFromJid(StropheBackend.connection.jid);
		var newRoomNode = initiatorName + '_' + randomId;
		var newRoomJid = newRoomNode + '@' + serviceJid;
		
		var uniqueId = StropheBackend.connection.getUniqueId();
		var newRoomIq = $iq({
			id: uniqueId,
			to: newRoomJid,
			type: 'get'
		}).c('query', { xmlns: 'http://jabber.org/protocol/disco#info' });
		
		// Add Handler here, using closure please!
		var me = this;
		StropheBackend.connection.addHandler(function(packet) {
			var $packet = $(packet);
			
			if ($packet.find('error[code="404"]').size() > 0) {
				var fullRoomInitiator = newRoomJid + '/' + initiatorName;
				
				// No room exist, create one!
				var initPres = $pres({
					id: StropheBackend.getUniqueId(),
					to: fullRoomInitiator
				}).c('x', { xmlns: 'http://jabber.org/protocol/muc' });
				
				var queryId = StropheBackend.getUniqueId();
				var ownerQuery = $iq({
					id: queryId,
					to: newRoomJid,
					type: 'get'
				}).c('query', { xmlns: 'http://jabber.org/protocol/muc#owner' });
				
				// Must check whether the IQ query is replied
				StropheBackend.connection.addHandler(function(queryPacket) {
					var modifId = StropheBackend.getUniqueId();
					var ownerModifQuery = $iq({
						id: modifId,
						to: newRoomJid,
						type: 'set'
					}).c('query', { xmlns: 'http://jabber.org/protocol/muc#owner' }).
					c('x', { xmlns: 'jabber:x:data', type: 'submit' }).
					c('field', { 'var': 'FORM_TYPE', type: 'hidden' }).c('value', {}, 'http://jabber.org/protocol/muc#roomconfig').up().
					c('field', { 'var': 'muc#roomconfig_roomname', type: 'text-single' }).c('value', {}, newRoomNode).up().
					c('field', { 'var': 'muc#roomconfig_publicroom', type: 'boolean' }).c('value', {}, '0').up().
					c('field', { 'var': 'muc#roomconfig_roomowners', type: 'jid-multi' }).c('value', {}, initiatorJid);
					
					var invitationId = StropheBackend.getUniqueId();
					var invitationMsg = $msg({
						id: invitationId,
						to: newRoomJid
					}).c('x', { xmlns: 'http://jabber.org/protocol/muc#user' }).
					c('invite', { to: inviteeJid }).
					c('reason', {}, 'Please join in a conference');
					
					StropheBackend.send(ownerModifQuery);
					StropheBackend.send(invitationMsg);
				}, null, 'iq', 'result', queryId);
				
				StropheBackend.send(initPres);
				StropheBackend.send(ownerQuery);
			} else {
				// There's a room exist using this name, but it will be VERY RARE, nearly IMPOSIBLE
				// Skip the implementation
			}
		}, null, 'iq', null, uniqueId);
		
		StropheBackend.send(newRoomIq);
	},
	
	initiateConferenceClick: function(comp) {
		var record = comp.up().record;
		var inviteeJid = record.get('jid');
		
		this.sendConferenceInvitation(inviteeJid);
	}
});