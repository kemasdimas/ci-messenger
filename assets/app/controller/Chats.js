/**
 * Chats.js
 * 
 * Controller to operate basic (internal) chat related functionality
 */
Ext.define('WG.controller.Chats', {
    extend: 'WG.controller.AbstractChats',
	
	views: [
		'user.FriendList',
		'user.RosterContextMenu',
		'chat.BasicChat',
		'chat.BroadcastWindow'
	],
	
	idPrefix: 'chatwindow-basic-',
	
    init: function() {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.presenceChange,
			this.handlePresenceChange, 
			this
		);
    	
    	WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.chatMessageReceived,
			this.handleIncomingMessage, 
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.buzzReceived,
			this.handleIncomingMessage, 
			this
		);
    	
    	this.control({
    		'sidebar friendlist': {
    			itemdblclick: this.startChat,
    			itemcontextmenu: this.itemRightClick
    		},
    		
    		'sidebar onlineagentlist': {
    			itemdblclick: this.startChat,
    			itemcontextmenu: this.itemRightClick
    		},
    		
    		'rostercontextmenu [action=sendmessage]': {
    			click: this.initiateMessageClick
    		},
    		
    		'basicchat': {
    			activate: this.handlePanelActivate
    		},
    		
    		'basicchat textareafield': {
    			keypress: this.handleTextAreaKeypress
    		},
    		
    		'basicchat button[action=send]': {
    			click: this.handleButtonSendClick
    		},
    		
    		'basicchat button[action=buzz]': {
    			click: function(comp) {
    				var chatwindow = comp.up().up().up().up();
    				this.sendBuzz(chatwindow, comp);
    			}
    		},
    		
    		'basicchat button[action=initiateconference]': {
    			click: this.handleInitiateConferenceClick
    		},
    		
    		'button[action=sendbroadcastmessage]': {
    			click: this.handleSendBroadCastMessage
    		},
    		
    		'broadcastwindow button[action=broadcastnow]': {
    			click: this.handleBroadcastNowClick
    		}
    	});
    	
    	this.callParent(arguments);
    },
    
    destroy: function() {
    	WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.presenceChange,
			this.handlePresenceChange
		);
    	
    	WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.chatMessageReceived,
			this.handleIncomingMessage, 
			this
		);
    	
    	WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.buzzReceived,
			this.handleBuzz, 
			this
		);
    	
    	this.callParent(arguments);
    },
    
    handlePresenceChange: function(packet) {
    	var $packet = $(packet);
		var from = Strophe.getBareJidFromJid($packet.attr('from'));
    	var isOnline = $packet.attr('type') !== 'unavailable';
    	
		var chatwindow = Ext.ComponentManager.get(this.idPrefix + from);
		if (chatwindow) {
			var name = Strophe.getNodeFromJid(from);
			var record = this.getRosterFromStore('jid', from);
			
			if (record !== null) {
				name = record.get('name');
			}
			
			var time = Ext.Date.format(new Date(), 'H:i');
			var notif = '*** ' + name + " went offline at " + time;
			
			if (isOnline) {
				var status = $packet.find('status').text();
				var notif = '*** ' + name + " went " + status + " at " + time;
			}
			
			chatwindow.notificationReceived(notif);
		}
    },
    
    handleIncomingMessage: function(packet) {
		var $packet = $(packet);
		var fullFrom = $packet.attr('from');
		var bareFrom = Strophe.getBareJidFromJid(fullFrom);
		var isPrivateConference = (fullFrom.indexOf('@conference.') != -1);
		
		var chatwindow = Ext.ComponentManager.get(this.idPrefix + (!isPrivateConference ? bareFrom : fullFrom));
		if (!chatwindow) {
			var name = Strophe.getNodeFromJid(fullFrom);
			var status = 'online';
			var record = this.getRosterFromStore('jid', bareFrom);
			var title = name;
			var isOnline = true;
			
			// Check if jid contains @conference.
			if (isPrivateConference) {
				name = Strophe.getResourceFromJid(fullFrom);
				title = name + ' - ' + Strophe.getNodeFromJid(fullFrom);
			} else if (record !== null) {
				name = record.get('name');
				status = record.get('status');
				title = name;
			}
			
			var chatscontainer = Ext.ComponentManager.get('chatscontainer');
    		chatwindow = chatscontainer.startChat((!isPrivateConference ? bareFrom : fullFrom), name, title, status, isOnline);
		}
		
		var body = undefined;
		var from = (!isPrivateConference ? bareFrom : fullFrom);
		
		if ($packet.find('body').size() > 0) {
			body = $packet.find('body').text();
			
			// Check delayed timestamp
			var $delay = $packet.find('x[xmlns$="delay"]');
			var date = new Date();
			var time = '';
			if ($delay.size() > 0) {
				var stamp = $delay.attr('stamp');
				date = Ext.Date.parse(stamp, "YmdTH:i:s");
				
				var offset = date.getTimezoneOffset() * -60000;
				date = new Date(date.getTime() + offset);
			}
			
			time = Ext.Date.format(date, 'H:i');
			
			chatwindow.messageReceived(from, time, body);
		} else if ($packet.find('attention,buzz').size() > 0) {
			chatwindow.buzzReceived(from);
		}
		
		return true;
	},
    
    /**
     * TODO: Buzz in the private conference room is still misrouted!
     */
    sendBuzz: function(chatwindow, button) {
    	var to = chatwindow.jid;
    	var from = StropheBackend.connection.jid;
    	var isPrivateConference = (to.indexOf('@conference.') != -1);
    	
    	if (isPrivateConference) {
    		from = Strophe.getBareJidFromJid(to) + '/' + Strophe.getNodeFromJid(from);
    		to = Strophe.getResourceFromJid(to) + '@' + StropheBackend.xmppDomain;
    	}
    	
    	var msg = $msg({
    		to: to,
    		from: from,
    		id: StropheBackend.connection.getUniqueId()
    	}).c('attention', {
    		xmlns: 'urn:xmpp:attention:0'
    	}).up().c('buzz', {
    		xmlns: 'http://www.jivesoftware.com/spark'
    	});
    	
    	StropheBackend.send(msg);
    	
    	button.setDisabled(true);
    	
    	// Enable the buzz button after 30 seconds
    	Ext.defer(function() {
    		button.setDisabled(false);
    	}, 30000);
    },
    
    sendMessage: function(chatwindow) {
    	if (chatwindow.xtype !== 'basicchat')
			return;
			
    	var textarea = chatwindow.down('textareafield');
    	var body = textarea.getValue();
    	
    	if (body.trim().length > 0) {
    		var to = chatwindow.jid;
	    	var from = StropheBackend.connection.jid;
	    	
	    	textarea.setValue('');
	    	
	    	var msg = $msg({
	    		to: to,
	    		type: 'chat',
	    		id: StropheBackend.connection.getUniqueId()
	    	}).c('body', {}, body);
	    	
	    	StropheBackend.send(msg);
	    	
	    	chatwindow.messageReceived(Strophe.getNodeFromJid(from),  
	    			Ext.Date.format(new Date(), 'H:i'), body, true);	
    	}
    },
    
    startChat: function(grid, record) {
    	var chatscontainer = Ext.ComponentManager.get('chatscontainer');
    	
    	chatscontainer.startChat(record.get('jid'), record.get('name'), record.get('name'), record.get('status'), record.get('isOnline'));
    },
    
    itemRightClick: function(comp, record, item, index, evt) {
		evt.stopEvent();
		
		var menu = Ext.widget('rostercontextmenu', {
			record: record
		});
		menu.showAt(evt.getX(), evt.getY());
		
		return false;
	},
	
	initiateMessageClick: function(comp) {
		var record = comp.up().record;
		
		this.startChat(null, record);
	},
	
	handleInitiateConferenceClick: function(comp) {
		var chatWindow = comp.up('basicchat');
		var inviteeJid = chatWindow.jid;
		
		var conferenceController = this.application.getController('ConferenceChats');
		if (conferenceController) {
			conferenceController.sendConferenceInvitation(inviteeJid);
		}
	},
	
	handleSendBroadCastMessage: function(comp) {
		var broadcastWindow = Ext.ComponentManager.get('broadcast-window');
		
		if (broadcastWindow) {
			broadcastWindow.focus(false, false);
		} else { 
			Ext.widget('broadcastwindow');
		}
	},
	
	handleBroadcastNowClick: function(comp) {
		var broadcastWindow = comp.up('broadcastwindow');
		var form = broadcastWindow.down('form').getForm();
		
		if (form.isValid()) {
			var destinationList = broadcastWindow.down('friendlist').getSelectionModel().getSelection();
			var textBody = form.getValues().body;
			
			Ext.each(destinationList, function(roster) {
				var jid = roster.get('jid');
				
				/**
				 * <message id="IBgyL-65" to="group3-kemas@kemass-macbook-air.local"><body>coba aja</body></message>
				 */
				var msg = $msg({
					id: StropheBackend.getUniqueId(),
					to: jid
				}).c('body', {}, textBody);
				
				StropheBackend.send(msg);
			});
			
			broadcastWindow.close();
			Ext.example.msg('Broadcast Sent', 'Message successfully broadcasted.');
		}
	}
});