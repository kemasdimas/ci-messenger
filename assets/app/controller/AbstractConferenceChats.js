/**
 * AbstractConferenceChats.js
 * 
 * Base controller which extends AbstractChats.js and is extended by ConferenceChats.js and WorkgroupChats.js
 * operate basic multi user chat functionality
 */
Ext.define('WG.controller.AbstractConferenceChats', {
    extend: 'WG.controller.AbstractChats',
    
    views: [
		'ChatsContainer',
		'user.CheckableFriendList',
		'chat.conference.RoomParticipantList'
	],
	
	idPrefix: '',
	windowXtype: '',
	
	init: function() {
		
		this.control({
			'checkablefriendlist button[action=invitecancel]': {
				click: this.inviteCancelClick
			},
			
			'checkablefriendlist button[action=invitenow]': {
				click: this.inviteNowClick
			}
		});
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		this.callParent(arguments);
	},
	
	sendMessage: function(chatwindow) {
		if (chatwindow.xtype !== this.windowXtype)
			return;
			
    	var textarea = chatwindow.down('#chatarea');
    	var body = textarea.getValue();
    	
    	if (body.trim().length > 0) {
    		var to = chatwindow.jid;
	    	var from = StropheBackend.connection.jid;
	    	
	    	textarea.setValue('');
	    	
	    	var msg = $msg({
	    		to: to,
	    		type: 'groupchat',
	    		id: StropheBackend.connection.getUniqueId()
	    	}).c('body', {}, body).c('x', {
	    		xmlns: 'jabber:x:event'
	    	}).c('offline').up().
	    	c('delivered').up().
	    	c('displayed').up().
	    	c('composing');
	    	
	    	StropheBackend.send(msg);
	    	
	    	chatwindow.messageReceived(Strophe.getNodeFromJid(from),  
	    			Ext.Date.format(new Date(), 'H:i'), body, true);	
    	}
    },
    
	getParticipantFromStore: function(store, key, value) {
		if (store) {
			return store.findRecord(key, value)
		} else {
			return null;
		}
    },
	
	handleConferencePresence: function(packet) {
		var $packet = $(packet);
		var fullFrom = $packet.attr('from');
		var bareFrom = Strophe.getBareJidFromJid(fullFrom);
		var nodeName = Strophe.getResourceFromJid(fullFrom);
		var show = $packet.find('show').text() || 'online';
		var type = $packet.attr('type');
		var isOnline = true;
		
		var chatwindow = Ext.ComponentManager.get(this.idPrefix + bareFrom);
		
		if ($packet.find('status[code="201"]').size() > 0) {
			// This block is used when this user is initiating a conference!
			chatwindow = this.startConference(bareFrom, nodeName, this.windowXtype);	
		}
		
		if (chatwindow && chatwindow.xtype !== this.windowXtype)
			return;
		
		if (chatwindow) {
			var store = chatwindow.down('roomparticipantlist').getStore();
			
			var record = this.getParticipantFromStore(store, 'jid', fullFrom);
			var notif = '';
			var time = Ext.Date.format(new Date(), 'H:i');
			
			if (!record) {
				// Add record donk
				store.add({
					name: nodeName,
					jid: fullFrom,
					status: show,
					isOnline: isOnline
				});
				
				notif = nodeName + ' has joined this conference';
			} else if (type === 'unavailable') {
				store.remove(record);
				
				notif = nodeName + ' has left this conference';
			} else {
				record.set('status', show);
			}
			
			chatwindow.notificationReceived(notif + ' at ' + time);
		}
		
		WGMessenger_GLOBAL.clearStatusBar();
	},
	
	handleIncomingGroupMessage: function(packet) {
		var $packet = $(packet);
		var fullFrom = $packet.attr('from');
		var bareFrom = Strophe.getBareJidFromJid(fullFrom);
		var senderName = Strophe.getResourceFromJid(fullFrom);
		var myName = Strophe.getNodeFromJid(StropheBackend.connection.jid);
		
		var chatwindow = Ext.ComponentManager.get(this.idPrefix + bareFrom);
		if (!chatwindow) {
			// Add conference chat window!
			chatwindow = this.startConference(bareFrom, senderName, this.windowXtype);
		}
		
		if (chatwindow && chatwindow.xtype !== this.windowXtype)
			return;
		
		if (chatwindow) {
			var body = undefined;
			if ($packet.find('body').size() > 0) {
				body = $packet.find('body').text();
				
				// Check delayed timestamp
				var $delay = $packet.find('x[xmlns$="delay"]');
				var date = new Date();
				var time = '';
				if ($delay.size() > 0) {
					var stamp = $delay.attr('stamp');
					date = Ext.Date.parse(stamp, "YmdTH:i:s");
					
					var offset = date.getTimezoneOffset() * -60000;
					date = new Date(date.getTime() + offset);
				} else if (myName === senderName) {
					return;
				}
				
				time = Ext.Date.format(date, 'H:i');
				chatwindow.messageReceived(senderName, time, body, myName === senderName);
			}	
		}
	},
	
	handleIncomingGroupNotification: function(packet) {
		var $packet = $(packet);
		var from = $packet.attr('from');
		var body = $packet.find('body');
		var decline = $packet.find('decline');
		var time = Ext.Date.format(new Date(), 'H:i');
		
		var chatwindow = Ext.ComponentManager.get(this.idPrefix + from);
		if (chatwindow && chatwindow.xtype !== this.windowXtype)
			return;
			
		if (chatwindow && body.size() > 0) {
			// Standard group notification
			
			var notif = body.text() + ' at ' + time;
			chatwindow.notificationReceived(notif);
		} else if (chatwindow && decline.size() > 0) {
			// Decline notification from other user
			
			var declineFrom = Strophe.getNodeFromJid(decline.attr('from'));
			var reason = '(declined) ' +  decline.find('reason').text();
			chatwindow.messageReceived(declineFrom, time, reason);
		}
	},
	
	_confirmPanelClose: function(chatwindow) {
		var cofirmation = Ext.Msg.confirm(
			'Confirmation', 
			'Are you sure to leave this conference?', 
			function(answer) {
				if (answer === 'yes') {
					// Send left from conference
					var myNode = Strophe.getNodeFromJid(StropheBackend.connection.jid);
					
					var leftPres = $pres({
						id: StropheBackend.connection.getUniqueId(),
						to: chatwindow.jid + '/' + myNode,
						type: 'unavailable'
					}).c('x', {
						xmlns: 'http://jabber.org/protocol/muc'
					});
					
					StropheBackend.send(leftPres);
					
					chatwindow.destroy();
				}
    	});
    	
    	return false;
	},
	
	handlePanelClose: function(chatwindow, evt) {
		if (chatwindow.xtype !== this.windowXtype)
			return;
		
		return this._confirmPanelClose(chatwindow);
	},

	startConference: Ext.emptyFn,
	
	startPrivateConference: function(grid, record) {
		var chatscontainer = Ext.ComponentManager.get('chatscontainer');
    	var title = record.get('name') + ' - ' + Strophe.getNodeFromJid(record.get('jid'));
    	
    	return chatscontainer.startChat(record.get('jid'), record.get('name'), title, '', true);
	},
	
	handleInviteOtherClick: function(comp) {
		var chatwindow = comp.up(this.windowXtype);
		if (chatwindow.xtype !== this.windowXtype)
			return;

		var inviteWindow = Ext.widget('checkablefriendlist', {
			title: 'Invite to Conference',
			jid: chatwindow.jid,
			tabId: chatwindow.id
		});
		inviteWindow.show();
	},
	
	inviteNowClick: function(comp) {
		var checkableWindow = comp.up().up();

		var selectionModel = checkableWindow.down('friendlist').getSelectionModel();
		var selection = selectionModel.getSelection();
		var chatwindow = Ext.ComponentManager.get(checkableWindow.tabId);
		
		Ext.Array.each(selection, function(item) {
			var inviteeJid = item.get('jid');
			var inviteeName = Strophe.getNodeFromJid(inviteeJid);
			
			var invitationMsg = $msg({
				id: StropheBackend.getUniqueId(),
				to: checkableWindow.jid
			}).c('x', { xmlns: 'http://jabber.org/protocol/muc#user' }).
			c('invite', { to: inviteeJid }).
			c('reason', {}, 'Please join in a conference');
			
			StropheBackend.send(invitationMsg);
			
			if (chatwindow) {
				var time = Ext.Date.format(new Date(), 'H:i');
				var notif = 'Invitation sent to ' + inviteeName + ' at ' + time;
				chatwindow.notificationReceived(notif);
			}
		});
		
		checkableWindow.close();
	},
	
	inviteCancelClick: function(comp) {
		comp.up().up().close();	
	}
});