/**
 * PersonalMacro.js
 * 
 * Controller to operate message template add / edit / delete functionality
 */
Ext.define('WG.controller.workgroup.PersonalMacros', {
    extend: 'Ext.app.Controller',

	stores: ['PersonalMacros'],
	models: ['PersonalMacro'],
	views: [
		'workgroup.PersonalMacro',
		'workgroup.PersonalMacroForm'
	],
  
    init: function() {
    	this.control({
    		'personalmacro grid': {
    			itemdblclick: this.handleMacroDoubleClick
    		},
    		
    		'personalmacro button[action=macroadd]': {
    			click: this.handleMacroAddClick
    		},
    		
    		'personalmacro button[action=macroedit]': {
    			click: this.handleMacroEditClick
    		},
    		
    		'personalmacro button[action=macrodelete]': {
    			click: this.handleMacroDeleteClick
    		},
    		
    		'personalmacro button[action=personalmacrosave]': {
    			click: this.handlePersonalMacroSaveClick
    		},
    		
    		'personalmacroform button[action=save]': {
    			click: this.handleFormSaveClick
    		}
    	});
    	
    	this.callParent(arguments);
    },
    
    destroy: function() {
    	this.callParent(arguments);
    },
    
    showForm: function(record) {
    	var formWindow = Ext.widget('personalmacroform');
    	
    	if (record) {
    		formWindow.down('form').loadRecord(record);
    	}
    },
    
    handleMacroDoubleClick: function(comp, record) {
    	this.showForm(record);
    },
    
    handleMacroAddClick: function(comp) {
    	this.showForm();
    },
    
    handleMacroEditClick: function(comp) {
   		var grid = comp.up().up();
   		var selectedData = grid.getSelectionModel().getSelection();
   		
   		if (selectedData.length > 0) {
   			selectedData = selectedData[0];
   			
   			this.showForm(selectedData);
   		}
    },
    
    handleMacroDeleteClick: function(comp) {
    	var grid = comp.up().up();
   		var selectedData = grid.getSelectionModel().getSelection();
   		
   		if (selectedData.length > 0) {
   			selectedData = selectedData[0];
   			
   			// Check whether selected data is "Auto Greeting", if true, show warning
   			if (selectedData.get('title') === 'Auto Greeting') {
   				Ext.Msg.alert('Error', 'Auto Greeting macro can not be deleted.');
   				return;
   			}
   			
   			this.getPersonalMacrosStore().remove(selectedData);
   		}
    },
    
    _isMacroExisted: function(title, record) {
    	var store = this.getPersonalMacrosStore();
    	var storedRecord = store.findRecord('title', title, 0, false, false, true);
    	
    	if (record && storedRecord.id == record.id) {
    		return false;
    	}
    	
    	return storedRecord !== null;
    },
    
    /**
     * Handle PersonalMacro window save
     */
    handlePersonalMacroSaveClick: function(comp) {
    	var macroWindow = comp.up().up();
    	var macrogroup = $build('macrogroup');
    	macrogroup.c('title', {}, 'Personal');
    	var macros = macrogroup.c('macros');
    	
    	var macroCount = this.getPersonalMacrosStore().count();
    	
    	this.getPersonalMacrosStore().each(function(record) {
    		macros.c('macro')
    			.c('title', {}, record.get('title'))
    			.c('description', {}, 'null')
    			.c('response', {}, record.get('response'))
    			.c('type', {}, '0').up();
    	}, this);
    	
    	var macroModifId = StropheBackend.getUniqueId();
    	var macroModif = $iq({
			id: macroModifId,
			to: StropheBackend.workgroupFullJid,
			type: 'set'
		}).c('macros', { xmlns: 'http://jivesoftware.com/protocol/workgroup' })
		  .c('personal', {}, 'true')
		  .c('personalMacro', {}, macrogroup.toString());
		
		WGMessenger_GLOBAL.messageBus.addListener(
			macroModifId,
			function(packet) {
				var $packet = $(packet);
				
				if ($packet.attr('type') === 'result') {
					Ext.example.msg('Macro Saved', 'Macro successfully saved to server');
				}
			}, 
			this,
			{single: true}
		);
		
		StropheBackend.send(macroModif);
    	
    	// Requery personal macro
    	var macroQuery = $iq({
    		id: StropheBackend.getUniqueId(),
    		to: StropheBackend.workgroupFullJid,
    		type: 'get'
    	}).c('macros', { xmlns: 'http://jivesoftware.com/protocol/workgroup' }).
    	c('personal', {}, 'true');
    	
    	StropheBackend.send(macroQuery);
    	
    	macroWindow.close();
    },
    
    /**
     * Handle PersonalMacroForm save
     */
    handleFormSaveClick: function(comp) {
    	var win    = comp.up('window'),
	        form   = win.down('form').getForm(),
	        record = form.getRecord(),
	        values = form.getValues();
	
		if (form.isValid()) {
			// If macro title exist, cancel operation!
			if (this._isMacroExisted(values.title, record)) {
				Ext.Msg.alert('Error', values.title + ' is already in macro list');
			} else {
				if (record) {
					record.set(values);	
				} else {
					values.type = 0;
					this.getPersonalMacrosStore().add(values);
				}
			    
			    win.close();	
			}
		}
    }
});