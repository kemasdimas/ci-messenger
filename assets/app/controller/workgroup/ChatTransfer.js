/**
 * ChatTransfer.js
 * 
 * Controller to operate workgroup chat (customer chat) transfer functionality
 */
Ext.define('WG.controller.workgroup.ChatTransfer', {
	extend: 'Ext.app.Controller',
	
	stores: [
		'Queues',
		'WorkgroupAgents'
	],
	
	models: [
		'Queue',
		'Roster'
	],
	
	views: [
		'workgroup.TransferDestinationWindow'
	],
	
	init: function() {
		
		this.control({
			'transferdestinationwindow': {
				beforeshow: this.handleTransferWindowBeforeShow
			},
			
			'transferdestinationwindow button[action=transfernow]': {
				click: this.handleTransferNowClick
			},
			
			'transferdestinationwindow grid': {
				itemdblclick: this.handleGridDoubleClick
			}
			
		});
		
		this.callParent(arguments);
	},
	
	_prepareTransferDestinations: function() {
		var data = [
			{ 
				name: 'Current Workgroup', 
				jid: StropheBackend.workgroupFullJid, 
				group: 'Workgroups',
				status: 'workgroup'
			}
		];
		
		this.getQueuesStore().each(function(record) {
			data.push({
				name: record.get('name'),
				jid: record.get('jid'),
				group: 'Departments',
				status: 'queue'
			});
		});
		
		this.getWorkgroupAgentsStore().each(function(record) {
			data.push({
				name: record.get('name'),
				jid: record.get('jid'),
				group: 'Agents',
				status: 'user'
			});
		});
		
		return data;
	},
	
	handleTransferWindowBeforeShow: function(comp) {
		var grid = comp.down('grid');
		grid.getStore().loadData(this._prepareTransferDestinations());
	},
	
	_sendTransferOffer: function(transferWindow, record) {
		var textArea = transferWindow.down('textareafield');
		
		var roomJid = transferWindow.roomJid;
		var type = record.get('status');
		var from = StropheBackend.connection.jid;
		var to = StropheBackend.workgroupFullJid;
		var sessionId = Strophe.getNodeFromJid(roomJid);
		var reason = textArea.getValue();
		var inviteeJid = record.get('jid');
		
		/**
		 * <iq id="o5gjB-75" to="kemas@workgroup.kemass-macbook-air.local" from="ciba-kemas@kemass-macbook-air.local/WG Messenger 2.6.3" type="set">
		 * 	<transfer xmlns="http://jabber.org/protocol/workgroup" type="queue">
		 * 	 <session xmlns="http://jivesoftware.com/protocol/workgroup" id="0qi8zv1784"></session>
		 *   <invitee>kemas@workgroup.kemass-macbook-air.local/Default Queue</invitee>
		 * 	 <reason>I&apos;m transferring this conversation to you.</reason>
		 *  </transfer> 
		 * </iq>
		 */
		var transferIq = $iq({
			id: StropheBackend.getUniqueId(),
			to: to,
			from: from,
			type: 'set'
		}).c('transfer', {
			xmlns: 'http://jabber.org/protocol/workgroup',
			type: type
		}).c('session', {
			xmlns: 'http://jivesoftware.com/protocol/workgroup',
			id: sessionId
		}).up().c('invitee', {}, inviteeJid).c('reason', {}, reason);
		
		StropheBackend.send(transferIq);
		
		transferWindow.close();
		
		// Append transfer info to corresponding chat window
		var chatWindow = Ext.ComponentManager.get('chatwindow-wgconf-' + roomJid);
		if (chatWindow) {
			var time = Ext.Date.format(new Date(), 'H:i');
			chatWindow.notificationReceived('Requesting transfer to ' + record.get('name') + ' at ' + time);
		}
	},
	
	handleTransferNowClick: function(comp) {
		var transferWindow = comp.up('window');
		var grid = transferWindow.down('grid');
		
		var records = grid.getSelectionModel().getSelection();
		if (records.length > 0) {
			this._sendTransferOffer(transferWindow, records[0]);	
		} else {
			Ext.Msg.alert('Invalid', 'Please choose a transfer destination');
		}
	},
	
	handleGridDoubleClick: function(comp, record) {
		var transferWindow = comp.up('window');
		
		this._sendTransferOffer(transferWindow, record);
	}
});
