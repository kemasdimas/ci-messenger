/**
 * ChatHistories.js
 * 
 * Controller to operate chat history list and also chat history detail functionality
 * handle showing transcripts, notes and also metadata
 */
Ext.define('WG.controller.workgroup.ChatHistories', {
    extend: 'Ext.app.Controller',

	stores: ['GlobalChatHistories', 'UserRequests', 'TransferRequests'],
	models: ['ChatHistory', 'UserChatTranscript', 'UserRequest', 'TransferRequest'],
	views: [
		'workgroup.HistoryList',
		'workgroup.HistoryDetail',
		'workgroup.MetadataList',
		'workgroup.HistoryContextMenu'
	],
  
  	loaderBuffer: {},
  
    init: function() {
    	WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.historyTranscriptReceived,
			this.handleHistoryTranscriptReceived, 
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.historyMetadataReceived,
			this.handleHistoryMetadataReceived, 
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.historyNoteReceived,
			this.handleHistoryNoteReceived, 
			this
		);
    	
    	this.control({
    		'chathistorylist': {
    			itemdblclick: this.handleHistoryItemDoubleClick,
    		},
    		
    		'sidebar chathistorylist': {
    			expand: this.handleSidebarChatHistoryExpand
    		},
    		
    		'workgroupchat chathistorylist': {
    			expand: this.handleWorkgroupChatHistoryExpand
    		},
    		
    		'chathistorylist button[action=historyrefreh]': {
    			click: this.historyRefreshClick
    		},
    		
    		'historycontextmenu [action=historyviewdetail]': {
    			click: this.handleHistoryViewDetailClick
    		},
    		
    		'historycontextmenu [action=historysendmessage]': {
    			click: this.handleHistorySendMessageClick
    		},
    		
    		'historydetail button[action=sendimmessage]': {
    			click: this.handleDetailSendMessageClick
    		}
    	});
    	
    	this.callParent(arguments);
    },
    
    destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.historyTranscriptReceived,
			this.handleHistoryTranscriptReceived
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.historyMetadataReceived,
			this.handleHistoryMetadataReceived
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.historyNoteReceived,
			this.handleHistoryNoteReceived
		);
		
    	this.callParent(arguments);
    },
    
    handleChatHistoryResult: function(packet) {
    	var $packet = $(packet);
    	var sessions = $packet.find('chat-session');
    	var data = [];
    	
    	if (sessions.size() > 0) {
    		sessions.each(function(index, session) {
    			var $session = $(session);
    			var dateRaw = $session.find('date').text().toString();
    			
    			var newData = {
    				sessionId: $session.find('sessionID').text(),
    				date: new Date(parseInt(dateRaw.substr(2), 10)),
    				duration: $session.find('duration').text(),
    				visitorsName: $session.find('visitorsName').text(),
    				visitorsEmail: $session.find('visitorsEmail').text(),
    				question: $session.find('question').text()
    			};
    			
    			data.push(newData);
    		});
    	}
    	
    	if (data.length > 0) {
    		this.getStore().loadData(data);
    	}
    	
    	this.setLoading(false);
    },
    
    handleUserHistoriesResult: function(packet) {
    	var $packet = $(packet);
    	var transcripts = $packet.find('transcript');
    	var data = [];
    	
    	transcripts.each(function(index, transcript) {
    		var $transcript = $(transcript);
    		var sessionId = $transcript.attr('sessionID');
    		var agentsJid = $transcript.find('agentJID');
    		var joinTimes = $transcript.find('joinTime');
    		var leftTimes = $transcript.find('leftTime');
    		
    		var agents = [];
    		var joinTime = WGMessenger_GLOBAL.parseUTCDate($(joinTimes[0]).text());
    		var leftTime = WGMessenger_GLOBAL.parseUTCDate($(leftTimes[leftTimes.size() - 1]).text());
    		var duration = Math.round((leftTime - joinTime) / 1000);
    		
    		agentsJid.each(function(index, agent) {
    			var $agent = $(agent);
    			var rawName = Strophe.getNodeFromJid($agent.text());
    			
    			agents.push(rawName.split('-')[0]);
    		});
    		
    		data.push({
    			sessionId: sessionId,
    			agentNames: agents.join(', '),
    			date: joinTime,
    			duration: duration
    		});
    	});
    	
    	this.getStore().loadData(data);
    	
    	this.setLoading(false);
    },
    
    _reloadHistory: function(grid) {
    	WGMessenger_GLOBAL.setTimedLoading(grid, 'Loading Histories');
    	
    	var historyQueryId = StropheBackend.getUniqueId();
    	var historyQuery = $iq({
    		id: historyQueryId,
    		to: StropheBackend.workgroupFullJid,
    		type: 'get'
    	}).c('chat-sessions', { 
    		xmlns: 'http://jivesoftware.com/protocol/workgroup',
    		agentJID: Strophe.getBareJidFromJid(StropheBackend.connection.jid),
    		maxSessions: '20',
    		startDate: '0'
    	});
    	
		WGMessenger_GLOBAL.messageBus.addListener(
			historyQueryId,
			this.handleChatHistoryResult,
			grid,
			{ single: true }
		);
    	
    	StropheBackend.send(historyQuery);
    },
    
    historyRefreshClick: function(comp) {
    	var grid = comp.up('grid');
    	this._reloadHistory(grid);
    },
    
    handleSidebarChatHistoryExpand: function(comp) {
    	if (!comp.isFirstHistoryQueue) {
    		return;
    	}
    	
    	this._reloadHistory(comp);
    	
    	comp.isFirstHistoryQueue = false;
    },
    
    handleWorkgroupChatHistoryExpand: function(comp) {
    	if (!comp.isFirstHistoryQueue) {
    		return;
    	}
    	
    	// TODO: load user history
    	// User transcript query
    	/**
    	 * <iq id="W4uUs-72" to="kemas@workgroup.kemass-macbook-air.local" type="get">
    	 * 	<transcripts xmlns="http://jivesoftware.com/protocol/workgroup" userID="kemas.dimas@yahoo.kemass-macbook-air.local">
    	 *  </transcripts>
    	 * </iq>
    	 */
    	var chatWindow = comp.up('workgroupchat');
    	var sessionId = Strophe.getNodeFromJid(chatWindow.jid);
    	var record = WGMessenger_GLOBAL.getRequestRecord(sessionId);
    	
    	if (record) {
    		var metadataStore = Ext.create('Ext.data.Store', {
    			model: 'WG.model.Metadata',
    			autoLoad: true,
    			data: record.data.metadatas,
    			proxy: {
					type: 'memory'
				},
    		})
    		
    		var rawJid = record.get('userJid');
    		var userId = metadataStore.findRecord('name', 'userID').get('value');
    		var resourceId = Strophe.getResourceFromJid(userId);
    		
    		if (resourceId) {
    			userId = resourceId;
    		}
    		
    		WGMessenger_GLOBAL.setTimedLoading(comp, 'Loading Histories');
    		
	    	var historyQueryId = StropheBackend.getUniqueId();
	    	var historyQuery = $iq({
	    		id: historyQueryId,
	    		to: StropheBackend.workgroupFullJid,
	    		type: 'get'
	    	}).c('transcripts', { 
	    		xmlns: 'http://jivesoftware.com/protocol/workgroup',
	    		userID: userId
	    	});
	    	
			WGMessenger_GLOBAL.messageBus.addListener(
				historyQueryId,
				this.handleUserHistoriesResult,
				comp,
				{ single: true }
			);
	    	
	    	StropheBackend.send(historyQuery);
    		
    		comp.isFirstHistoryQueue = false;		
    	}
    },
    
	handleHistoryTranscriptReceived: function(packet) {
		try {
			var $packet = $(packet);
			var transcript = $packet.find('transcript');
			
			var existingHistoryDetail = Ext.ComponentManager.get('single-historydetail');
			if (existingHistoryDetail) {
				existingHistoryDetail.loadTranscriptPacket(transcript.children());
				existingHistoryDetail.focus();
			} else {
				var detailWindow = Ext.widget('historydetail');
				detailWindow.loadTranscriptPacket(transcript.children());
				detailWindow.show();	
			}
			
			if (this.loaderBuffer['historygrid']) {
				this.loaderBuffer['historygrid'].setLoading(false);
		    	delete this.loaderBuffer['historygrid'];
			}	
		} catch (err) {
			console.error(err);
		}
	},
	
	_processRawMetadata: function(packet) {
		var $packet = $(packet);
		var values = $packet.find('value');
		var data = [];
		
		values.each(function(item, value) {
			var $value = $(value);
			var name = $value.attr('name');
			var valueBody = $value.text();
			
			data.push({
				name: name,
				value: valueBody
			});
		});
		
		return data;
	},
	
	handleHistoryMetadataReceived: function(packet) {
		var existingHistoryDetail = Ext.ComponentManager.get('single-historydetail');
		if (existingHistoryDetail) {
			existingHistoryDetail.loadMetadataData(this._processRawMetadata(packet));
		}
	},
	
	handleHistoryNoteReceived: function(packet) {
		var existingHistoryDetail = Ext.ComponentManager.get('single-historydetail');
		if (existingHistoryDetail) {
			var $packet = $(packet);
			var text = $packet.find('text').text();
			existingHistoryDetail.loadNote(text);
		}
	},
    
    handleHistoryViewDetailClick: function(comp) {
    	var record = comp.up().record;
    	
    	this.handleHistoryItemDoubleClick(comp.up().grid, record);
    },
    
    handleHistorySendMessageClick: function(comp) {
    	this.loaderBuffer['historygrid'] = comp.up().grid;
    	WGMessenger_GLOBAL.setTimedLoading(this.loaderBuffer['historygrid']);
    	
    	var record = comp.up().record;
    	
    	var sessionId = record.get('sessionId');
    	
    	var queryId = StropheBackend.getUniqueId();
    	var metadataQuery = $iq({
    		id: queryId,
    		to: StropheBackend.workgroupFullJid,
    		type: 'get'
    	}).c('chat-metadata', {
    		xmlns: 'http://jivesoftware.com/protocol/workgroup'
    	}).c('sessionID', {}, sessionId);
    	
    	WGMessenger_GLOBAL.messageBus.addListener(
			queryId,
			function(packet) {
				if (this.loaderBuffer['historygrid']) {
					this.loaderBuffer['historygrid'].setLoading(false);
			    	delete this.loaderBuffer['historygrid'];
				}
				
				var data = this._processRawMetadata(packet);
				var tempStore = Ext.create('Ext.data.Store', {
					model: 'WG.model.Metadata',
					proxy: {
						type: 'memory'
					},
					data: data
				});
				
				this._openSendMessageWindow(tempStore, comp);
			},
			this,
			{ single: true }
		);
    	
    	StropheBackend.send(metadataQuery);
    	
    	// Adding history query timeout action
    	Ext.defer(function() {
    		if (this.loaderBuffer['historygrid']) {
				this.loaderBuffer['historygrid'].setLoading(false);
		    	delete this.loaderBuffer['historygrid'];
		    	
		    	Ext.example.msg('Request Timeout', 'Send instant message request timeout, please try again');
			}
    	}, 9000, this);
    },
    
    handleDetailSendMessageClick: function(comp) {
    	var detailWindow = comp.up('window');
    	var metadataGrid = detailWindow.down('metadatalist');
    	
    	this._openSendMessageWindow(metadataGrid.getStore(), comp);
    },
    
    _openSendMessageWindow: function(tempStore, comp) {
    	var userIdRecord = tempStore.findRecord('name', 'userID');
		var channel = tempStore.findRecord('name', 'channel');
		
		if (userIdRecord && channel) {
			var value = userIdRecord.get('value');
			var handler = Strophe.getNodeFromJid(value);
			var resource = Strophe.getResourceFromJid(value);
			var destination = Strophe.getNodeFromJid(resource);
			
			var wgController = this.application.getController('Workgroup');
			wgController.handleSendIMMessageClick(comp, null, {
				initialValue: {
					sendto: destination,
					sendvia: handler
				}
			});
		} else {
			Ext.Msg.alert('Error', 'Conversation done via web widget, can\'t send message to this user');
		}
    },
    
    handleHistoryItemDoubleClick: function(comp, record) {
    	var existingHistoryDetail = Ext.ComponentManager.get('single-historydetail');
    	if (existingHistoryDetail) {
    		Ext.example.msg('History Window Exist', 'Please close existing chat history detail before opening another');
    		
    		return;
    	}
    	
    	this.loaderBuffer['historygrid'] = comp;
    	WGMessenger_GLOBAL.setTimedLoading(this.loaderBuffer['historygrid']);
	
    	var sessionId = record.get('sessionId');
    	
    	/**
    	 *  <iq id="fym5o-69" to="kemas@workgroup.kemass-macbook-air.local" type="get">
    	 * 		<transcript xmlns="http://jivesoftware.com/protocol/workgroup" sessionID="we778d1142"></transcript></iq>
		 *	<iq id="fym5o-70" to="kemas@workgroup.kemass-macbook-air.local" type="get">
		 * 		<chat-metadata xmlns="http://jivesoftware.com/protocol/workgroup"><sessionID>we778d1142</sessionID></chat-metadata> </iq>
		 *	<iq id="fym5o-71" to="kemas@workgroup.kemass-macbook-air.local" type="get">
		 * 		<chat-notes xmlns="http://jivesoftware.com/protocol/workgroup"><sessionID>we778d1142</sessionID></chat-notes> </iq>
    	 */
    	var transcriptQuery = $iq({
    		id: StropheBackend.getUniqueId(),
    		to: StropheBackend.workgroupFullJid,
    		type: 'get'
    	}).c('transcript', {
    		xmlns: 'http://jivesoftware.com/protocol/workgroup',
    		sessionID: sessionId
    	});
    	
    	var metadataQuery = $iq({
    		id: StropheBackend.getUniqueId(),
    		to: StropheBackend.workgroupFullJid,
    		type: 'get'
    	}).c('chat-metadata', {
    		xmlns: 'http://jivesoftware.com/protocol/workgroup'
    	}).c('sessionID', {}, sessionId);
    	
    	var noteQuery = $iq({
    		id: StropheBackend.getUniqueId(),
    		to: StropheBackend.workgroupFullJid,
    		type: 'get'
    	}).c('chat-notes', {
    		xmlns: 'http://jivesoftware.com/protocol/workgroup'
    	}).c('sessionID', {}, sessionId);
    	
    	StropheBackend.send(transcriptQuery);
    	Ext.defer(function() {
	    	StropheBackend.send(metadataQuery);
			StropheBackend.send(noteQuery);
    	}, 800)
    	
    	// Adding history query timeout action
    	Ext.defer(function() {
    		if (this.loaderBuffer['historygrid']) {
				this.loaderBuffer['historygrid'].setLoading(false);
		    	delete this.loaderBuffer['historygrid'];
		    	
		    	Ext.example.msg('Request Timeout', 'Send instant message request timeout, please try again');
			}
    	}, 9000, this);
    }
});