/**
 * OfferProggress.js
 * 
 * Controller to operate user request and transfer request offer window
 */
Ext.define('WG.controller.workgroup.OfferProgress', {
    extend: 'Ext.app.Controller',

	stores: ['Rosters', 'UserRequests', 'TransferRequests', 'WorkgroupForms', 'Preferences'],
	models: ['Roster', 'UserRequest', 'TransferRequest', 'WorkgroupForm', 'Preference'],
	views: [
		'workgroup.OfferProgressWindow',
		'workgroup.TransferOfferProgressWindow'
	],
  
  	additionalMetadataKeys: ['Division', 'TransportType', 'imUsername', 'RealUserID'],
  
    init: function() {
    	WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.ui.userOfferReceived,
			this.handleWorkgroupChatOfferReceived,
			this
		);
    	
    	WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.ui.transferOfferReceived,
			this.handleWorkgroupTransferOfferReceived,
			this
		);
    	
    	this.control({
    		'offerprogresswindow button[action=offeraccept]': {
    			click: this.handleOfferAcceptClick
    		},
    		
    		'offerprogresswindow button[action=offerreject]': {
    			click: this.handleOfferRejectClick
    		},
    		
    		'transferofferprogresswindow button[action=offeraccept]': {
    			click: this.handleTransferAcceptClick
    		},
    		
    		'transferofferprogresswindow button[action=offerreject]': {
    			click: this.handleOfferRejectClick
    		}
    	});
    	
    	this.callParent(arguments);
    },
    
    destroy: function() {
    	WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.ui.userOfferReceived,
			this.handleWorkgroupChatOfferReceived
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.ui.transferOfferReceived,
			this.handleWorkgroupTransferOfferReceived
		);
    	
    	this.callParent(arguments);
    },
    
    _getCustomerName: function(tabName, sessionId) {
    	if ($.trim(tabName).length === 0) {
    		var request = this._getRequestStore(sessionId).findRecord('sessionId', sessionId);
    		
    		if (request !== null) {
	    		$.each(request.get('metadatas'), function(idx, metadata) {
	    			if (metadata.name === 'username') {
	    				tabName = metadata.value;
	    			}
	    		});
    		}
    	}
    	
    	return tabName;
    },
    
    _getRequestStore: function(sessionId) {
    	var userRequestStore = this.getUserRequestsStore();
    	var transferRequestStore = this.getTransferRequestsStore();
    	
    	if (userRequestStore.find('sessionId', sessionId) !== -1) {
    		return userRequestStore;
    	} else if (transferRequestStore.find('sessionId', sessionId) !== -1) {
    		return transferRequestStore;
    	}
    	
    	return null;
    },
    
    _getRequestRecord: function(sessionId) {
    	var store = this._getRequestStore(sessionId);
    	
    	return store.findRecord('sessionId', sessionId);
    },
    
    _sendOfferResponse: function(progressWindow, sessionId, responseType, config) {
    	var store = this._getRequestStore(sessionId);
    	if (!store) {
    		return;
    	}
    	
    	var record = store.findRecord('sessionId', sessionId);
    	
    	if (record) {
    		/**
    		 * <iq id="9a44E-67" to="kemas@workgroup.kemass-macbook-air.local" type="set">
    		 * 	<offer-reject id="yw529a1512" xmlns="http://jabber.org/protocol/workgroup"/>
    		 * </iq>
    		 */
    		var responseId = config.responseId || StropheBackend.getUniqueId();
    		var responseIq = $iq({
    			id: responseId,
    			to: StropheBackend.workgroupFullJid,
    			type: 'set'
    		}).c(responseType, {
    			id: sessionId,
    			xmlns: 'http://jabber.org/protocol/workgroup'
    		});
    		
    		if (responseType === 'offer-reject') {
    			store.remove(record);
    		} else {
    			WGMessenger_GLOBAL.setStatusBarLoading('Preparing user chat...');
    			
    			record.set('isActive', true);
    		}
    		
    		StropheBackend.send(responseIq);
    	}
    	
    	progressWindow.close();
    },
    
    handleTransferAcceptClick: function(comp) {
    	var progressWindow = comp.up('window');
    	var sessionId = progressWindow.sessionId;
    	var responseId = StropheBackend.getUniqueId();
    	var tabName = this._getCustomerName(progressWindow.tabName, sessionId);
    	
    	var record = this._getRequestRecord(sessionId);
    	if (record && record.get('inviterJid')) {
    		var roomJid = record.get('roomJid');
    		
    		WGMessenger_GLOBAL.messageBus.addListener(
				responseId,
				function(packet) {
					// Initiate workgroup user chat tab
					var conferenceServiceJid = WGMessenger_GLOBAL.features.conference.jid;
			    	var userChatController = this.getController('WorkgroupChats');
			    	
			    	userChatController.startConference(roomJid, tabName, userChatController.windowXtype);
			    	
			    	// Send presence to the conference room
			    	var myNode = Strophe.getNodeFromJid(StropheBackend.connection.jid);
			    	var pres = $pres({
			    		id: StropheBackend.getUniqueId(),
			    		to: roomJid + '/' + myNode,
			    	}).c('x', { xmlns: 'http://jabber.org/protocol/muc' });
			    	
			    	StropheBackend.send(pres);
				}, 
				this,
				{ single: true }
			);
    		
    		this._sendOfferResponse(progressWindow, sessionId, 'offer-accept', {
	    		responseId: responseId
	    	});	
    	}
    },
    
    handleOfferAcceptClick: function(comp) {
    	var progressWindow = comp.up('window');
    	var sessionId = progressWindow.sessionId;
    	var responseId = StropheBackend.getUniqueId();
    	var tabName = this._getCustomerName(progressWindow.tabName, sessionId);
    	
    	WGMessenger_GLOBAL.messageBus.addListener(
			responseId,
			function(packet) {
				// Initiate workgroup user chat tab
				var conferenceServiceJid = WGMessenger_GLOBAL.features.conference.jid;
		    	var userChatController = this.getController('WorkgroupChats');
		    	
		    	userChatController.startConference(sessionId + '@' + conferenceServiceJid, tabName, userChatController.windowXtype);
			}, 
			this,
			{ single: true }
		);
    	
		this._sendOfferResponse(progressWindow, sessionId, 'offer-accept', {
    		responseId: responseId
    	});	
    },
    
    handleOfferRejectClick: function(comp) {
    	var progressWindow = comp.up('window');
    	var sessionId = progressWindow.sessionId;
    	
    	this._sendOfferResponse(progressWindow, sessionId, 'offer-reject', { });
    },
    
    _prepareWindowConfig: function(record) {
    	var wgForm = this.getWorkgroupFormsStore();
    	var metadatas = record.get('metadatas');
    	var filteredMetadata = [];
    	var me = this;
    	var departmentName = 'Default Queue';
    	var timeout = record.get('timeout');
    	var sessionId = record.get('sessionId');
    	var tabName = '';
    	var realName = '';
    	
    	Ext.Array.each(metadatas, function(metadata) {
    		var name = metadata.name;
    		var value = metadata.value;
    		
    		var formRecord = wgForm.findRecord('name', name);
    		if (formRecord) {
    			metadata.label = formRecord.get('label');
    			filteredMetadata.push(metadata);
    		} else if (name === 'Division') {
    			departmentName = value;
    			metadata.label = 'Department Name';
    			filteredMetadata.push(metadata);
    		} else if (name === 'TransportType') {
    			metadata.label = 'Gateway Type';
    			filteredMetadata.push(metadata);
    		} else if (name === 'imUsername') {
    			metadata.label = 'Gateway IM Account';
    			filteredMetadata.push(metadata);
    		} else if (name === 'RealUserID') {
    			tabName = value;
    			metadata.label = 'User IM Account';
    			filteredMetadata.push(metadata);
    		} 
    		
    		if (name === 'username') {
    			realName = value;
    		}
    	});
    	
    	
    	return {
    		title: 'Chat Request from ' + departmentName + ' Department',
    		timeout: timeout,
			sessionId: sessionId,
			tabName: tabName,
			realName: realName,
			metadatas: filteredMetadata
    	};
    },
    
    handleWorkgroupTransferOfferReceived: function(record) {
    	var config  = this._prepareWindowConfig(record);
    	var inviterJid = Strophe.getBareJidFromJid(record.get('inviterJid'));
    	var inviterName = Strophe.getNodeFromJid(inviterJid);
    	
    	var roster = this.getRostersStore().findRecord('jid', inviterJid);
    	if (roster) {
    		inviterName = roster.get('name');
    	}
    	
    	Ext.widget('transferofferprogresswindow', Ext.merge(config, {
    		title: 'Chat Transfer from ' + inviterName,
    		reason: record.get('reason')
    	})).show();
    },
    
    handleWorkgroupChatOfferReceived: function(record) {
    	var config  = this._prepareWindowConfig(record);
    	
    	var offerWindow = Ext.widget('offerprogresswindow', config);
    	offerWindow.show();
    	
    	// Auto accept this offer after 2 second
    	var record = this.getPreferencesStore().findRecord('key', 'acceptChatRequest');
    	if (record && record.get('value') === true) {
    		Ext.defer(function() {
    			var acceptButton = offerWindow.down('button[action=offeraccept]');
    			
    			// Programmatically simulate a button click
    			this.handleOfferAcceptClick.call(this, acceptButton, Ext.EventObject);
    		}, 2000, this);
    	}
    }
});