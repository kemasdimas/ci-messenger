/**
 * ChatCategories.js
 * 
 * Controller to operate chat categories sidebar in a workgroup chat (customer chat) window
 */
Ext.define('WG.controller.workgroup.ChatCategories', {
    extend: 'Ext.app.Controller',

	models: ['ChatCategory'],

	views: [
		'workgroup.ChatCategoryList'
	],
  
    init: function() {
    	this.control({
    		'chatcategorylist button[action=categorysave]': {
    			click: this.handleCategorySaveClick
    		},
    		
    		'chatcategorylist': {
    			expand: this.handleListExpand
    		}
    	});
    	
    	this.callParent(arguments);
    },
    
    destroy: function() {
    	this.callParent(arguments);
    },
    
    handleListExpand: function(comp) {
    	var chatWindow = comp.up('workgroupchat');
    	var chategoryList = chatWindow.down('chatcategorylist');
    	
    	if (chategoryList.isFirstExpand) {
    		var sessionId = Strophe.getNodeFromJid(chatWindow.jid);
    	
	    	// Query tags
			/**
			 * <iq id="zJW8W-71" to="kemas@workgroup.kemass-macbook-air.local" type="get">
			 * 	<list-tags xmlns="http://wgchat.com/providers" sessionID="5dztyu1648">
			 *  </list-tags> 
			 * </iq>
			 * <iq id="zJW8W-73" to="kemas@workgroup.kemass-macbook-air.local" type="set">
			 * 	<save-tags xmlns="http://wgchat.com/providers" sessionID="5dztyu1648" tags="{#kemas} {#dimas} {#raman}">
			 *  </save-tags>
			 * </iq>
			 */
			var tagQueryId = StropheBackend.getUniqueId();
			var tagQuery = $iq({
				id: tagQueryId,
				to: StropheBackend.workgroupFullJid,
				type: 'get'
			}).c('list-tags', {
				xmlns: 'http://wgchat.com/providers',
				sessionID: sessionId
			});
			
			var me = this;
			WGMessenger_GLOBAL.messageBus.addListener(
				tagQueryId,
				me.handleTagListPacket,
				comp,
				{ single: true }
			);
			
			StropheBackend.send(tagQuery);
    		WGMessenger_GLOBAL.setTimedLoading(comp);
    		
    		chategoryList.isFirstExpand = false;
    	}
    },
    
    handleTagListPacket: function(packet) {
    	// this = Chat Category List
    	
    	var $packet = $(packet);
    	var tags = $packet.find('tag');
    	
    	var store = this.getStore();
    	var selectionModel = this.getSelectionModel();
    	
    	store.removeAll();
    	tags.each(function(index, tag) {
    		var $tag = $(tag);
    		var title = $tag.find('title').text();
    		var body = $tag.find('body').text();
    		var isChecked = $tag.find('isChecked').text() === 'true';
    		
    		var record = store.add({
    			title: title,
    			body: body,
    			isChecked: isChecked
    		});
    		
    		if (isChecked) {
    			selectionModel.select(record, true);
    		}
    	});
    	
    	this.setLoading(false);
    },
    
    handleCategorySaveClick: function(comp) {
    	var categoryList = comp.up('chatcategorylist');
    	var store = categoryList.getStore();
    	var selectionModel = categoryList.getSelectionModel();
    	var selections = selectionModel.getSelection();
    	var chatWindow = comp.up('workgroupchat');
    	var sessionId = Strophe.getNodeFromJid(chatWindow.jid);
    	var tags = new Array();
    	
    	Ext.each(selections, function(record) {
    		tags.push(record.get('body'));
    	});
		
		var queryId = StropheBackend.getUniqueId();
		var queryIq = $iq({
			id: queryId,
			to: StropheBackend.workgroupFullJid,
			type: 'set'
		}).c('save-tags', {
			xmlns: 'http://wgchat.com/providers',
			sessionID: sessionId,
			tags: tags.join(' ')
		});
		
		WGMessenger_GLOBAL.messageBus.addListener(
			queryId,
			function(packet) {
				var $packet = $(packet);
				
				if ($packet.attr('type') === 'result') {
					Ext.example.msg('Chat Categories Saved', 'Chat categories successfully saved to server.');
					
					// Calling a function and pass scope in the first argument
					this.handleTagListPacket.call(categoryList, packet);
				}
			},
			this,
			{ single: true }
		);
		
		StropheBackend.send(queryIq);
		WGMessenger_GLOBAL.setTimedLoading(categoryList);
    }

});