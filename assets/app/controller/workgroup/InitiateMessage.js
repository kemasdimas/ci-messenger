/**
 * InitiateMessage.js
 * 
 * Controller to operate one way instant message sending
 */
Ext.define('WG.controller.workgroup.InitiateMessage', {
    extend: 'Ext.app.Controller',

	stores: ['OnlineIMs'],
	models: ['OnlineIM'],
	views: [
		'workgroup.InitiateMessageWindow'
	],
  
    init: function() {
    	this.control({
    		'initiatemessagewindow': {
    			beforeshow: this.handleWindowBeforeShow
    		},
    		
    		'initiatemessagewindow button[action=sendimmessage]': {
    			click: this.handleSendImMessageClick
    		}
    	});
    	
    	this.callParent(arguments);
    },
    
    destroy: function() {
    	this.callParent(arguments);
    },
    
    handleWindowBeforeShow: function(comp) {
    	var $packet = $(comp.packet);
    	var accounts = $packet.find('im-account');
    	var data = [];
    	
    	accounts.each(function(index, account) {
    		var $account = $(account);
    		var username = $account.find('username').text();
    		var type = $account.find('type').text();
    		var handler = $account.find('handler').text();
    		
    		data.push({
    			username: username, 
    			type: type,
    			handler: handler
    		});
    	});
    	
    	var store = this.getOnlineIMsStore();
    	store.loadData(data, false);
    	
    	if (comp.initialValue) {
    		var form = comp.down('form').getForm();
    		form.setValues(comp.initialValue);
    	}
    },
    
    handleSendImMessageClick: function(comp) {
    	var messageWindow = comp.up().up();
    	var form = messageWindow.down('form').getForm();
    	
    	if (form.isValid()) {
    		var values = form.getValues();
			var handler = values.sendvia;
			var body = values.messagecontent;
			var to = values.sendto;
    		
    		var combobox = messageWindow.down('combobox');
    		var comboStore = combobox.getStore();
    		var type = comboStore.findRecord('handler', handler).get('type');
    		
    		var msgIq = $iq({
    			id: StropheBackend.getUniqueId(),
    			to: StropheBackend.workgroupFullJid,
    			type: 'set'
    		}).c('initiate-message', {
    			xmlns: 'http://wgchat.com/providers',
    			destinationID: to,
    			handler: handler,
    			message: body,
    			transportType: type
    		});
    		
    		StropheBackend.send(msgIq);
			
			Ext.example.msg('Message Sent', 'Message sent to server.');
    		messageWindow.close();
    	}
    }
});