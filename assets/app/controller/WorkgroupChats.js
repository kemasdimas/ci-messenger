/**
 * WorkgroupChats.js
 * 
 * Controller to operate customer chat related functionality
 */
Ext.define('WG.controller.WorkgroupChats', {
    extend: 'WG.controller.AbstractConferenceChats',
    
    stores: ['UserRequests', 'TransferRequests', 'PersonalMacros'],
	models: ['UserRequest', 'TransferRequest', 'PersonalMacro'],
    
    views: [
		'chat.WorkgroupChat',
		'workgroup.TransferDestinationWindow',
	],
	
	idPrefix: 'chatwindow-wgconf-',
	windowXtype: 'workgroupchat',
	
	init: function() {
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupUserChatPresence,
			this.handleConferencePresence, 
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupUserChatMessage,
			this.handleIncomingGroupMessage, 
			this
		);

		this.control({
			'workgroupchat roomparticipantlist': {
    			itemdblclick: this.startPrivateConference
    		},
			
			'workgroupchat roomparticipantlist button[action=inviteother]': {
    			click: this.handleInviteOtherClick
    		},
			
			'workgroupchat': {
    			activate: this.handlePanelActivate,
    			beforeclose: this.handlePanelClose
    		},
    		
    		'workgroupchat #chatarea': {
    			keypress: this.handleTextAreaKeypress
    		},
    		
    		'workgroupchat button[action=send]': {
    			click: this.handleButtonSendClick
    		},
    		
    		'workgroupchat button[action=transferchat]': {
    			click: this.handleTransferChatClick
    		},
    		
    		'workgroupchat button[action=inviteagent]': {
    			click: this.handleInviteOtherClick
    		},
    		
    		'workgroupchat button[action=endchat]': {
    			click: this.handleEndChatClick
    		},
    		
    		'workgroupchat button[action=messagetemplate] menuitem': {
    			click: this.handleTemplateItemClick
    		},
    		
    		'workgroupchat button[action=notesave]': {
    			click: this.handleNoteSaveClick
    		}
		});
		
		this.callParent(arguments);
	},
	
	destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupUserChatPresence,
			this.handleConferencePresence
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.xmpp.workgroupUserChatMessage,
			this.handleIncomingGroupMessage
		);
		
		this.callParent(arguments);
	},
	
	startConference: function(roomJid, name, xtype) {
		if (xtype !== this.windowXtype)
			return;
		
		var sessionId = Strophe.getNodeFromJid(roomJid);
		var request = WGMessenger_GLOBAL.getRequestRecord(sessionId);
		
		if (request) {
			// Request global macro if chat is accepted
			var macroQuery = $iq({
	    		id: StropheBackend.getUniqueId(),
	    		to: StropheBackend.workgroupFullJid,
	    		type: 'get'
	    	}).c('macros', { xmlns: 'http://jivesoftware.com/protocol/workgroup' });
	    	
	    	StropheBackend.send(macroQuery);
	    	
			var chatscontainer = Ext.ComponentManager.get('chatscontainer');
			
			// Replace \\40 with @
	    	var chatWindow = chatscontainer.startWorkgroupConference(roomJid, name.replace('\\40', '@'), {
	    		metadatas: request.get('metadatas')
	    	});
		} else {
			throw 'UserRequest record for ' + sessionId + ' can not be found.';
		}
	},
	
	handleEndChatClick: function(comp) {
		var chatWindow = comp.up(this.windowXtype);
		chatWindow.close();
	},
	
	handleTransferChatClick: function(comp) {
		var chatWindow = comp.up(this.windowXtype);
		var roomJid = chatWindow.jid;
		
		Ext.widget('transferdestinationwindow', {
			roomJid: roomJid
		});
	},
	
	_removeRequestFromStore: function(roomId) {
		var record = WGMessenger_GLOBAL.getRequestRecord(roomId);
		
		if (record) {
			var store = record.store;
			store.remove(record);
		}
	},
	
	handleConferencePresence: function(packet) {
		
		var $packet		= $(packet);
		var fullFrom 	= $packet.attr('from');
		var type 		= $packet.attr('type');
		var bareFrom 	= Strophe.getBareJidFromJid(fullFrom);
		
		var fromNode 	= Strophe.getResourceFromJid(fullFrom);
		var myNode 		= Strophe.getNodeFromJid(StropheBackend.connection.jid);
		var chatwindow	= Ext.ComponentManager.get(this.idPrefix + bareFrom);
		
		if (fromNode === myNode && $packet.find('status[code="307"]').size() > 0) {
			// 307 = user kicked from the room, automatically destroy the chatwindow!
			if (chatwindow) {
				var roomId = Strophe.getNodeFromJid(chatwindow.jid);
				this._removeRequestFromStore(roomId);
				
				chatwindow.resetUnreadMessages();
				chatwindow.destroy();
			}
			
			var reason = $packet.find('reason').text();
			Ext.example.msg('Transfer Status', reason);
			
			return;
		}
		
		if (chatwindow) {
			if (type !== 'unavailable') {
				chatwindow.participantsCount++;
			} else {
				chatwindow.participantsCount--;
			}
			
			if (chatwindow.justOpened && chatwindow.participantsCount > 1) {
				Ext.defer(function() {
					var macroStore = this.getPersonalMacrosStore();
					var macro = macroStore.findRecord('title', 'Auto Greeting');
					
					if (macro && macro.get('response').trim().length > 0) {
						var textarea = chatwindow.down('#chatarea');
						textarea.setValue(macro.get('response').trim());
						
						this.sendMessage(chatwindow);
					}
				}, 1000, this);
				
				chatwindow.justOpened = false;
			}
		}
		
		this.callParent(arguments);
	},
	
	handlePanelClose: function(comp) {
		var isClosing  = this.callParent(arguments);
		
		if (isClosing) {
			// Remove the request from UserRequests or TransferRequests Store
			var roomId = Strophe.getNodeFromJid(comp.jid);
			this._removeRequestFromStore(roomId);
		}
		
		return isClosing;
	},
	
	handleTemplateItemClick: function(comp, e) {
		var body = comp.body;
		
		if (body) {
			var topMenu = comp;
			while (topMenu.parentMenu) {
				topMenu = topMenu.parentMenu;
			}
			var chatWindow = topMenu.floatParent.up(this.windowXtype);
			
			var textArea = chatWindow.down('#chatarea');
			var value = textArea.getValue();
			if (value.trim().length > 0) {
				value += ' ';
			}
			value += body;
			
			textArea.setValue(value);
			textArea.focus(false, true);
		}
	},
	
	handleNoteSaveClick: function(comp) {
		var chatWindow = comp.up(this.windowXtype);
		var noteArea = comp.up().up().down('textareafield');
		var body = noteArea.getValue().trim();
		
		if (body.length > 0) {
			// Save note to server
			/**
			 * <iq id="ci12X-75" to="kemas@workgroup.kemass-macbook-air.local" type="set">
			 * 	<chat-notes xmlns="http://jivesoftware.com/protocol/workgroup">
			 *   <sessionID>5ksmfn1922</sessionID>
			 *   <notes>Halooo ini nyoba note</notes>
			 *  </chat-notes>
			 * </iq>
			 */
			WGMessenger_GLOBAL.setTimedLoading(comp);
			
			var roomId = Strophe.getNodeFromJid(chatWindow.jid);
			var iqId = StropheBackend.getUniqueId();
			var noteIq = $iq({
				id: iqId,
				to: StropheBackend.workgroupFullJid,
				type: 'set'
			}).c('chat-notes', { xmlns: 'http://jivesoftware.com/protocol/workgroup' }).
			c('sessionID', {}, roomId).
			c('notes', {}, body);
			
			WGMessenger_GLOBAL.messageBus.addListener(
				iqId,
				function(packet) { 
					comp.setLoading(false); 
					
					var $packet = $(packet);
				
					if ($packet.attr('type') === 'result') {
						Ext.example.msg('Notes Saved', 'Notes successfully saved to server.');
					}
				},
				this,
				{ single: true }
			);
			
			StropheBackend.send(noteIq);
		}
	}
});