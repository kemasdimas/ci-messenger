/**
 * Main.js
 * 
 * Controller to operate main panel & login panel functionality
 */
Ext.define('WG.controller.Main', {
    extend: 'Ext.app.Controller',

	views: [
		'LoginPanel',
		'MainPanel',
		'ChangePasswordWindow',
		'PreferencesWindow'
	],
    
    stores: [
    	'Accounts'
    ],
    
    init: function() {
    	WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.ui.loginSuccess,
			this.handleLoginSuccess, 
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.ui.loginFailed,
			this.handleLoginFailed, 
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.ui.autoLogin,
			this.handleAutoLogin, 
			this
		);
		
    	this.control({
    		'loginpanel button[action=login]': {
    			click: this.handleLoginClick
    		},
    		
    		'loginpanel textfield': {
    			keypress: this.handleLoginFormKeypress
    		},
    		
    		'mainpanel button[action=logout]': {
    			click: this.handleLogoutClick
    		},
    		
    		'mainpanel #changepassword': {
    			click: this.handleChangePasswordClick
    		},
    		
    		'mainpanel #preferences': {
    			click: this.handlePreferencesClick
    		},
    		
    		'changepasswordwindow button[action=save]': {
    			click: this.handlePasswordSaveClick
    		}
    	});
    	
    	this.callParent(arguments);
    },
    
    destroy: function() {
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.ui.loginSuccess,
			this.handleLoginSuccess
		);
		
		WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.ui.loginFailed,
			this.handleLoginFailed
		);
    	
    	WGMessenger_GLOBAL.messageBus.removeListener(
			WGMessenger_GLOBAL.eventType.ui.autoLogin,
			this.handleAutoLogin
		);
    	
    	this.callParent(arguments);
    },
    
    /**
     * Show password change window
     */
	handleChangePasswordClick: function(comp) {
		var existingPasswordWindow = Ext.ComponentManager.get('change-password-window');
		
		if (existingPasswordWindow) {
			existingPasswordWindow.focus(false, false);
		} else { 
			Ext.widget('changepasswordwindow');
		}
	},
	
	/**
	 * Show user preferences window
	 */
	handlePreferencesClick: function(comp) {
		var existingPrefWindow = Ext.ComponentManager.get('single-user-preferences');
		
		if (existingPrefWindow) {
			existingPrefWindow.focus(false, true);
		} else {
			Ext.widget('preferenceswindow');	
		}
	},
	
	/**
	 * Save password change to server and notify user
	 */
   	handlePasswordSaveClick: function(comp) {
   		var passWindow = comp.up('window');
   		var form = comp.up('window').down('form').getForm();
   		
   		if (form.isValid()) {
   			var values = form.getValues();
   			var password = values.password;
   			
   			/**
   			 * <iq id="edih0-84" to="kemass-macbook-air.local" type="set">
   			 * 	<query xmlns="jabber:iq:register">
   			 * 	 <username>ciba-kemas</username>
   			 *   <password>ciba-kemas</password>
   			 *  </query>
   			 * </iq>
   			 */
   			var domain = Strophe.getDomainFromJid(StropheBackend.connection.jid);
   			var username = Strophe.getNodeFromJid(StropheBackend.connection.jid);
   			var passIqId = StropheBackend.getUniqueId();
   			var passIq = $iq({
   				id: passIqId,
   				to: domain,
   				type: 'set'
   			}).c('query', { xmlns: 'jabber:iq:register' }).
   			c('username', {}, username).
   			c('password', {}, password);
   			
   			WGMessenger_GLOBAL.messageBus.addListener(
				passIqId,
				function() {
					Ext.example.msg('Password Changed', 'Password successfully changed.');
					this.close();
				}, 
				passWindow,
				{single: true}
			);
   			
   			StropheBackend.send(passIq);
   			WGMessenger_GLOBAL.setTimedLoading(passWindow);
   		}
   	},
   	
   	/**
   	 * Save login data to cookie
   	 * 
   	 * @param wgname string workgroup name
   	 * @param username string username
   	 * @param password string password
   	 * @param savepassword string on / off, determine to save password in cookie
   	 * @param showtutorial string on/ off, determine to show / hide tutorial
   	 */
   	saveLoginToCookie: function(wgname, username, password, savepassword, showtutorial) {
   		$.cookie('wgchat-wgname', wgname, { expires: 365 });
   		$.cookie('wgchat-username', username, { expires: 365 });
   		$.cookie('wgchat-savepass', savepassword === 'on', { expires: 365 });
   		$.cookie('wgchat-showtutorial', showtutorial === 'on', { expires: 365 });
   		
   		if (savepassword === 'on') {
   			$.cookie('wgchat-passwd', password, { expires: 365 });
		} else {
			$.cookie('wgchat-passwd', null);
		}
		
		if (showtutorial === 'on') {
			// Reset the doneTutorials cookie
			WGMessenger_GLOBAL.isTutorialEnabled = true;
			$.cookie('wgchat-donetutorials', null);
		} else {
			WGMessenger_GLOBAL.isTutorialEnabled = false;
			
			var doneTutorials = Ext.JSON.decode($.cookie('wgchat-donetutorials'));
			WGMessenger_GLOBAL.doneTutorials = doneTutorials || [];
		}
		
		// Save login credential to accounts cookie
		var credId = username + '-' + wgname;
		var credential = {
			id: credId,
			workgroup: wgname,
			username: username,
			password: $.cookie('wgchat-passwd')
		};
		
		var savedCredentials = Ext.JSON.decode($.cookie('wgchat-savedcreds')) || { };
		savedCredentials[credId] = credential;
		
		$.cookie('wgchat-savedcreds',  Ext.JSON.encode(savedCredentials), 365);
   	},
   	
   	_processLoginForm: function(form) {
   		var values = form.getValues();
    	
		// sometimes user tends to enter usename which includes workgroupname,
		// just like admin-workgroupname. We need to address this usability
		// question issue
		var workgroupname = values.workgroupname;
		var username = values.username;
		
		var splittedUsername = username.split('-', 2);
	
		if (splittedUsername.length > 1) {
			username = splittedUsername[0];
			workgroupname = splittedUsername[1];
		}
		
		return {
			username: username,
			workgroupname: workgroupname,
			password: values.password,
			savepassword: values.savepassword,
			showtutorial: values.showtutorial
		};
   	},
   	
   	handleLoginSuccess: function() {
   		var loginPanel = Ext.getCmp('login-panel');
   		
   		if (loginPanel) {
   			// Save workgroupname and username to cookie
   			var form = loginPanel.down('form').getForm();
   			var values = this._processLoginForm(form);
   			
   			this.saveLoginToCookie(values.workgroupname, values.username, values.password, values.savepassword, values.showtutorial);
   			
   			var mainContainer = loginPanel.up('#main-container');
   			mainContainer.removeAll(true);
   			
   			mainContainer.add(Ext.create('WG.view.MainPanel'));
   			
   			// if this is demo workgroup show additional info about demo page
   			try {
   				Ext.example.msg('DEMO Notification', 'You can try to chat as a customer to your demo username (' + demo_GLOBAL.username_demo + ') by visiting this <a href="http://www.wgchat.com/demo#customer" target="blank">WGchat Demo Page Info</a>, please <b>do not</b> close WG Messenger tab / window to continue demo. <br /><br />Click here to hide this popup', -1);	
			} catch (e) {}
   		} else {
   			// Reset features & notification
   			WGMessenger_GLOBAL.addPageTitleNotification(WGMessenger_GLOBAL.notificationCount * -2);
   			WGMessenger_GLOBAL.features = { };
   			WGMessenger_GLOBAL.isRelogin = false;
   			
   			// Clear msg list
   			Ext.example.clearMsg();
   			
   			// Reloggin state
   			var mainPanel = Ext.getCmp('main-panel');
   			
   			if (mainPanel) {
   				var mainContainer = mainPanel.up('#main-container');
	   			mainContainer.removeAll(true);
	   			
	   			mainContainer.add(Ext.create('WG.view.MainPanel'));
   			}
   		}
   	},
   
   	handleLoginFailed: function(status) {
   		Ext.example.msg(status.title, status.text, status.delay);
   		
   		var loginPanel = Ext.getCmp('login-panel');
   		
   		if (loginPanel) {
   			var loginButton = loginPanel.down('button[action=login]');
   			loginButton.setLoading(false);
   		}
   	},
   	
   	handleLoginFormKeypress: function(comp, evt) {
    	var key = evt.getKey();
    				
		if (key == 13) {
			evt.preventDefault();
			evt.stopPropagation();
			
			var button = comp.up('form').down('button[action=login]');
			button.fireEvent('click', button);
		}
    },
   	
    handleLoginClick: function(comp) {
    	var form = comp.up('form').getForm();
    	
    	if (form.isValid()) {
    		WGMessenger_GLOBAL.setTimedLoading(comp, 'Logging In...', -1);
    		
    		var values = this._processLoginForm(form);
    	
    		xmppApi.login(values.username, values.workgroupname, values.password);
    	}
   	},
   	
   	handleLogoutClick: function(comp) {
   		var me = this;
   		
   		if (WGMessenger_GLOBAL.notificationCount !== 0) {
   			var cofirmation = Ext.Msg.confirm(
				'Unread Message(s)', 
				'You have unread message(s), are You sure to log out?', 
				function(answer) {
					if (answer === 'yes') {
						me._logoutUser();
					}
	    	});
   		} else {
   			me._logoutUser();
   		}
   	},
   	
   	handleAutoLogin: function(interval) {
   		var pbar = Ext.create('Ext.ProgressBar', {
   		});
   		
   		var waitWindow = Ext.create('Ext.window.Window', {
   			autoShow: true,
   			title: 'Auto Login',
   			iconCls: 'wgchat-icon-login',
   			
   			layout: 'fit',
   			width: 200,
   			bodyPadding: 5,
   			closable: false,
   			modal: true,
   			
   			items: [
   				pbar
   			]
   		});
   		
   		var me = this;
		pbar.wait({
            interval: 1000,
            duration: interval,
            increment: interval / 1000,
            fn: function() {
            	me.reLogin.call(me);
            	
            	waitWindow.close();
            }
        });
   	},
   	
   	reLogin: function() {
   		Ext.example.clearMsg();
   		WGMessenger_GLOBAL.isRelogin = true;
   		
   		var password = StropheBackend.password;
   		var workgroupName = StropheBackend.workgroup;
   		var username = StropheBackend.username;
   		
   		xmppApi.login(username, workgroupName, password);
   		
   		Ext.example.msg('Retry to Login', 'Retrying to login, please wait...', -1);
   	},
   	
   	/**
   	 * Log user out from WGchat
   	 */
   	_logoutUser: function() {
   		WGMessenger_GLOBAL.isLoggingOut = true;
   		xmppApi.logout();
   		
   		var pbar = Ext.create('Ext.ProgressBar', {
   		});
   		
   		var waitWindow = Ext.create('Ext.window.Window', {
   			autoShow: true,
   			title: 'Logging Out',
   			iconCls: 'wgchat-icon-logout',
   			
   			layout: 'fit',
   			width: 200,
   			bodyPadding: 5,
   			closable: false,
   			modal: true,
   			
   			items: [
   				pbar
   			]
   		});
   		
		pbar.wait({
            interval: 100,
            duration: 90000,
            increment: 20,
        });
        
        Ext.defer(function() {
            window.location.reload();
        }, 500);
   	}
});