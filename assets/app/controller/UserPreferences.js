Ext.define('WG.controller.UserPreferences', {
	extend: 'Ext.app.Controller',
	
	views: [ 
		'PreferencesWindow'
	],
	
	stores: [
		'Preferences'
	],
	
	models: [
		'Preference'
	],
	
	init: function() {
		
		this.control({
			'preferenceswindow': {
				show: this.handlePreferenceWindowShow
			},
			
			'preferenceswindow button[action=save]': {
				click: this.handleSaveClick
			}
		});
		
		this.callParent(arguments);
	},
	
	handlePreferenceWindowShow: function(comp) {
		var store = this.getPreferencesStore();
		store.load();
		
		var tempValues = {};
		store.each(function(record) {
			tempValues[record.get('key')] = record.get('value');
		}, this);
		
		var form = comp.down('form').getForm();
		form.setValues(tempValues);
	},
	
	handleSaveClick: function(comp) {
		var prefWindow = comp.up('window');
		var form = comp.up('window').down('form').getForm();
		
		if (form.isValid()) {
			var values = {};
			
			var fields = form.getFields();
			fields.each(function(field) {
				var value = field.getValue();
				
				values[field.name] = value;
			});
			
			this.getPreferencesStore().updatePreferences(values);
			
			prefWindow.close();
			
			Ext.example.msg('Preferences Saved', 'Preferences successfully saved');
		}
	}
});
