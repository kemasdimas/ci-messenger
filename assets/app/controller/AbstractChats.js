/**
 * AbstractChats.js
 * 
 * Base controller which is extended by Chats.js and AbstractConferenceChats.js
 * operate basic function of a chat window
 */
Ext.define('WG.controller.AbstractChats', {
    extend: 'Ext.app.Controller',

	stores: ['Rosters'],
	models: ['Roster'],
	views: [
		'Sidebar',
		'ChatsContainer',
		'chat.ChatMessagePanel'
	],
    
    getRosterFromStore: function(key, value) {
    	var store = Ext.getStore('Rosters');
    	
		if (store) {
			return store.findRecord(key, value)
		} else {
			return null;
		}
    },
    
    handlePresenceChange: Ext.emptyFn,
    
    handleIncomingMessage: Ext.emptyFn,
    
    sendMessage: Ext.emptyFn,
    
    handlePanelClose: Ext.emptyFn,
    
    init: function() {
    	this.control({
    		'basicchat textareafield': {
    			focus: function(comp) {
    				var chatwindow = comp.up().up().up();
    				
    				if (WGMessenger_GLOBAL.isWindowActive) {
    					chatwindow.resetUnreadMessages();	
    				}
    			}
    		},
    		
    		'basicchat': {
    			activate: function(comp) {
    				comp.down('chatmessagepanel').scrollToBottom();
    			}
    		}
    	});
    	
    	this.callParent(arguments);
    },
    
    handlePanelActivate: function(comp, opts) {
    	if (WGMessenger_GLOBAL.isWindowActive) { 
    		var textarea = comp.down('#chatarea');
			textarea.focus(false, true);
    	}
	},
    
    handleTextAreaKeypress: function(comp, evt){
    	var key = evt.getKey();
    				
		if (key == 13 && !evt.hasModifier()) {
			evt.preventDefault();
			evt.stopPropagation();
			
			var chatwindow = comp.up('basicchat');
			this.sendMessage(chatwindow);
		}
    },
    
    handleButtonSendClick: function(comp) {
		var chatwindow = comp.up().up().up();
		this.sendMessage(chatwindow);
	}
});