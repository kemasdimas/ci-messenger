/**
 * Tutorial.js
 * Controller to operate tutorial functionality
 */
Ext.define('WG.controller.Tutorial', {
    extend: 'Ext.app.Controller',

	views: [
		'LoginPanel',
		'MainPanel',
		'ChangePasswordWindow',
		'Sidebar'
	],
    
    spot: Ext.create('Ext.ux.Spotlight', {
    	easing: 'easeOut',
    	duration: 300
    }),
    
    eventQueue: [],
    
    isWaiting: false,
    
    init: function() {
    	WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.ui.queueTutorial,
			this.handleQueueTutorial, 
			this
		);
    	
    	this.callParent(arguments);
    },
    
    destroy: function() {
    	this.callParent(arguments);
    },
    
    handleQueueTutorial: function(tutorialId, title, text, callback, component) {
    	this.eventQueue.push({
    		tutorialId: tutorialId,
    		title: title,
    		text: text,
    		callback: callback,
    		component: component
    	});
    	
    	this.processNextTutorial();
    },
    
    processNextTutorial: function() {
    	if (this.isWaiting) {
    		return;
    	}
    	
    	var tutorial = this.eventQueue.shift();
    	
    	if (tutorial) {
    		this.isWaiting = true;
    		
    		var tutorialId = tutorial.tutorialId;
	    	var title = tutorial.title;
	    	var text = tutorial.text;
	    	var callback = tutorial.callback;
	    	var component = tutorial.component;
    		
    		var me = this;
	    	var spot = this.spot;
    		
    		if (this.isTutorialDone(tutorialId) || this.isTutorialDisabled(tutorialId)) {
    			me.isWaiting = false;
				me.processNextTutorial();
    		} else {
    			if (component && component.id) {
			    	Ext.defer(function() {
			    		if (typeof callback === 'function') {
				    		callback.call(component);
				    	}
			    		
			    		Ext.defer(function() {
			    			spot.show(component.id);
					    	
					    	var tutorialEndCaretaker = undefined;
							Ext.example.msg(title, text + '<span class="msg-right">(klik pop up ini untuk melanjutkan)&nbsp;&nbsp;|&nbsp;&nbsp;<a id="end-tutorial-link" href="javascript:void(0)">Akhiri tutorial?</a></span>', -1, function() {
								spot.hide();
								me.isWaiting = false;
								
								if (tutorialEndCaretaker) {
									tutorialEndCaretaker.unbind('click');
								}
								
								me.updateDoneTutorial.call(me, tutorialId);
								me.processNextTutorial.call(me);
							});
							
							tutorialEndCaretaker = $('#end-tutorial-link');
							tutorialEndCaretaker.one('click', function(evt) {
								
								// Disable the tutorial in cookie & in global var
								$.cookie('wgchat-showtutorial', false, { expires: 365 });
								WGMessenger_GLOBAL.isTutorialEnabled = false;
							});
							
			    		}, 750);
			    	}, 50);
		    	} else {
		    		throw 'Please supply valid component.';
		    	}		
    		}
    	}
    },
    
    /**
     * Check whether certain tutorialId is disabled / no,
     * enabled tutorial = tutorial outside newFeatures scope, regardless of showtutorial value
     * 
     * @param tutorialId string
     */
    isTutorialDisabled: function(tutorialId) {
    	var tutorialEnabled = WGMessenger_GLOBAL.isTutorialEnabled;
    	var tutorialInNewFeature = $.inArray(tutorialId, WGMessenger_GLOBAL.newFeatures) !== -1;
    	
    	return !(tutorialEnabled || tutorialInNewFeature);
    },
    
    /**
     * Check whether a certain tutorialId has been shown
     * 
     * @param tutorialId string
     */
    isTutorialDone: function(tutorialId) {
    	var tutorialInDone = $.inArray(tutorialId, WGMessenger_GLOBAL.doneTutorials) !== -1;
    	
    	return tutorialInDone;
    },
    
    /**
     * Save recently shown tutorial to donetutorials cookie
     * 
     * @param tutorialId string
     */
    updateDoneTutorial: function(tutorialId) {
    	WGMessenger_GLOBAL.doneTutorials.push(tutorialId);
    	
    	// push this to cookie
    	var doneTutorialsJson = Ext.JSON.encode(WGMessenger_GLOBAL.doneTutorials);
    	$.cookie('wgchat-donetutorials', doneTutorialsJson, { expires: 365 });
    }
});