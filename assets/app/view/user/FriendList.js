Ext.define('WG.view.user.FriendList', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.friendlist',
	
	store: 'Rosters',
	preventHeader: true,
	hideHeaders: true,
	border: 0,
    
	initComponent: function() {
		
		this.features = [
			Ext.create('Ext.grid.feature.Grouping', {
				border: 0,
				groupHeaderTpl: '{[values.name == true ? "Online" : "Offline"]} ({rows.length})'
			})
		];
		
		this.columns = [
			{
				header: 'Friend List', 
				dataIndex: 'name', 
				flex: 1,
				renderer: function(value, opts, record) {
					return Ext.String.format(
						'<div class="wgchat-roster-item" data-jid="{0}">' +
							'<span class="status status-{1}">&nbsp;</span>' +
							'<span class="name">{2}</span>' +
						'</div>'
					, record.data.jid, record.data.status, value);
				}
			}
		];
		
		this.callParent(arguments);
	}
});