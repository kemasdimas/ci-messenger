Ext.define('WG.view.user.CheckableFriendList', {
	extend: 'Ext.window.Window',
	alias: 'widget.checkablefriendlist',
	
	width: 300,
	height: 250,
	autoScroll: true,
	
	buttons: [
		{
			text: 'Invite',
			action: 'invitenow'
		},
		{
			text: 'Cancel',
			action: 'invitecancel'
		}
	],
	
	initComponent: function() {
		
		this.items = [
			{
				xtype: 'friendlist',
				selModel: Ext.create('Ext.selection.CheckboxModel')
			}
		];
		
		this.callParent(arguments);
	}
});