Ext.define('WG.view.user.RosterContextMenu', {
	extend: 'Ext.menu.Menu',
	alias: 'widget.rostercontextmenu',
	record: {},
	
	items: [
		{
			text: 'Send Message',
			action: 'sendmessage',
			iconCls: 'wgchat-icon-bell'
		},
		{
			text: 'Initiate Conference',
			action: 'initiateconference'
		}
	]
});
