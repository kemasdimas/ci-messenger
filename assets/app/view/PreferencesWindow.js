Ext.define('WG.view.PreferencesWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.preferenceswindow',
	
	autoShow: true,
	width: 300,
	resizable: false,
	
	title: 'User Preferences',
	id: 'single-user-preferences',
	
	initComponent: function() {
		
		this.buttons = [
			{
				text: 'Save',
				action: 'save',
				iconCls: 'wgchat-icon-save'
			},
			{
				text: 'Cancel',
				handler: this.close,
				scope: this
			}
		];
		
		this.items = [
			{
				layout: 'anchor',
				xtype: 'form',
				bodyPadding: 5,
				
				defaults: {
					enableKeyEvents: true,
					xtype: 'textfield',
				},
				
				fieldDefaults: {
					layout: 'anchor',
					anchor: '100%',
					labelWidth: 200,
				},
				
				items: [
					{
						xtype: 'checkbox',
						fieldLabel: 'Auto Accept Chat Request',
						name: 'acceptChatRequest',
						checked: true,
						
						listeners: {
						    render: function(c) {
						        Ext.QuickTips.register({
						            target: c.getEl(),
						            text: 'Auto accept incoming customer chat request'
						        });
						    }
						}
					},
					// {
						// xtype: 'checkbox',
						// fieldLabel: 'Store Internal Chat History',
						// name: 'storeInternalChat',
						// id : 'storeInternalChat',
// 						
						// listeners: {
						    // render: function(c) {
						        // Ext.QuickTips.register({
						            // target: c.getEl(),
						            // text: 'Store internal chat history in your browser'
						        // });
						    // }
						// }
					// },
				]
			}
		];
		
		this.callParent(arguments);
	}
});
