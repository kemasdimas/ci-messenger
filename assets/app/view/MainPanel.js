Ext.define('WG.view.MainPanel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.mainpanel',
	
	id: 'main-panel',
	layout: 'border',
	title: 'WG Messenger',
	iconCls: 'wgchat-favicon',
	border: 0,
	
	defaults: {
		split: true
	},
	
	initComponent: function() {
		this.items = [
			{
				region: 'center',
				xtype: 'chatscontainer',
				id: 'chatscontainer'
			},
			{
				region: 'west',
				width: 200,
				minWidth: 200,
				collapsible: true,
				id: 'main-sidebar',
				
				xtype: 'sidebar'
			}
		];
		
		var currentUserMenu = [{
			text: 'Preferences',
    		iconCls: 'wgchat-icon-preferences',
    		itemId: 'preferences'
		}];
		
		// Removing the preference menu item
		// currentUserMenu = [];
		
		if (!WGMessenger_GLOBAL.isDemoAccount()) {
			currentUserMenu.push({
        		text: 'Change Password',
        		iconCls: 'wgchat-icon-lock',
        		itemId: 'changepassword'
        	});
		}
		
		this.tbar = {
			items: [
				{
		            text : Strophe.getNodeFromJid(StropheBackend.connection.jid),
		            iconCls: 'wgchat-icon-user',
		            menu: currentUserMenu
		        },
		        '-',
				{
					text: 'Broadcast Message',
					action: 'sendbroadcastmessage',
					iconCls: 'wgchat-icon-broadcast',
					tooltip: '<b>Broadcast</b><br/>Broadcast message to Internal Group',
		        },
		        '-',
		        {
		        	text: 'Help',
					action: 'help',
					iconCls: 'wgchat-icon-help',
					tooltip: '<b>Help</b><br/>See WG Messenger Web help documentation',
					href: 'https://wgchat.uservoice.com/knowledgebase/topics/12084-wg-messenger-web',
		        },
		        // begin using the right-justified button container
		        '->', // same as { xtype: 'tbfill' }
		        '-',
		        {
		        	text: 'Logout',
		        	action: 'logout',
		        	iconCls: 'wgchat-icon-logout',
		        	tooltip: '<b>Logout</b><br/>Logout from WG Messenger',
		        }
			]
		};
		
		this.callParent(arguments);
	}
});