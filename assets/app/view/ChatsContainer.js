Ext.define('WG.view.ChatsContainer', {
	extend: 'Ext.tab.Panel',
	alias: 'widget.chatscontainer',
	
	preventHeader: true,
	itemId: 'chatsContainer',
	activeTab: 0,
	plugins: [{
        ptype: 'tabscrollermenu',
        maxText  : 15,
        pageSize : 5
    }],
	
	initComponent: function() {	
		// this.items = [
			// {
				// title: 'WGchat Info'
			// }
		// ];
		
		this.callParent(arguments);
	},
	
	startChat: function(jid, name, title, status, isOnline) {
		// Check whether name == null
		if (!name) {
			name = 'System Admin';
			title = name;
		}
		
		var chatwindow = Ext.ComponentManager.get('chatwindow-basic-' + jid);
		if (!chatwindow) {
			chatwindow = this.add({
				title: title,
				xtype: 'basicchat',
				jid: jid,
				friendName: name,
				id: 'chatwindow-basic-' + jid
			});
			
			if (!isOnline) {
				chatwindow.notificationReceived(name + ' is offline, message will be delivered when the user is online.');
			}	
		}
		
		this.setActiveTab(chatwindow);
		
		return chatwindow;
	},
	
	startConference: function(jid, name) {
		var chatwindow = Ext.ComponentManager.get('chatwindow-conf-' + jid);
		if (!chatwindow) {
			chatwindow = this.add({
				title: 'Conference with ' + name,
				xtype: 'conferencechat',
				jid: jid,
				id: 'chatwindow-conf-' + jid
			});
		}
		
		this.setActiveTab(chatwindow);
		
		return chatwindow;
	},
	
	startWorkgroupConference: function(jid, name, config) {
		var chatwindow = Ext.ComponentManager.get('chatwindow-wgconf-' + jid);
		if (!chatwindow) {
			chatwindow = this.add(Ext.merge({
				title: name,
				xtype: 'workgroupchat',
				jid: jid,
				id: 'chatwindow-wgconf-' + jid
			}, config));
		}
		
		this.setActiveTab(chatwindow);
		
		return chatwindow;
	}
});