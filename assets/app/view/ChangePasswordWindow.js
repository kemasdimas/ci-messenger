Ext.define('WG.view.ChangePasswordWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.changepasswordwindow',
	
	autoShow: true,
	width: 250,
	layout: 'fit',
	title: 'Change Password',
	id: 'change-password-window',
	
	initComponent: function() {
		
		this.items = [
			{
				layout: 'anchor',
				xtype: 'form',
				bodyPadding: 5,
				
				defaults: {
					xtype: 'textfield',
					inputType: 'password',
					layout: 'anchor',
					anchor: '100%',
				},
				
				fieldDefaults: {
					labelAlign: 'top',
					allowBlank: false,
					minLength: 5
				},
				
				items: [
					{
						fieldLabel: 'New Password',
						name: 'password',
						itemId: 'password'
					},
					{
						fieldLabel: 'Confirm Password',
						name: 'confirmpassword',
						vtype: 'password',
						initialPassField: 'password'
					}
				]
			}
		];
		
		this.buttons = [
			{
				text: 'Save',
				action: 'save',
				iconCls: 'wgchat-icon-save'
			},
			{
				text: 'Cancel',
				handler: this.close,
				scope: this
			}
		];
		
		this.callParent(arguments);
	}
});
