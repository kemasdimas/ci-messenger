Ext.define('WG.view.Sidebar', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.sidebar',
	
	layout: 'accordion',
	title: 'Sidebar',
	autoScroll: true,
	
	initComponent: function() {
		this.defaults = {
			autoScroll: true,
			layout: 'fit',
		};
		this.items = [
			{
				xtype: 'panel',
				title: 'Internal Group',
				iconCls: 'wgchat-icon-users',
				id: 'internal-group',
				
				items: [
					{
						xtype: 'friendlist'
					}
				],
				
				listeners: {
					afterrender: {
						fn: function(comp) {
							WGMessenger_GLOBAL.messageBus.fireEvent(
								WGMessenger_GLOBAL.eventType.ui.queueTutorial,
								'internal-group',
								
								'Internal Group',
								
								'Menampilkan semua anggota workgroup, Anda dapat: ' + 
								'<ul>' + 
									'<li>Klik ganda pada salah satu member untuk memulai percakapan internal</li>' + 
									'<li>Klik kanan pada salah satu member untuk menampilkan menu</li>' + 
									'<li>Pilih menu "Initiate Conference" untuk memulai konferensi dengan member tertentu</li>' +
								'</ul>',
								
								function() {
									comp.expand();
								},
								
								comp
							);
						}
					}
				}
			}
		];
		
		this.callParent(arguments);
	}
});