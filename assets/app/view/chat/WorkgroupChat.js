Ext.define('WG.view.chat.WorkgroupChat', {
	extend: 'WG.view.chat.BasicChat',
	alias: 'widget.workgroupchat',
	
	metadatas: [],
	participantsCount: 0,
	justOpened: true,
	
	initComponent: function() {
		var me = this;
		this.items = [
			{
				region: 'east',
				minWidth: 200,
				maxWidth: 200,
				collapsible: true,
				
				preventHeader: false, 
				title: 'User Chat Tool',
				layout: 'accordion',
				
				defaults: {
					border: 0,
					preventHeader: false,
					layout: 'fit',
					autoScroll: true
				},
				
				items: [
					{
						title: 'User Information',
						iconCls: 'wgchat-icon-userinfo',
						xtype: 'metadatalist',
						store: Ext.create('Ext.data.Store', {
							model: 'WG.model.Metadata',
							data: me.metadatas
						}),
						
						listeners: {
							afterrender: {
								fn: function(comp) {
									WGMessenger_GLOBAL.messageBus.fireEvent(
										WGMessenger_GLOBAL.eventType.ui.queueTutorial,
										'wgchat-user-information',
										
										'User Information',
										
										'Menampilkan detail informasi data pelanggan',
										
										function() {
											var chatWindow = comp.up('workgroupchat');
											var chatContainer = chatWindow.up('chatscontainer');
											
											chatContainer.setActiveTab(chatWindow);
											
											comp.expand(false);
										},
										
										comp
									);
								}
							}
						}
					},
					{
						title: 'Chat Categories',
						iconCls: 'wgchat-icon-category',
						xtype: 'chatcategorylist',
						listeners: {
							afterrender: {
								fn: function(comp) {
									WGMessenger_GLOBAL.messageBus.fireEvent(
										WGMessenger_GLOBAL.eventType.ui.queueTutorial,
										'wgchat-chat-categories',
										
										'Chat Categories',
										
										'Menampilkan kategori dalam workgroup, Anda dapat: ' + 
										'<ul>' + 
											'<li>Memilih satu atau lebih kategori dan menekan tombol "Save" untuk mengelompokkan percakapan ini dalam kategori tertentu</li>' +
										'</ul>',
										
										function() {
											var chatWindow = comp.up('workgroupchat');
											var chatContainer = chatWindow.up('chatscontainer');
											
											chatContainer.setActiveTab(chatWindow);
											
											comp.expand(false);
										},
										
										comp
									);
								}
							}
						}
					},
					{
						title: 'Chat Notes',
						iconCls: 'wgchat-icon-edit',
						tbar: [
							{
								text: 'Save',
								iconCls: 'wgchat-icon-save',
								action: 'notesave'
							}
						],
						items: [
							{
								xtype: 'textareafield',
								name: 'notearea',
								emptyText: 'Enter chat notes here'
							}
						],
						listeners: {
							afterrender: {
								fn: function(comp) {
									WGMessenger_GLOBAL.messageBus.fireEvent(
										WGMessenger_GLOBAL.eventType.ui.queueTutorial,
										'wgchat-chat-notes',
										
										'Chat Notes',
										
										'Menambahkan catatan pada percakapan ini, gunakan fitur ini untuk melengkapi detail permasalahan yang dialami pelanggan',
										
										function() {
											var chatWindow = comp.up('workgroupchat');
											var chatContainer = chatWindow.up('chatscontainer');
											
											chatContainer.setActiveTab(chatWindow);
											
											comp.expand(false);
										},
										
										comp
									);
								}
							}
						}
					},
					{
						title: 'Participants',
						iconCls: 'wgchat-icon-users',
						xtype: 'roomparticipantlist',
						listeners: {
							afterrender: {
								fn: function(comp) {
									WGMessenger_GLOBAL.messageBus.fireEvent(
										WGMessenger_GLOBAL.eventType.ui.queueTutorial,
										'wgchat-chat-participants',
										
										'Customer Chat Participants',
										
										'Menampilkan semua peserta dalam percakapan ini, Anda dapat: ' + 
										'<ul>' + 
											'<li>Menekan tombol "Invite Other" untuk mengundang customer service lain dalam satu workgroup</li>' +
											'<li>Klik ganda pada salah satu peserta untuk melakukan percakapan tersendiri (hanya berlaku untuk peserta customer service)</li>' +
										'</ul>',
										
										function() {
											var chatWindow = comp.up('workgroupchat');
											var chatContainer = chatWindow.up('chatscontainer');
											
											chatContainer.setActiveTab(chatWindow);
											
											comp.expand(false);
										},
										
										comp
									);
								}
							}
						}
					},
					{
						title: 'Customer Histories',
						iconCls: 'wgchat-icon-history',
						xtype: 'chathistorylist',
						tbar: null,
						store: Ext.create('Ext.data.Store', {
							model: 'WG.model.UserChatTranscript',
							sorters: [
						        {
						            property : 'date',
						            direction: 'DESC'
						        }
						    ],
						}),
						columns: [
					        { header: 'Chat With',  dataIndex: 'agentNames', flex: 1 },
					        { header: 'Time', dataIndex: 'date', flex: 1, renderer: function(value) {
					        	return Ext.Date.format(value, 'Y-m-d H:i');
					        } }
					    ],
					    
					    listeners: {
							afterrender: {
								fn: function(comp) {
									WGMessenger_GLOBAL.messageBus.fireEvent(
										WGMessenger_GLOBAL.eventType.ui.queueTutorial,
										'wgchat-user-chat-histories',
										
										'Customer Histories',
										
										'Menampilkan riwayat percakapan pelanggan tertentu dengan semua customer service, Anda dapat: ' + 
										'<ul>' + 
											'<li>Klik ganda pada salah satu riwayat untuk menampilkan detail riwayat</li>' +
										'</ul>',
										
										function() {
											var chatWindow = comp.up('workgroupchat');
											var chatContainer = chatWindow.up('chatscontainer');
											
											chatContainer.setActiveTab(chatWindow);
											
											comp.expand(false);
										},
										
										comp
									);
								}
							}
						}
					}
				]
			}
		];
		
		this.chatToolbar = [
			{
				text: 'Transfer Chat',
				iconCls: 'wgchat-icon-transfer',
				action: 'transferchat',
				listeners: {
					afterrender: {
						fn: function(comp) {
							WGMessenger_GLOBAL.messageBus.fireEvent(
								WGMessenger_GLOBAL.eventType.ui.queueTutorial,
								'wgchat-toolbar-transfer',
								
								'Transfer Chat',
								
								'Transfer percakapan ini ke customer service lain dalam workgroup yang sama',
								
								function() {
									var chatWindow = comp.up('workgroupchat');
									var chatContainer = chatWindow.up('chatscontainer');
									
									chatContainer.setActiveTab(chatWindow);
								},
								
								comp
							);
						}
					}
				}
			},
			{
				text: 'Invite Agent',
				iconCls: 'wgchat-icon-useradd',
				action: 'inviteagent',
				listeners: {
					afterrender: {
						fn: function(comp) {
							WGMessenger_GLOBAL.messageBus.fireEvent(
								WGMessenger_GLOBAL.eventType.ui.queueTutorial,
								'wgchat-toolbar-invite',
								
								'Invite Agent',
								
								'Mengundang customer service lain untuk membantu Anda, customer service yang menyetujui undangan akan muncul pada tab "Participants"',
								
								function() {
									var chatWindow = comp.up('workgroupchat');
									var chatContainer = chatWindow.up('chatscontainer');
									
									chatContainer.setActiveTab(chatWindow);
								},
								
								comp
							);
						}
					}
				}
			},
			{
				text: 'Message Template',
				iconCls: 'wgchat-icon-entries',
				action: 'messagetemplate',
				menu: Ext.create('WG.view.workgroup.ChatMacroMenu'),
				listeners: {
					afterrender: {
						fn: function(comp) {
							WGMessenger_GLOBAL.messageBus.fireEvent(
								WGMessenger_GLOBAL.eventType.ui.queueTutorial,
								'wgchat-toolbar-template',
								
								'Message Template',
								
								'Kumpulan template jawaban percakapan untuk mempercepat pelayanan Anda, pastikan meminta Admin workgroup untuk menambahkan Global Template',
								
								function() {
									var chatWindow = comp.up('workgroupchat');
									var chatContainer = chatWindow.up('chatscontainer');
									
									chatContainer.setActiveTab(chatWindow);
								},
								
								comp
							);
						}
					}
				}
			},
			'->',
			{
				text: 'End Chat',
				iconCls: 'wgchat-icon-delete',
				action: 'endchat',
				listeners: {
					afterrender: {
						fn: function(comp) {
							WGMessenger_GLOBAL.messageBus.fireEvent(
								WGMessenger_GLOBAL.eventType.ui.queueTutorial,
								'wgchat-toolbar-end',
								
								'End Chat',
								
								'Mengakhiri percakapan ini dan secara otomatis mengirimkan pesan penutup ke pelanggan',
								
								function() {
									var chatWindow = comp.up('workgroupchat');
									var chatContainer = chatWindow.up('chatscontainer');
									
									chatContainer.setActiveTab(chatWindow);
								},
								
								comp
							);
						}
					}
				}
			}
		];
		
		this.callParent(arguments);
	}
});