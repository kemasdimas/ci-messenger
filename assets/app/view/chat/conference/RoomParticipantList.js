Ext.define('WG.view.chat.conference.RoomParticipantList', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.roomparticipantlist',
	
	preventHeader: true,
	hideHeaders: true,
    
    tbar: [
    	{
    		text: 'Invite Other',
    		iconCls: 'wgchat-icon-useradd',
    		action: 'inviteother'
    	}
    ],
    
	initComponent: function() {
		this.columns = [
			{
				header: 'User', 
				dataIndex: 'name', 
				flex: 1,
				renderer: function(value, opts, record) {
					return Ext.String.format(
						'<div class="wgchat-roster-item" data-jid="{0}">' +
							'<span class="status status-{1}">&nbsp;</span>' +
							'<span class="name">{2}</span>' +
						'</div>'
					, record.data.jid, record.data.status, value);
				}
			}
		];
		
		this.store = Ext.create('Ext.data.Store', {
			model: 'WG.model.Roster',
			data: [
			]
		});
		
		this.callParent(arguments);
	}
});