Ext.define('WG.view.chat.conference.ConferenceInvitation', {
	extend: 'Ext.window.Window',
	alias: 'widget.conferenceinvitation',
	
	title: 'Conference Invitation',
	width: 300,
	layout: 'fit',
	closable: false,
	resizable: false,
	bodyPadding: 5,
	
	tpl: new Ext.XTemplate(
		'<p>From: {name}</p>',
		'<p>{reason}</p>'
	),
	
	buttons: [
		{
			text: 'Accept',
			action: 'accept',
			iconCls: 'wgchat-icon-accept'
		},
		{
			text: 'Reject',
			action: 'reject',
			iconCls: 'wgchat-icon-delete'
		}
	],
	
	initComponent: function() {
		var me = this;
		var notif = undefined;
		this.listeners = {
			show: function() {
				notif = WGMessenger_GLOBAL.showDesktopNotification('Conference Invitation',  me.inviterName + ' says ' + me.reason);
			},
			
			destroy: function() {
				if (notif) {
					notif.cancel();
				}
			}
		};
				
		this.callParent(arguments);
	}
});
