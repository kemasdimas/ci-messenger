Ext.define('WG.view.chat.BasicChat', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.basicchat',
	
	layout: 'border',
	defaults: {
		split: true,
		preventHeader: true,
	},
	closable: true,
	unreadMessages: 0,
	
	// Friend JID
	jid: '',
	
	// Friend nickname
	friendName: '',
	
	// Chat Toolbar
	chatToolbar: [
		{
			xtype: 'button',
			text: 'Buzz',
			action: 'buzz',
			iconCls: 'wgchat-icon-bell'
		},
		{
			xtype: 'button',
			text: 'Initiate Conference',
			action: 'initiateconference',
			iconCls: 'wgchat-icon-conference'
		}
	],
	
	// Sound used as a message notification
	MSG_SOUND_ID: 'incomingchat',
	BUZZ_SOUND_ID: 'buzz',
	
	initComponent: function() {
		this.listeners = {
			beforeclose: {
				fn: this.resetUnreadMessages,
				scope: this
			}
		};
		
		this.callParent(arguments);
		
		var me = this;
		this.add([
			{
				region: 'center',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				
				defaults: {
					border: 0
				},
				
				items: [
					{
						xtype: 'chatmessagepanel',
						flex: 1,
						bbar: me.chatToolbar,
					},
					{
						layout: 'hbox',
						align: 'stretch',
						height: 40,
						border: 1,
						
						items: [
							{
								flex: 10,
								xtype: 'textareafield',
								enableKeyEvents: true,
								grow: false,
								maxHeight: 40,
								itemId: 'chatarea',
								anchor: '100%',
								emptyText: 'Type your message here...'
							},
							{
								width: 50,
								xtype: 'button',
								action: 'send',
								text: 'Send',
								scale: 'large'
							}
						]
					}
				]
			}
		]);
	},
	
	tabNotification: function(soundId, desktopNotifTitle, desktopNotifText) {
		var tabPanel = this.up();
		var self = this;
		
		if (!WGMessenger_GLOBAL.isWindowActive || tabPanel.getActiveTab() !== this) {
			this.unreadMessages++;
		
			if (this.unreadMessages > 0) {
				// play sound...
				soundManager.onready(function() {
					soundManager.play(soundId);
				});
				
				var title = this.title;
				this.setTitle('(' + this.unreadMessages + ') ' + this.initialConfig.title);
				this.tab.addCls('x-tab-unread');
				
				WGMessenger_GLOBAL.addPageTitleNotification(1);
				
				if (desktopNotifTitle && desktopNotifText) {
					WGMessenger_GLOBAL.showDesktopNotification(desktopNotifTitle, desktopNotifText, 15000)
				}
			}	
		}
	},
	
	notificationReceived: function(notif) {
		var msgPanel = this.down('chatmessagepanel');
		msgPanel.notificationReceived(notif);
	},
	
	buzzReceived: function(from) {
		var notif = (from === this.jid ? this.friendName : from) + ' wants your attention!';
		
		this.notificationReceived(notif);
		this.tabNotification(this.BUZZ_SOUND_ID, 'Buzz!!!', notif);
	},
	
	messageReceived: function(from, time, body, isMe) {
		if (body.trim().length == 0) {
			return;
		}
		
		// Append message to messages list
		var name = (from === this.jid ? this.friendName : from);
		
		var msgPanel = this.down('chatmessagepanel');
		msgPanel.messageReceived(name, time, body, isMe);
		
		this.tabNotification(this.MSG_SOUND_ID, name, '(' + time + ') ' + body);
	},
	
	resetUnreadMessages: function() {
		WGMessenger_GLOBAL.addPageTitleNotification(this.unreadMessages * -1);
		this.unreadMessages = 0;
		
		this.setTitle(this.initialConfig.title);
		this.tab.removeCls('x-tab-unread');
	},
	
	setFriendName: function(name) {
		this.friendName = name;
		this.setTitle(name);
	}
});