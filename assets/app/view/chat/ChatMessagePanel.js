Ext.define('WG.view.chat.ChatMessagePanel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.chatmessagepanel',
	
	bodyPadding: 5,
	autoScroll: true,
	
	initComponent: function() {
		this.data =  [];
		this.tpl = WGMessenger_GLOBAL.template.chatAreaTemplate;
		
		this.callParent(arguments);
	},
	
	clearData: function() {
		this.update([]);
		this.scrollToBottom();
	},
	
	notificationReceived: function(notif) {
		var data = this.data || [];
		data.push({
			notif: notif
		});
		
		this.update(data);
		this.scrollToBottom();
	},
	
	messageReceived: function(from, time, body, isMe) {
		if (body.trim().length == 0) {
			return;
		}
		
		var data = this.data || [];
		data.push({
			notif: null,
			time: time,
			name: from,
			me: (isMe ? 'me' : ''),
			body: $('<pre>').text(body).html()//body.replace(/\n/g, "<br />")
		});
		
		this.update(data);
		this.scrollToBottom();
	},
	
	scrollToBottom: function() {
		if (this.body) {
			var dom = this.body.dom;
			dom.scrollTop = dom.scrollHeight - dom.offsetHeight;	
		}
	}
})
