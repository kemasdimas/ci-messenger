Ext.define('WG.view.chat.ConferenceChat', {
	extend: 'WG.view.chat.BasicChat',
	alias: 'widget.conferencechat',
	
	chatToolbar: [],
	
	initComponent: function() {
		this.items = [
			{
				region: 'east',
				minWidth: 200,
				maxWidth: 200,
				collapsible: true,
				
				preventHeader: false, 
				title: 'Participants',
				xtype: 'roomparticipantlist',
				
				listeners: {
					afterrender: {
						fn: function(comp) {
							WGMessenger_GLOBAL.messageBus.fireEvent(
								WGMessenger_GLOBAL.eventType.ui.queueTutorial,
								'conference-participant',
								
								'Conference Participants',
								
								'Menampilkan semua peserta dalam konferensi ini. Anda dapat: ' + 
								'<ul>' + 
									'<li>Menekan tombol "Invite Other" untuk mengundang peserta lain</li>' + 
									'<li>Klik ganda pada salah satu peserta untuk memulai percakapan tersendiri</li>' +
								'</ul>',
								
								function() {
									comp.expand(false);
								},
								
								comp
							);
						}
					}
				}
			}
		];
		
		this.callParent(arguments);
	}
});