Ext.define('WG.view.chat.BroadcastWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.broadcastwindow',
	
	title: 'Broadcast Message',
	id: 'broadcast-window',
	
	width: 500,
	height: 400,
	autoShow: 'true',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	
	defaults: {
		layout: 'fit'
	},
	
	initComponent: function() {
		
		this.items = [
			{
				xtype: 'form',
				flex: 1,
				bodyPadding: 5,
				
				defaults: {
					anchor: '100%'
				},
				
				items: [
					{
						xtype: 'textareafield',
						emptyText: 'Enter message here',
						allowBlank: false,
						name: 'body'
					}
				]
			},
			{
				xtype: 'friendlist',
				width: 200,
				autoScroll: true,
				title: 'Friend List',
				hideHeaders: false,
				selModel: Ext.create('Ext.selection.CheckboxModel')
			}
		];
		
		this.buttons = [
			{
				text: 'Send',
				action: 'broadcastnow'
			},
			{
				text: 'Cancel',
				handler: this.close,
				scope: this
			}
		];
		
		this.callParent(arguments);
	}
});