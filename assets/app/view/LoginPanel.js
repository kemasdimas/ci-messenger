Ext.define('WG.view.LoginPanel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.loginpanel',
	
	id: 'login-panel',
	layout: {
		type: 'hbox',
		align: 'middle',
		pack: 'center'
	},
	border: 0,
	
	defaults: {
		margin: 5
	},
	
	baseCls: 'x-plain',
	style: {
		// backgroundImage: 'url(http://www.sencha.com/img/20110215-feat-html5.png)'
	},
	
	initComponent: function() {
		
		var username	= $.cookie('wgchat-username');
		var wgname		= $.cookie('wgchat-wgname');
		var password	= $.cookie('wgchat-passwd');
		try {
			username	= demo_GLOBAL.username_demo;
			wgname		= demo_GLOBAL.workgroup_demo;
			password	= username + '-' + wgname;	
		} catch (e) {}
		
		this.items = [
			// {
				// xtype: 'panel',
				// width: 184,
				// height: 90,
				// border: 0,
// 				
				// items: [
					// {
						// xtype: 'image',
						// src: 'http://www.sencha.com/img/20110215-feat-html5.png',
					// }
				// ]
			// },
			{
				layout: 'anchor',
				xtype: 'form',
				iconCls: 'wgchat-favicon',
				title: 'User Login',
				bodyPadding: 5,
				width: 250,
				
				defaults: {
					enableKeyEvents: true,
					xtype: 'textfield',
				},
				
				fieldDefaults: {
					labelAlign: 'top',
					layout: 'anchor',
					anchor: '100%',
					allowBlank: false
				},
				
				items: [
					{	
						fieldLabel: 'Workgroup Name',
						name: 'workgroupname',
						value: wgname,
						allowBlank: true
					},
					{
						fieldLabel: 'Username',
						name: 'username',
						value: username
					},
					{
						fieldLabel: 'Password',
						inputType: 'password',
						name: 'password',
						value: password
					},
					{
						xtype: 'checkbox',
						labelAlign: 'left',
						fieldLabel: 'Save Password',
						name: 'savepassword',
						checked: $.cookie('wgchat-savepass') ? $.cookie('wgchat-savepass') === 'true' : true
					},
					{
						xtype: 'checkbox',
						labelAlign: 'left',
						fieldLabel: 'Show Tutorial',
						name: 'showtutorial',
						checked: $.cookie('wgchat-showtutorial') ? $.cookie('wgchat-showtutorial') === 'true' : false,
						
						// source: http://h4kr.com/extjs/tooltip-for-textfield
						listeners: {
						    render: function(c) {
						        Ext.QuickTips.register({
						            target: c.getEl(),
						            text: 'Show in application tutorial to help you use WG Messenger Web'
						        });
						    }
						}
					},
					{
						xtype: 'panel',
						border: 0,
						margin: '10 0 0',
						html: '<center>Default password is user-wgname, e.g. andrew-demo.</center>'
					}
				],
				
				buttons: [
					{
						text: 'Login',
						action: 'login',
						iconCls: 'wgchat-icon-login'
					}
				]
			}
		];
		
		this.callParent(arguments);
		
		this._checkSavedAccounts();
	},
	
	_checkSavedAccounts: function() {
		var formPanel = this.down('form');
		var store = Ext.getStore('Accounts');
		
		// Reading data from cookie
		var savedCredentials = Ext.JSON.decode($.cookie('wgchat-savedcreds')) || { };
		store.loadData(Ext.Object.getValues(savedCredentials));
		
		var me = this;
		if (store && store.count() > 0) {
			formPanel.insert(0, {	
				fieldLabel: 'Saved Accounts',
				name: 'savedaccounts',
				xtype: 'combobox',
				store: 'Accounts',
				
				emptyText: 'Choose your login account',
				forceSelection: true,
				
				queryMode: 'local',
				displayField: 'id',
				valueField: 'id',
				allowBlank: true,
				
				listeners: {
					select: {
						fn: function(comp, records) {
							var record = records[0];
							
							me._changeLoginFieldValues.call(this,
								record.get('workgroup'),
								record.get('username'),
								record.get('password')
							);
						},
						scope: formPanel
					}
				}
			});	
		}
	},
	
	_changeLoginFieldValues: function(workgroup, username, password) {
		var form = this.getForm();
		
		form.setValues({
			workgroupname: workgroup,
			username: username,
			password: password
		});
	}
});