Ext.define('WG.view.workgroup.ChatCategoryList', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.chatcategorylist',
	
	layout: 'fit',
	autoScroll: true,
	isFirstExpand: true,
	
	tbar: [
		{
			text: 'Save',
			iconCls: 'wgchat-icon-save',
			action: 'categorysave'
		}
	],
	
	columns: [
		{
			header: 'Name',
			dataIndex: 'title',
			flex: 1
		}
	],
	
	initComponent: function() {
		
		this.store = Ext.create('Ext.data.Store', {
			model: 'WG.model.ChatCategory'
		})
		this.selModel = Ext.create('Ext.selection.CheckboxModel');
		
		this.callParent(arguments);
	}
});