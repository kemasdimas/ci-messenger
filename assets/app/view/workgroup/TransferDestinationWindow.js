Ext.define('WG.view.workgroup.TransferDestinationWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.transferdestinationwindow',
	
	title: 'Transfer Conversation',
	width: 250,
	height: 400,
	autoShow: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	roomJid: '',
	
	initComponent: function() {
		
		var me = this;
		var store = Ext.create('Ext.data.Store', {
			model: 'WG.model.Roster',
			autoLoad: true,
			groupField: 'group',
			data: [],
			sorters: [
				{
					property: 'name',
					direction: 'ASC'
				}
			],
			
			proxy: {
				type: 'memory'
			}
		});
		
		this.items = [
			{
				xtype: 'grid',
				flex: 1,
				store: store,
				columns: [
					{ header: 'Name', dataIndex: 'name', flex: 1 }
				],
				features: [
					Ext.create('Ext.grid.feature.Grouping', {
						border: 0,
						groupHeaderTpl: '{name}'
					})
				]
			},
			{
				xtype: 'textareafield',
				fieldLabel: 'Reason',
				labelAlign: 'top',
				height: 70,
				value: 'I\'m transferring this conversation to you',
				padding: '5 0 0'
			}
		];
		
		this.buttons = [
			{
				text: 'Transfer',
				action: 'transfernow'
			},
			{
				text: 'Cancel',
				action: 'transfercancel',
				scope: this,
				handler: this.close
			}
		];
		
		this.callParent(arguments);
	}
});