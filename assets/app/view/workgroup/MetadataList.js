Ext.define('WG.view.workgroup.MetadataList', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.metadatalist',
	
	columns: [
        { header: 'Name',  dataIndex: 'name', flex: 1, renderer: function(value, opts, record) {
        	return '<div data-qtip="' + record.data.name + ': ' + record.data.value + '">' + value + '</div>';
        }},
        { header: 'Value', dataIndex: 'value', flex: 2, renderer: function(value, opts, record) {
        	return '<div data-qtip="' + record.data.name + ': ' + record.data.value + '">' + value + '</div>';
        }}
    ],
    
	initComponent: function() {
		this.callParent(arguments);
	}
});
