Ext.define('WG.view.workgroup.PersonalMacroForm', {
    extend: 'Ext.window.Window',
    alias : 'widget.personalmacroform',

    title : 'Personal Macro Form',
    layout: 'fit',
    autoShow: true,
	width: 350,
	
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                bodyPadding: 5,
                layout: 'anchor',
                
			    fieldDefaults: {
					anchor: '100%',
					allowBlank: false
				},
			    
                items: [
                    {
                        xtype: 'textfield',
                        name : 'title',
                        fieldLabel: 'Title'
                    },
                    {
                        xtype: 'textareafield',
                        name : 'response',
                        fieldLabel: 'Message Text'
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Save',
                action: 'save'
            },
            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

		this.listeners = {
			show: {
				fn: this.handleFormActive,
				scope: this
			}
		};

        this.callParent(arguments);
    },
    
    handleFormActive: function(comp) {
    	comp.down('textfield').focus(true, true);
    }
});