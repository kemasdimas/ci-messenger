Ext.define('WG.view.workgroup.TransferOfferProgressWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.transferofferprogresswindow',
	
	closable: false,
	width: 300,
	bodyPadding: 5,
	layout: 'fit',
	
	timeout: 90,
	interval: null,
	sessionId: '',
	metadatas: [],
	reason: 'Transfer from bla bla bla',
	
	REQUEST_SOUND_ID: 'chatrequest',
	
	buttons: [
		{
			text: 'Accept',
			action: 'offeraccept',
			iconCls: 'wgchat-icon-accept'
		},
		{
			text: 'Reject',
			action: 'offerreject',
			iconCls: 'wgchat-icon-delete'
		}
	],
	
	initComponent: function() {
		
		var progressBar = Ext.create('Ext.ProgressBar');
		var counter = 1;
		var me = this;
		
		progressBar.updateProgress(0, this.timeout + 's');	
		this.interval = setInterval(function() {
			var remaining = me.timeout - counter;
			
			progressBar.updateProgress(counter / me.timeout, remaining + 's');
			if (remaining === 0) {
				clearInterval(me.interval);
				me.close();
			}
				
			counter++;
		}, 1000);
		
		var me = this;
		this.items =  [
			progressBar,
			{
				xtype: 'panel',
				html: me.reason,
				padding: '5 0 0',
				bodyPadding: 5
			},
			{
				xtype: 'metadatalist',
				padding: '5 0 0',
				store: Ext.create('Ext.data.Store', {
					model: 'WG.model.Metadata',
					data: this.metadatas
				}),
				columns: [
					{ header: 'Name', dataIndex: 'label', flex: 1 },
					{ header: 'Value', dataIndex: 'value', flex: 1 }
				]
			}
		];
		
		this.listeners = {
			show: function() {
				soundManager.onready(function() {
					soundManager.play(me.REQUEST_SOUND_ID);
				});
				
				notif = WGMessenger_GLOBAL.showDesktopNotification(me.title, me.reason);
			},
			
			destroy: function() {
				if (notif) {
					notif.cancel();
				}
			}
		};
		
		this.callParent(arguments);
	}
});
