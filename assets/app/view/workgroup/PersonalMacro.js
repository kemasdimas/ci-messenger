Ext.define('WG.view.workgroup.PersonalMacro', {
	extend: 'Ext.window.Window',
	alias: 'widget.personalmacro',
	
	title: 'Personal Macro',
	width: 300,
	height: 350,
	layout: 'fit',
	
	initComponent: function() {
		var macroGrid = Ext.create('Ext.grid.Panel', {
			store: 'PersonalMacros',
			columns: [
		        { header: 'Title',  dataIndex: 'title', flex: 2 },
		        { header: 'Response', dataIndex: 'response', flex: 5 }
		    ],
		    tbar: [
		    	{
		    		text: 'Add',
		    		action: 'macroadd',
		    		iconCls: 'wgchat-icon-accept'
		    	},
		    	{
		    		text: 'Edit',
		    		action: 'macroedit',
		    		iconCls: 'wgchat-icon-edit'
		    	},
		    	{
		    		text: 'Delete',
		    		action: 'macrodelete',
		    		iconCls: 'wgchat-icon-delete'
		    	}
		    ]
		});
		
		this.items = [macroGrid];
		this.buttons = [
			{
				text: 'Save',
				action: 'personalmacrosave'
			},
			{
				text: 'Cancel',
				scope: this,
				handler: this.close
			}
		];
		
		this.listeners = {
			afterrender: {
				fn: function(comp) {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.ui.queueTutorial,
						'wgchat-personal-macro',
						
						'Personal Macro Setting',
						
						'Menambah / Mengubah / Menghapus template pesan pribadi Anda, Anda dapat: ' + 
						'<ul>' + 
							'<li>Menggunakan template pesan ini untuk mempercepat pelayanan Anda pada pelanggan</li>' +
							'<li>Mengubah template "Auto Greeting" yang akan secara otomatis dikirimkan ke pelanggan setiap kali melayani pelanggan</li>' +
						'</ul>',
						
						function() {
							comp.focus(false, true);
						},
						
						comp
					);
				}
			}
		};
		
		this.callParent(arguments);
	}
});
