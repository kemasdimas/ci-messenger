Ext.define('WG.view.workgroup.HistoryDetail', {
	extend: 'Ext.window.Window',
	alias: 'widget.historydetail',
	
	// TODO: Multiple history can be opened in one time
	id: 'single-historydetail',
	title: 'Chat History Detail',
	width: 400,
	height: 350,
	layout: 'fit',
	
	initComponent: function() {
		this.items = [
			{
				xtype: 'tabpanel',
				defaults: {
					autoScroll: true,
					bodyPadding: 5
				},
				
				items: [
					{
						xtype: 'chatmessagepanel',
						title: 'Transcript',
						itemId: 'history-transcript'
					},
					{
						xtype: 'metadatalist',
						bodyPadding: 0,
						title: 'User Data',
						itemId: 'history-metadata'
					},
					{
						xtype: 'panel',
						title: 'Note',
						itemId: 'history-note',
						html: 'No note for this chat.'
					}
				]
			}
		];
		
		this.tbar = [
			{
				text: 'Send IM to Customer',
				action: 'sendimmessage',
				iconCls: 'wgchat-icon-sendim'
			}
		];
		
		this.listeners = {
			afterrender: {
				fn: function(comp) {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.ui.queueTutorial,
						'wgchat-history-detail',
						
						'Chat History Detail',
						
						'Menampilkan detail riwayat percakapan dengan pelanggan. Anda dapat: ' + 
						'<ul>' + 
							'<li>Mengirimkan pesan 1 arah ke pelanggan menggunakan tombol "Send IM to Customer"</li>' +
						'</ul>',
						
						function() {
							comp.focus(false, true);
						},
						
						comp
					);
				}
			}
		};
		
		this.callParent(arguments);
	},
	
	_checkDelay: function(packet) {
		var $packet = $(packet);
		var $delay = $packet.find('x[xmlns$="delay"]');
		var date = new Date();
		var time = '';
		if ($delay.size() > 0) {
			var stamp = $delay.attr('stamp');
			date = Ext.Date.parse(stamp, "YmdTH:i:s");
			
			var offset = date.getTimezoneOffset() * -60000;
			date = new Date(date.getTime() + offset);
		}
		
		return date;
	},
	
	_clear: function() {
		var chatMessagePanel = this.down('chatmessagepanel');
		
		if (chatMessagePanel) {
			chatMessagePanel.clearData();
		}
	},
	
	_printPresence: function(packet) {
		var chatMessagePanel = this.down('chatmessagepanel');
		
		var $packet = $(packet);
		var fullFrom = $packet.attr('from');
    	var isOnline = $packet.attr('type') !== 'unavailable';
    	
		if (chatMessagePanel) {
			var name = Strophe.getResourceFromJid(fullFrom);
			
			var time = Ext.Date.format(this._checkDelay(packet), 'H:i');
			var notif = name + " left conversation at " + time;
			
			if (isOnline) {
				var notif = name + " joined conversation at " + time;
			}
			
			chatMessagePanel.notificationReceived(notif);
		}
	},
	
	_printMessage: function(packet) {
		var chatMessagePanel = this.down('chatmessagepanel');
		
		var $packet = $(packet);
		var fullFrom = $packet.attr('from');
		var bareFrom = Strophe.getBareJidFromJid(fullFrom);
		var senderName = Strophe.getResourceFromJid(fullFrom);
		var myName = Strophe.getNodeFromJid(StropheBackend.connection.jid);
		
		if ($packet.find('body').size() > 0) {
			body = $packet.find('body').text();
			
			time = Ext.Date.format(this._checkDelay(packet), 'H:i');
			chatMessagePanel.messageReceived(senderName, time, body, myName === senderName);
		}
	},
	
	loadTranscriptPacket: function(packets) {
		var me = this;
		
		Ext.defer(function() {
			me._clear();
			
			packets.each(function(index, packet) {
				var $packet = $(packet);
				var tagName = $packet.prop('tagName');
				
				if (tagName.toLowerCase() === 'presence') {
					me._printPresence(packet);
				} else if (tagName.toLowerCase() === 'message') {
					me._printMessage(packet);
				}
			});
		}, 50);
	},
	
	loadMetadataData: function(data) {
		var metadataGrid = this.down('metadatalist');
		var store = metadataGrid.getStore();
		store.loadData(data);
	},
	
	loadNote: function(note) {
		var nodePanel = this.down('tabpanel').getComponent('history-note');
		nodePanel.html = note;
	}
});
