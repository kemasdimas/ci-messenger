Ext.define('WG.view.workgroup.HistoryList', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.chathistorylist',
	layout: 'fit',
	border: 0,
	isFirstHistoryQueue: true,
	
	columns: [
        { header: 'Name',  dataIndex: 'visitorsName', flex: 1 },
        { header: 'Time', dataIndex: 'date', flex: 1, renderer: function(value) {
        	if (value) {
        		return Ext.Date.format(new Date(value), 'Y-m-d H:i');	
        	} else {
        		return '';
        	}
        } }
    ],
    
    tbar: [
		'->',
		{
			text: 'Refresh',
			iconCls: 'wgchat-icon-refresh',
			action: 'historyrefreh'
		}
	],
    
    store: 'GlobalChatHistories',
    
	initComponent: function() {
		this.callParent(arguments);
	}
});
