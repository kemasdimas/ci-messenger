Ext.define('WG.view.workgroup.OnlineAgentList', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.onlineagentlist',
	
	store: 'WorkgroupAgents',
	border: 0,
    
	initComponent: function() {
		
		this.columns = [
			{
				header: 'Online Agents', 
				dataIndex: 'name', 
				flex: 1,
				renderer: function(value, opts, record) {
					return Ext.String.format(
						'<div class="wgchat-roster-item" data-jid="{0}">' +
							'<span class="status status-{1}">&nbsp;</span>' +
							'<span class="name">{2}</span>' +
						'</div>'
					, record.data.jid, record.data.status, value);
				}
			}
		];
		
		this.callParent(arguments);
	}
});