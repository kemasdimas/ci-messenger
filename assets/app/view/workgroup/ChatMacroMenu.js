Ext.define('WG.view.workgroup.ChatMacroMenu', {
	extend: 'Ext.menu.Menu',
	alias: 'widget.chatmacromenu',
	
	personalMacroStore: null,
	globalMacroStore: null,
	
	items: [
		{
			text: 'Global Template',
			itemId: 'global-template'
		},
		{
			text: 'Personal Template',
			itemId: 'personal-template'
		}
	],
	
	initComponent: function() {
		this.callParent(arguments);
		
		this.personalMacroStore = Ext.getStore('PersonalMacros');
		
		this.globalMacroStore = Ext.getStore('GlobalMacros');
		
		this.handlePersonalStoreDataChange(Ext.getStore('PersonalMacros'));
		this.handleGlobalStoreDataChange(Ext.getStore('GlobalMacros'));
	},
	
	destroy: function() {
		this.callParent(arguments);
	},
	
	handlePersonalStoreDataChange: function(store) {
		var root = this.getComponent('personal-template');
		var innerMenu = Ext.widget('menu');
		
		store.each(function(record) {
			this.add({
				text: record.get('title'),
				body: record.get('response')
			});
		}, innerMenu);
		
		if (root && root.menu) {
			root.menu.destroy();
		}
		root.menu = innerMenu;
		
		this.doLayout();
	},
	
	handleGlobalStoreDataChange: function(store) {
		var root = this.getComponent('global-template');
		var prevGroupName = null;
		var groupHandler = {};
		var menuItems = [];
		
		// TODO: menu tree masih belum beres
		store.each(function(record) {
			var groupName = record.get('group');
			
			if (prevGroupName !== groupName) {
				prevGroupName = groupName;
				groupHandler = {
					text: groupName,
					menu: []
				};
				
				menuItems.push(groupHandler);
			}
			
			groupHandler.menu.push({
				text: record.get('title'),
				body: record.get('response')
			});
		}, this);
		
		if (root.menu) {
			root.menu.destroy();
		}
		
		var innerMenu = Ext.widget('menu', {
			items: menuItems
		}); 
		
		root.menu = innerMenu;
		
		this.doLayout();
	}
})
