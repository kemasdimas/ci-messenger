Ext.define('WG.view.workgroup.HistoryContextMenu', {
	extend: 'Ext.menu.Menu',
	alias: 'widget.historycontextmenu',
	record: {},
	
	items: [
		{
			text: 'View Detail',
			action: 'historyviewdetail'
		},
		{
			text: 'Send IM to Customer',
			action: 'historysendmessage'
		}
	]
});
