Ext.define('WG.view.workgroup.InitiateMessageWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.initiatemessagewindow',
	
	title: 'Send IM Message',
	id: 'initiatemessagewindow',
	autoShow: true,
	width: 300,
	height: 250,
	imStore: [],
	
	layout: {
		type: 'fit'
	},
	
	buttons: [
		{
			text: 'Send Message',
			action: 'sendimmessage',
			iconCls: 'wgchat-icon-sendim'
		}
	],
	
	initComponent: function() {
		this.items = [
			{
				xtype: 'form',
				plain: true,
				bodyPadding: 5,
				
				fieldDefaults: {
					labelWidth: 55,
					anchor: '100%',
					allowBlank: false
				},
				
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				
				items: [
					{
						xtype: 'combobox',
						name: 'sendvia',
						store: Ext.getStore('OnlineIMs'),
						displayField: 'username',
						valueField: 'handler',
						queryMode: 'local',
						typeAhead: true,
						emptyText: 'Select sender account',
						forceSelection: true,
						valueNotFoundText: 'Select sender account',
						listConfig: {
							getInnerTpl: function() {
								return '<div data-qtip="Send via {username}"><span class="wgchat-icon wgchat-icon-{type}">&nbsp;</span>&nbsp;{username}</div>';
							}
						}
					},
					{
						xtype: 'textfield',
						name: 'sendto',
						emptyText: 'Destinated IM username (e.g. my.customer)'
					},
					{
						xtype: 'textarea',
						emptyText: 'Enter message here',
						fieldLabel: 'Message Text',
						hideLabel: true,
						name: 'messagecontent',
						maxLength: 500,
						margin: 0,
						flex: 1
					}
				]
			}
		];
		
		this.callParent(arguments);
	}
});
