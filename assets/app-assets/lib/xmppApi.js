var xmppApi = {
	handler: {
		connectionDetachedHandler: function() {
			setTimeout(function() {
				StropheBackend.attachConnection(xmppApi.handler.mainConnectionHandler);
			}, 250);
		},
		
		mainConnectionHandler: function(status, msg) {
			if (status === Strophe.Status.ATTACHED) {
				WGMessenger_GLOBAL.messageBus.fireEvent(
					WGMessenger_GLOBAL.eventType.ui.reattachSuccess
				);
				
				xmppApi.addRosterHandler();
				xmppApi.addAfterReadyHandlers();
			} else if (status === Strophe.Status.CONNECTED) {
				WGMessenger_GLOBAL.isLoggingOut = false;
				WGMessenger_GLOBAL.messageBus.fireEvent(
					WGMessenger_GLOBAL.eventType.ui.loginSuccess
				);
				
	            WGMessenger_GLOBAL.setStatusBarValid('Logged in');
	            
	            xmppApi.prepareUser();
	        } else if (status === Strophe.Status.CONNECTING) {
	        	WGMessenger_GLOBAL.setStatusBarLoading('Connecting...');
	        } else if (status === Strophe.Status.CONNFAIL) {
	        	
	        	if (WGMessenger_GLOBAL.isRelogin) {
	        		// Retry to login again, since the connection has been failed
	        		WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.ui.autoLogin,
						WGMessenger_GLOBAL.autoLoginInterval
					);
	        	} else {
	        		var demoText = '';
		        	if (WGMessenger_GLOBAL.isDemoAccount()) {
		        		var userNode = 'csX-demo';
		        		try {
		        			userNode = Strophe.getNodeFromJid(StropheBackend.connection.jid);	
		        		} catch (err) {
		        			
		        		}
		        		
		        		demoText = '<br /><br />* Note: Anda menggunakan akun demo <b>' + userNode + '</b>, kemungkinan ada orang lain yang menggunakan akun demo yang sama dengan Anda, ' + 
		        			'silakan mencoba menggunakan username demo lain (cs1 - cs5).';
		        	}
		        	
		        	WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.ui.loginFailed,
						{
							title: 'Connection Failed',
							text: 'Please wait for automatic login or refresh this page.<br /><br /> ' + 
								'If this problem still exist, please report this issue using "Report Issue" button at the bottom right corner' + demoText,
							delay: -1
						}
					);
	        	}
	        	
	        	WGMessenger_GLOBAL.setStatusBarError('Connection failed');
	        } else if (status === Strophe.Status.AUTHENTICATING) {
	        	WGMessenger_GLOBAL.setStatusBarLoading('Logging in...');
	        } else if (status === Strophe.Status.AUTHFAIL) {
	        	WGMessenger_GLOBAL.messageBus.fireEvent(
					WGMessenger_GLOBAL.eventType.ui.loginFailed,
					{
						title: 'Invalid Login',
						text: 'Invalid login, check username & password'
					}
				);
	        	
	        	WGMessenger_GLOBAL.setStatusBarError('Invalid login, check username & password');
	        } else if (status === Strophe.Status.DISCONNECTING) {
	            WGMessenger_GLOBAL.setStatusBarLoading('Disconnecting...');
	        } else if (status === Strophe.Status.DISCONNECTED) {
	            WGMessenger_GLOBAL.setStatusBarValid('Disconnected');
	            
	            if (!WGMessenger_GLOBAL.isLoggingOut) {
	            	xmppApi.retryAttaching();
	            }
	        } else if (status === 99) {
	        	WGMessenger_GLOBAL.messageBus.fireEvent(
					WGMessenger_GLOBAL.eventType.ui.loginFailed,
					{
						title: 'Connection Disconnected',
						text: 'Your session has been disconnected due to connection problem, please wait for automatic login or refresh this page',
						delay: -1
					}
				);	
		    	
		    	// If connection failed and not a demo account, demo account could possibly conflicted
				if (!WGMessenger_GLOBAL.isDemoAccount()) {
					WGMessenger_GLOBAL.isLoggingOut = true;
					// Fire autologin request 
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.ui.autoLogin,
						WGMessenger_GLOBAL.autoLoginInterval
					);
				}
	        }
		},
		
		rosterResultHandler: function(packet) {
			try {
				WGMessenger_GLOBAL.messageBus.fireEvent(
					WGMessenger_GLOBAL.eventType.xmpp.rosterResultReceived,
					packet
				);	
			} catch (err) {
				console.error(err);
			}	
		},
		
		rosterPresenceHandler: function(packet) {
			try {
				var $packet = $(packet);
				var isMuc = $packet.find('x[xmlns$="muc#user"]').size() > 0;
				var isWorkgroup = $packet.attr('from').indexOf('@workgroup.') != -1;
				var isWorkgroupRoster = !isWorkgroup && $packet.find('agent-status').size() > 0;
				
				if (isMuc) {
					if (xmppApi.isActiveUserRequest(packet)) {
						WGMessenger_GLOBAL.messageBus.fireEvent(
							WGMessenger_GLOBAL.eventType.xmpp.workgroupUserChatPresence,
							packet
						);
					} else {
						WGMessenger_GLOBAL.messageBus.fireEvent(
							WGMessenger_GLOBAL.eventType.xmpp.conferencePresenceChange,
							packet
						);	
					}
				} else if (isWorkgroup) {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.workgroupPresenceChange,
						packet
					);
				} else if (isWorkgroupRoster) {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.workgroupRosterPresenceChange,
						packet
					);
				} else {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.presenceChange,
						packet
					);	
				}
			} catch (err) {
				if (console) {
					console.error(err);
				}
			}
			
			return true;
		},
		
		userReadyHandler: function() {
			WGMessenger_GLOBAL.messageBus.removeListener(
				WGMessenger_GLOBAL.eventType.ui.userReady,
				xmppApi.handler.userReadyHandler
			);
			
			xmppApi.addAfterReadyHandlers();
			
			var pres = $pres({
				type: 'available'
			}).c('status', {}, 'Online')
			.c('priority', {}, '1');
			
			StropheBackend.send(pres);
		},
		
		globalIQHandler: function(packet) {
			try {
				var $packet = $(packet);
				var id = $packet.attr('id');
				
				WGMessenger_GLOBAL.messageBus.fireEvent(
					id,
					packet
				);
			} catch (err) {
				console.error(err);
			}

			return true;
		},
		
		workgroupServiceConfirmedHandler: function() {
			var workgroup = WGMessenger_GLOBAL.features.workgroup;
			var wgServiceJid = workgroup.jid;
			var workgroupNode = StropheBackend.workgroup;
			var workgroupFullJid = workgroupNode + '@' + wgServiceJid;
			
			var agentQuery = $iq({
				id: StropheBackend.getUniqueId(),
				to: workgroupFullJid,
				type: 'get'
			}).c('agent-status-request', { xmlns: 'http://jabber.org/protocol/workgroup' });
			
			StropheBackend.send(agentQuery);
			
			// Query workgroup form initially
			var formQuery = $iq({
				id: StropheBackend.getUniqueId(),
				to: workgroupFullJid,
				type: 'get'
			}).c('workgroup-form', { xmlns: 'http://jivesoftware.com/protocol/workgroup' });
			
			StropheBackend.send(formQuery);
			
			// Query personal macro
			var macroQuery = $iq({
	    		id: StropheBackend.getUniqueId(),
	    		to: workgroupFullJid,
	    		type: 'get'
	    	}).c('macros', { xmlns: 'http://jivesoftware.com/protocol/workgroup' }).
	    	c('personal', {}, 'true');
			
			StropheBackend.send(macroQuery);
			
			// Query global macro
			var globalMacroQuery = $iq({
	    		id: StropheBackend.getUniqueId(),
	    		to: workgroupFullJid,
	    		type: 'get'
	    	}).c('macros', { xmlns: 'http://jivesoftware.com/protocol/workgroup' });
			
			StropheBackend.send(globalMacroQuery);
			
			StropheBackend.workgroupFullJid = workgroupFullJid;
		},
		
		wgchatProviderResultHandler: function(packet) {
			try {
				var $packet = $(packet);
				
				if ($packet.find('online-im-list').size() > 0) {
					// Packet contains Online IM List
					
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.wgchatOnlineIMResultReceived,
						packet
					);
				} else if ($packet.find('slot-notification').size() > 0) {
					// Packet contains slot notification
					
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.wgchatSlotNotificationReceived,
						packet
					);
				}
			} catch (err) {
				console.error(err);
			}
			
			return true;
		},
		
		workgroupQueuePresenceHandler: function(packet) {
			try {
				var $packet = $(packet);
				
				if ($packet.find('notify-queue-details').size() > 0) {
					
				} else if ($packet.find('notify-queue').size() > 0) {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.workgroupQueuePresence,
						packet
					);
				}				
			} catch (err) {
				console.error(err);
			}

			return true;
		},
		
		_workgroupIQHandler: function(packet) {
			try {
				var $packet = $(packet);
				var type = $packet.attr('type');
				
				if ($packet.find('offer-revoke').size() > 0) { 
				
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.workgroupOfferRevokeReceived,
						packet
					);
				} else if ($packet.find('offer').size() > 0) {
					var id = $packet.attr('id');
					var from = $packet.attr('from');
					var transfer = $packet.find('transfer');
					
					var offerAck = $iq({
						id: id,
						type: 'result',
						to: from
					});
	
					StropheBackend.send(offerAck);
					
					// If offer received, must automatically query workgroup form
					var formQuery = $iq({
						id: StropheBackend.getUniqueId(),
						to: StropheBackend.workgroupFullJid,
						type: 'get'
					}).c('workgroup-form', { xmlns: 'http://jivesoftware.com/protocol/workgroup' });
					
					StropheBackend.send(formQuery);
					
					// workgroupChatOfferReceived and workgroupTransferOfferReceived
					// will be handled by WG.store.UserRequests.js
					if (transfer.size() > 0) {
						WGMessenger_GLOBAL.messageBus.fireEvent(
							WGMessenger_GLOBAL.eventType.xmpp.workgroupTransferOfferReceived,
							packet
						);	
					} else {
						WGMessenger_GLOBAL.messageBus.fireEvent(
							WGMessenger_GLOBAL.eventType.xmpp.workgroupChatOfferReceived,
							packet
						);	
					}
				} else if ($packet.find('macros').size() > 0) {
					// Packet contains Macro
					var rawModel = $packet.find('model').text();
					var $model = $(rawModel);
					var titles = $model.find('title');
					var title = '';
					
					if (titles.size() > 0) {
						title = $(titles[titles.size() - 1]).text();
					}
					
					if (title === 'Personal') {
						WGMessenger_GLOBAL.messageBus.fireEvent(
							WGMessenger_GLOBAL.eventType.xmpp.personalMacroResultReceived,
							packet
						);	
					} else if (title === 'Parent Category') {
						WGMessenger_GLOBAL.messageBus.fireEvent(
							WGMessenger_GLOBAL.eventType.xmpp.globalMacroResultReceived,
							packet
						);
					}
				} else if ($packet.find('chat-sessions').size() > 0) {
					// Packet contains chat session
					
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.chatHistoryResultReceived,
						packet
					);
				} else if ($packet.find('transcripts').size() > 0) {
					// Packet contains user chat histories
					
					// WGMessenger_GLOBAL.messageBus.fireEvent(
						// WGMessenger_GLOBAL.eventType.xmpp.workgroupUserHistoriesReceived,
						// packet
					// );
				} else if ($packet.find('transcript').size() > 0) {
					// Packet contains chat transcript
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.historyTranscriptReceived,
						packet
					);
				} else if ($packet.find('chat-metadata').size() > 0) {
					// Packet contains chat history metadata
					
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.historyMetadataReceived,
						packet
					);
				} else if ($packet.find('chat-notes').size() > 0 && type === 'result') {
					// Packet contains chat history note
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.historyNoteReceived,
						packet
					);
				} else if ($packet.find('workgroup-form').size() > 0) {
					
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.workgroupFormReceived,
						packet
					);
				}				
			} catch (err) {
				console.error(err);
			}

			return true;
		},
		
		workgroupIQResultHandler: function(packet) {
			
			return xmppApi.handler._workgroupIQHandler(packet);
		},
		
		broadcastMessageHandler: function(packet) {
			try {
				var $packet = $(packet);
				var type = $packet.attr('type');
				var body = $packet.find('body');
				
				if ((!type || type === 'headline') && body.size() > 0) {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.chatMessageReceived,
						packet
					);	
				}
			} catch (err) {
				console.error(err);
			}
			
			return true;
		},
		
		chatMessageHandler: function(packet) {
			try {
				WGMessenger_GLOBAL.messageBus.fireEvent(
					WGMessenger_GLOBAL.eventType.xmpp.chatMessageReceived,
					packet
				);
			} catch (err) {
				console.error(err);
			}

			return true;
		},
		
		groupMessageHandler: function(packet) {
			try {
				var $packet = $(packet);
				
				if (xmppApi.isActiveUserRequest(packet)) {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.workgroupUserChatMessage,
						packet
					);	
				} else if ($packet.find('x[xmlns="jabber:x:event"]').size() > 0 || $packet.find('repeated[xmlns$="/repeated"]')) {
					
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.groupMessageReceived,
						packet
					);	
				} else {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.groupNotificationReceived,
						packet
					);
				}
			} catch (err) {
				console.error(err);
			}
			
			return true;
		},
		
		groupNotificationMessageHandler: function(packet) {
			try {
				var $packet = $(packet);
				var type = $packet.attr('type');
				
				if (type !== 'groupchat') {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.groupNotificationReceived,
						packet
					);
				}
			} catch (err) {
				console.error(err);
			}
			
			return true;
		},
		
		buzzHandler: function(packet) {
			try {
				WGMessenger_GLOBAL.messageBus.fireEvent(
					WGMessenger_GLOBAL.eventType.xmpp.buzzReceived,
					packet
				);
			} catch (err) {
				console.error(err);
			}
			
			return true;
		},
		
		conferenceInvitationHandler: function(packet) {
			try {
				var $packet = $(packet);
				var invite = $packet.find('invite');
				
				if ($packet.find('offer[xmlns$="/protocol/workgroup"]').size() > 0) {
					// User request inviitation
					
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.workgroupUserRoomInvitation,
						packet
					);
					
					// Automatically accept the workgroup chat request invitation
					/**
					 * <presence id="lBUfQ-68" to="f316h71485@conference.kemass-macbook-air.local/ciba-kemas">
					 * 	<x xmlns="http://jabber.org/protocol/muc">
					 * 	</x>
					 * </presence>
					 */
					var myNode = Strophe.getNodeFromJid(StropheBackend.connection.jid);
					var from = $packet.attr('from');
					var pres = $pres({
						id: StropheBackend.getUniqueId(),
						to: from + '/' + myNode
					}).c('x', { xmlns: 'http://jabber.org/protocol/muc' });
					
					StropheBackend.send(pres);
				} else if (invite.size() > 0) {
					WGMessenger_GLOBAL.messageBus.fireEvent(
						WGMessenger_GLOBAL.eventType.xmpp.conferenceInvitationReceived,
						packet
					);
				}	
			} catch (err) {
				console.error(err);
			}
			
			return true;
		}
	},
	
	retryAttaching: function() {
		WGMessenger_GLOBAL.messageBus.fireEvent(
			WGMessenger_GLOBAL.eventType.xmpp.xmppConnectionDetached
		);
	},
	
	/**
	 * Check whether a sessionID is currently inside UserRequest or TransferRequest store
	 * and is currently active
	 */
	isActiveUserRequest: function(packet) {
		try {
			var $packet = $(packet);
			var roomId = Strophe.getNodeFromJid($packet.attr('from'));
			var request = WGMessenger_GLOBAL.getRequestRecord(roomId);
			
			if (request && request.get('isActive') === true) {
				return true;
			}
		} catch (err) {
			console.error(err);
		}
		
		return false;
	},
	
	/**
	 * Log a user in
	 */
	login: function(username, workgroupname, password) {
		StropheBackend.openConnection(username + '-' + workgroupname, password, xmppApi.handler.mainConnectionHandler, xmppApi);
		StropheBackend.username = username;
		StropheBackend.workgroup = workgroupname;
		StropheBackend.isAttaching = false;
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.ui.userReady,
			xmppApi.handler.userReadyHandler,
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.ui.workgroupServiceConfirmed,
			xmppApi.handler.workgroupServiceConfirmedHandler,
			this
		);
		
		WGMessenger_GLOBAL.messageBus.addListener(
			WGMessenger_GLOBAL.eventType.xmpp.xmppConnectionDetached,
			xmppApi.handler.connectionDetachedHandler,
			this
		);
	},
	
	/**
	 * Log user session out
	 */
	logout: function() {
		WGMessenger_GLOBAL.messageBus.clearListeners();
   		
   		if (StropheBackend.connection) {
	   		StropheBackend.connection.disconnect();	
   		}
	},
	
	/**
	 * Queries available features from connected server
	 */
	featuresQuery: function() {
		var me = this;
		
		// Features query
        var discoId = StropheBackend.connection.getUniqueId();
        var discoQuery = $iq({
        	type: 'get',
        	to: StropheBackend.xmppDomain,
        	id: discoId
        }).c('query', {
        	xmlns: 'http://jabber.org/protocol/disco#items'
        });
        
        StropheBackend.connection.addHandler(function(packet) {
        	var $packet = $(packet);
        	var items = $packet.find('item');
        	
        	items.each(function(index, elem) {
        		var $elem = $(elem);
        		var feature = {};
        		feature.jid = $elem.attr('jid');
        		feature.name = $elem.attr('name');
        		
        		var featureId = StropheBackend.connection.getUniqueId();
        		var featureQuery = $iq({
		        	type: 'get',
		        	to: feature.jid,
		        	id: featureId
		        }).c('query', {
		        	xmlns: 'http://jabber.org/protocol/disco#items'
		        });
		        
		        // Add feature check handler
		        StropheBackend.connection.addHandler(function(aFeaturePacket) {
		        	var $aFeaturePacket = $(aFeaturePacket);
		        	var featureJid = $aFeaturePacket.attr('from');
		        	var featureShortname = featureJid.substring(0, featureJid.indexOf('.'));
		        	
		        	var availableFeature = {};
		        	availableFeature.jid = featureJid;
		        	availableFeature.name = feature.name;
		        	
		        	WGMessenger_GLOBAL.features[featureShortname] = availableFeature;
		        	
		        	// If feature = Fastpath, or the workgroup feature, proceed with special treatment
		        	if (feature.name === 'Fastpath') {
		        		var workgroup = WGMessenger_GLOBAL.features.workgroup;
						var wgServiceJid = workgroup.jid;
						var workgroupNode = StropheBackend.workgroup;
						
						// Send online presence to workgroup
						var wgPresInit = $pres({
							id: StropheBackend.getUniqueId(),
							to: workgroupNode + '@' + wgServiceJid
						}).c('agent-status', { xmlns: 'http://jabber.org/protocol/workgroup' });
						
						StropheBackend.send(wgPresInit);
						
						var wgPresOnline = $pres({
							id: StropheBackend.getUniqueId(),
							to: workgroupNode + '@' + wgServiceJid
						}).c('status', { }, 'Online')
						.c('priority', { }, '1');
						
						StropheBackend.send(wgPresOnline);
		        	}
		        }, null, 'iq', 'result', featureId);
		        
		        StropheBackend.send(featureQuery);
        	});
        	
        	// Make sure workgroup service is loaded, wait for 10s
        	Ext.defer(function() {
        		var wgFeature = WGMessenger_GLOBAL.features['workgroup'];
        		
        		if (!(wgFeature && wgFeature.jid)) {
        			me.featuresQuery();
        		}
        	}, 10000);
        }, null, 'iq', 'result', discoId);
        
        StropheBackend.send(discoQuery);
	},
	
	/**
	 * Query user's rosters and proceed to features query
	 */
	prepareUser: function(isReconnect) {
        var rosterQuery = $iq({
            type: 'get',
            id: StropheBackend.connection.getUniqueId()
        }).c('query', {
            xmlns: 'jabber:iq:roster'
        });
        
        xmppApi.addRosterHandler();
        StropheBackend.send(rosterQuery);
        
        if (isReconnect !== true) {
        	xmppApi.featuresQuery();	
        }
	},
	
	/**
	 * addHandler encapsulation functions
	 */
	addRosterHandler: function() {
		StropheBackend.connection.addHandler(xmppApi.handler.rosterResultHandler, 'jabber:iq:roster');
	},
	
	addAfterReadyHandlers: function() {
		// Add other user presence handler to update friends presence
		StropheBackend.connection.addHandler(xmppApi.handler.rosterPresenceHandler, 'jabber:client', 'presence');
		
		// Add message listener
		StropheBackend.connection.addHandler(xmppApi.handler.broadcastMessageHandler, 'jabber:client', 'message');
		StropheBackend.connection.addHandler(xmppApi.handler.chatMessageHandler, 'jabber:client', 'message', 'chat');
		StropheBackend.connection.addHandler(xmppApi.handler.groupMessageHandler, 'jabber:client', 'message', 'groupchat');
		StropheBackend.connection.addHandler(xmppApi.handler.groupNotificationMessageHandler, 'http://jabber.org/protocol/muc#user', 'message');
		StropheBackend.connection.addHandler(xmppApi.handler.buzzHandler, 'urn:xmpp:attention:0', 'message');
		StropheBackend.connection.addHandler(xmppApi.handler.conferenceInvitationHandler, 'jabber:x:conference', 'message');
		
		// Workgroup packet handler need to used in complex conditional
		// TODO: refactor this workgroupIQResultHandler2 !
		StropheBackend.connection.addHandler(xmppApi.handler.workgroupQueuePresenceHandler, 'http://jabber.org/protocol/workgroup', 'presence');
		StropheBackend.connection.addHandler(xmppApi.handler.workgroupIQResultHandler, 'http://jabber.org/protocol/workgroup', 'iq');
		StropheBackend.connection.addHandler(xmppApi.handler.workgroupIQResultHandler, 'http://jivesoftware.com/protocol/workgroup', 'iq');
		StropheBackend.connection.addHandler(xmppApi.handler.wgchatProviderResultHandler, 'http://wgchat.com/providers', 'iq', 'result');
		
		// Add global iq handler, that propagate packet by its id
		StropheBackend.connection.addHandler(xmppApi.handler.globalIQHandler, null, 'iq');
	}
};
