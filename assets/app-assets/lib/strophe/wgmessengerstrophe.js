var StropheBackend = {
	boshUrl: Messenger_GLOBAL.server_uri,
    xmppDomain: Messenger_GLOBAL.xmpp_domain,
    connection: null,
    isWorkgroupAvailable: false,
    isAttachmentFailed: true,
    resource: Messenger_GLOBAL.app_version,
    
    connectedJid: '',
    username: '',
    workgroup: '',
    password: '',
    workgroupFullJid: '',
    timeout: 90,
    pingInterval: 5 * 60 * 1000,
    maxAttachRetry: 5,
    attachRetry: 0,
    isAttaching: false,
    attachTimeoutHandler: null,
    pingerTimer: null,
    
    packetQueue: [],
    
    _initConnection: function() {
    	StropheBackend.connectedJid = StropheBackend.connection.jid;
    	
    	// Add ping request handler, to make this client stay connected
        StropheBackend.connection.addHandler(StropheBackend.handlePingIq, 'urn:xmpp:ping', 'iq');
        StropheBackend.connection.addHandler(StropheBackend.handleRidUpdate);
        
       	// Debugging
       	StropheBackend.connection.addHandler(StropheBackend.onPacketLog);
       	
       	StropheBackend.initPinger();
		StropheBackend.connection.flush();
		
		StropheBackend.attachRetry = 0;
		StropheBackend.isAttaching = false;
    },
    
    _attachCheckHandler: function(status, msg, callback, scope) {
		// console.error('ATTACHING SUCCESS!');
		clearTimeout(StropheBackend.attachTimeoutHandler);
		
		StropheBackend.attachRetry = 0;
		StropheBackend.isAttaching = false;
		
		StropheBackend._initConnection();
		callback.call(scope, Strophe.Status.ATTACHED, msg);
		
		return false;
	},
    
    _internalConnectionCallback: function(status, msg, callback, scope) {
    	// console.log('Internal callback: ' + status + ' - ' + msg);
    	
    	if (status === Strophe.Status.CONNECTED) {
    		StropheBackend._initConnection();
    	} else if (status === Strophe.Status.ATTACHED) {
    		// Test attachment success / not
    		clearTimeout(StropheBackend.attachTimeoutHandler);
    		StropheBackend.attachTimeoutHandler = setTimeout(function() {
    			if (StropheBackend.isAttaching) {
    				// console.error('ATTA TIMEOUT');
					StropheBackend.connection.deleteHandler(StropheBackend._attachCheckHandler);
					
					var raw = JSON.parse($.cookie(Strophe.getNodeFromJid(StropheBackend.connectedJid)));
    				// console.error(raw);
    				
		    		var jid = raw.jid;
					var sid = raw.sid;
					var rid = raw.rid;
					
					// Try to increment RID
					StropheBackend.handleRidUpdate(null, jid, sid, rid + 1);
					
					StropheBackend.attachConnection(callback, scope);
				}
			}, 3000);
			
			// var id = StropheBackend.getUniqueId();
    		StropheBackend.connection.addHandler(function() {
    			StropheBackend._attachCheckHandler(status, msg, callback, scope);
    		}, 'jabber:client', 'iq', 'result');
    		StropheBackend.sendPing();
    	}
    	
    	// If is attaching, surpress the DISCONNECTING / DICONNECTED status
    	if (StropheBackend.isAttaching && (status === Strophe.Status.DISCONNECTING || status === Strophe.Status.DISCONNECTED)) {
    		return;
    	}
    	
    	if (callback && status !== Strophe.Status.ATTACHED) {
    		if (!scope) {
    			scope = this;
    		}
    		
    		callback.call(scope, status, msg);
    	}
    },
    
    attachConnection: function(callback, scope) {	
    	if (StropheBackend.connection) {
    		if (StropheBackend.attachRetry > StropheBackend.maxAttachRetry) {
    			callback.call(scope, 99, 'attachment failed');
    			
    			return;
    		}
    		
    		StropheBackend.attachRetry++;
    		StropheBackend.isAttaching = true;
    		// console.error('Attach retry: ' + StropheBackend.attachRetry);
    		
    		var raw = JSON.parse($.cookie(Strophe.getNodeFromJid(StropheBackend.connectedJid)));
    		
    		var jid = raw.jid;
			var sid = raw.sid;
			var rid = raw.rid;
			
			StropheBackend.connection.reset();
			StropheBackend.connection = new Strophe.Connection(StropheBackend.boshUrl);
    		StropheBackend.connection.attach(jid, sid, rid, function(status, msg) {
    			StropheBackend._internalConnectionCallback.call(StropheBackend, status, msg, callback, scope)
    		}, StropheBackend.timeout);
    	}
    },
    
    openConnection: function(user, password, callback, scope) {
    	StropheBackend.closeConnection();
    	
        StropheBackend.connection = new Strophe.Connection(StropheBackend.boshUrl);
        StropheBackend.connection.connect(user + '@' + StropheBackend.xmppDomain + '/' + StropheBackend.resource, 
        	password, function(status, msg) {
        		if (status === Strophe.Status.CONNECTED) { 
        			StropheBackend.password = password;
        		}
    			
    			StropheBackend._internalConnectionCallback.call(StropheBackend, status, msg, callback, scope)
    		}, StropheBackend.timeout);
    },
    
    closeConnection: function(reason) {
        if (StropheBackend.connection !== null) {
            StropheBackend.connection.disconnect(reason);
            
            StropheBackend.connection = null;
        }
    },
    
    handlePingIq: function(packet) {
    	var $packet = $(packet);
    	var from 	= $packet.attr('from');
    	var to		= $packet.attr('to');
    	var id		= $packet.attr('id');
    	
    	var result = $iq({
            from: to,
            to: from,
            type: 'result',
            id: id
        });
        
        StropheBackend.send(result);
    	
    	return true;
    },
    
    onPacketLog: function(packet) {
        console.log('LOG Packet: ' + (new Date()));
        console.log(packet);
        
        return true;
    },
    
    /**
     * @param packet
     * @param force boolean force to send packet!
     */
    send: function(packet, force) {
    	if (!force && StropheBackend.isAttaching) {
    		return;
    	}
    	
    	if (StropheBackend.connection !== null) {
    		console.log('OUT: ' + packet.toString());
    		StropheBackend.connection.send(packet);
    	} else {
    		throw 'No connection exist.';
    	}
    },
    
    handleRidUpdate: function(packet, jid, sid, rid) {    	
    	if (!jid) {
    		jid = StropheBackend.connection.jid;
    	}
    	
    	if (!sid) {
    		sid = StropheBackend.connection.sid;
    	}
    	
    	if (!rid) {
    		rid = StropheBackend.connection.rid;
    	}
    	
        StropheBackend.storeConnectionCookie(jid, sid, rid);
        
        return true;
    },
    
    storeConnectionCookie: function(jid, sid, rid) {
    	var expiry = 1;
    	var raw = {
    		jid: jid,
    		sid: sid,
    		rid: rid
    	};
    	
    	$.cookie(Strophe.getNodeFromJid(StropheBackend.connectedJid), JSON.stringify(raw), { expires: expiry, path: window.location.pathname });
    },
    
    resetConnectionCookie: function() {
    	$.cookie(Strophe.getNodeFromJid(StropheBackend.connectedJid), null);
    },
    
    sendPing: function(id) {
    	if (!id) {
    		id = StropheBackend.getUniqueId();
    	}
    	
    	var pingIq = $iq({
            type: 'get',
            id: id
        }).c('ping', {
            xmlns: 'urn:xmpp:ping'
        });
        
        StropheBackend.send(pingIq, true);
    },
    
    initPinger: function() {
    	if (StropheBackend.pingerTimer) {
    		clearInterval(StropheBackend.pingerTimer);
    	}
    	
    	StropheBackend.pingerTimer = setInterval(function () {
			if (StropheBackend.connection === null) {
				clearInterval(StropheBackend.pingerTimer);
				return;
			}
			
			StropheBackend.sendPing();
			
		}, StropheBackend.pingInterval);
	},
    
    getUniqueId: function() {
    	if (StropheBackend.connection !== null) {
    		return StropheBackend.connection.getUniqueId();
    	} else {
    		throw 'No connection exist.';
    	}
    }
};