/**
 * Global variable for WG Messenger Web
 */
WGMessenger_GLOBAL = {
	isWindowActive: true,
	isLoggingOut: true,
	isRelogin: false,
	
	features: {},
	notificationCount: 0,
	defaultPageTitle: 'WG Messenger',
	autoLoginInterval: 5000, // in milisecond
	
	isTutorialEnabled: true,
	
	// Fill new features with tutorialIds
	// tutorialIds in this array will be shown, even if the tutorial is disabled
	newFeatures: [],
    doneTutorials: [],
	
	// notifications list
	desktopNotifications: [],
	
	eventType: {
		xmpp: {
			xmppConnectionDetached: 'xmppConnectionDetached', 
			
			rosterResultReceived: 'rosterResultReceived',
			presenceChange: 'presenceChange',
			chatMessageReceived: 'chatMessageReceived',
			buzzReceived: 'buzzReceived',
			
			groupMessageReceived: 'groupMessageReceived',
			groupNotificationReceived: 'groupNotificationReceived',
			conferencePresenceChange: 'conferencePresenceChange',
			conferenceInvitationReceived: 'conferenceInvitationReceived',
			
			workgroupUserChatPresence: 'workgroupUserChatPresence',
			workgroupUserChatMessage: 'workgroupUserChatMessage',
			workgroupUserChatNotification: 'workgroupUserChatNotification',
			workgroupPresenceChange: 'workgroupPresenceChange',
			workgroupRosterPresenceChange: 'workgroupRosterPresenceChange',
			workgroupTransferOfferReceived: 'workgroupTransferOfferReceived',
			workgroupChatOfferReceived: 'workgroupChatOfferReceived',
			workgroupOfferRevokeReceived: 'workgroupOfferRevokeReceived',
			workgroupFormReceived: 'workgroupFormReceived',
			workgroupUserRoomInvitation: 'workgroupUserRoomInvitation',
			workgroupQueuePresence: 'workgroupQueuePresence',
			
			globalMacroResultReceived: 'globalMacroResultReceived',
			personalMacroResultReceived: 'personalMacroResultReceived',
			chatHistoryResultReceived: 'chatHistoryResultReceived',
			historyTranscriptReceived: 'historyTranscriptReceived',
			historyMetadataReceived: 'historyMetadataReceived',
			historyNoteReceived: 'historyNoteReceived',
			wgchatOnlineIMResultReceived: 'wgchatOnlineIMResultReceived',
			
			wgchatSlotNotificationReceived: 'wgchatSlotNotifReceived'
		},
		ui: {
			userReady: 'userReady',
			workgroupServiceConfirmed: 'workgroupServiceConfirmed',
			workgroupServiceError: 'workgroupServiceError',
			personalMacroQuerySend: 'personalMacroQuerySend',
			
			reattachSuccess: 'reattachSuccess',
			loginSuccess: 'uiloginSuccess',
			loginFailed: 'uiloginFailed',
			autoLogin: 'uiAutoLogin',
			
			userOfferReceived: 'userOfferReceived',
			transferOfferReceived: 'transferOfferReceived',
			
			queueTutorial: 'queueTutorial'
		}
	},
	
	messageBus: null,
	template: {
		chatAreaTemplate: null,
		selectableGridContentTemplate: null
	},
	chatMacroMenu: null,
	
	parseUTCDate: function(dateText) {
    	var date = Ext.Date.parse(dateText, "YmdTH:i:s");
				
		var offset = date.getTimezoneOffset() * -60000;
		date = new Date(date.getTime() + offset);
		
		return date;
	},
	
	/**
	 * Search a session ID within UserRequest and TransferRequest store,
	 * to determine active customer conversation
	 * 
	 * @param string sessionId a session ID to look up for
	 */
	getRequestRecord: function(sessionId) {
    	var userRequestStore = Ext.getStore('UserRequests');
    	var transferRequestStore = Ext.getStore('TransferRequests');
    	var record = null;
    	
    	try {
    		var conferenceServiceJid = WGMessenger_GLOBAL.features.conference.jid;
    	
	    	record = userRequestStore.findRecord('sessionId', sessionId);
	    	if (!record) {
	    		// Check session value in transfer request.
	    		record = transferRequestStore.findRecord('roomJid', sessionId + '@' + conferenceServiceJid);
	    	} 	
    	} catch (err) { }
    	
    	return record;
    },
    
    setStatusBarValid: function(statusText) {
    	var sb = Ext.getCmp('basic-statusbar');
    	if (sb) {
	    	sb.setStatus({
		        text: statusText,
		        iconCls: 'x-status-valid',
		        clear: true
		    });	
    	}
    },
    
    setStatusBarError: function(statusText) {
    	var sb = Ext.getCmp('basic-statusbar');
    	if (sb) {
	    	sb.setStatus({
		        text: statusText,
		        iconCls: 'x-status-error',
		        clear: true
		    });	
    	}
    },
    
    setStatusBarLoading: function(statusText) {
    	var sb = Ext.getCmp('basic-statusbar');
    	if (sb) {
	    	sb.showBusy(statusText);
    	}
    },
    
    clearStatusBar: function(statusText) {
    	var sb = Ext.getCmp('basic-statusbar');
    	if (sb) {
	    	sb.clearStatus();
    	}
    },
    
    /**
     * Add a / some number to notificationCount to change page title
     * same as the facebook when having notification count in page title
     * 
     * @param integer count a value to add to notificationCount, passing negative value will substract notification count
     */
    addPageTitleNotification: function(count) {
    	WGMessenger_GLOBAL.notificationCount += count;
    	
    	if (WGMessenger_GLOBAL.notificationCount <= 0) {
    		WGMessenger_GLOBAL.notificationCount = 0;
    		
    		document.title = WGMessenger_GLOBAL.defaultPageTitle;
    		WGMessenger_GLOBAL.setFaviconUnread(false);
    		
    		// Reset the desktop notifications
    		WGMessenger_GLOBAL.clearDesktopNotification();
    	} else {
    		document.title = '(' + WGMessenger_GLOBAL.notificationCount + ') ' + WGMessenger_GLOBAL.defaultPageTitle;
    		WGMessenger_GLOBAL.setFaviconUnread(true);
    	}
    },
    
    /**
     * Change the page favicon to indicate unread message
     * 
     * @param boolean unread
     */
    setFaviconUnread: function(unread) {
    	if (WGMessenger_GLOBAL.isTesting) {
    		return;
    	}
    	
    	var url = base_url + 'favicon.png'
    	
    	if (unread) {
    		url = base_url + 'favicon-msg.png'
    	}
    	
    	var canvas = document.createElement('canvas'),
	        ctx,
	        img = document.createElement('img'),
	        link = document.getElementById('favicon').cloneNode(true),
	        day = (new Date).getDate() + '';
	
	    if (canvas.getContext) {
	    	canvas.height = canvas.width = 16; // set the size
	    	ctx = canvas.getContext('2d');
	    	
	    	img.onload = function () { // once the image has loaded
	        	ctx.drawImage(this, 0, 0);
        		
        		link.href = canvas.toDataURL('image/png');
	        	
	        	document.body.appendChild(link);
	     	 };
	      	
	      	img.src = url;
	    }
    },
    
    /**
     * Show Chrome desktop notification to indicate message / request
     * *require Google Chrome to be functional
     * 
     * @param string title notification title
     * @param string text notification text
     * @param integer timeout (optional) notification fade out after certain timeout
     */
    showDesktopNotification: function(title, text, timeout) {
    	if (window.webkitNotifications && window.webkitNotifications.checkPermission() === 0) {
    		var notif = window.webkitNotifications.createNotification(
				base_url + '/assets/spark.ico', 
				title,
				text
			);
			
			notif.onclick = function(x) { window.focus(); this.cancel(); };
			notif.show();
			
			if (timeout) {
				Ext.defer(function() {
					notif.cancel();
				}, timeout);
			}
			
			WGMessenger_GLOBAL.desktopNotifications.push(notif);
			
			return notif;
    	}
    },
    
    clearDesktopNotification: function() {
    	while (WGMessenger_GLOBAL.desktopNotifications.length > 0) {
    		try {
    			var notif = WGMessenger_GLOBAL.desktopNotifications.shift();
    			if (notif) {
    				notif.cancel();
    			}
    		} catch (e) {}
    	}
    },
    
    /**
     * Perform application version check using HTML5 manifest (*.appcache) defined in index.html
     * If an update found, this function will automatically download recent files and prompt user
     * that an update is available.
     * 
     * Update can be seen after reloading the page when all cache files have been downloaded
     */
    checkMessengerUpdate: function() {
    	if (window.applicationCache) {
    		window.applicationCache.addEventListener('checking', function(e) {
    			console.log('Checking Update...');
    		});
    		
    		window.applicationCache.addEventListener('downloading', function(e) {
    			console.log('Downloading Update...');
    		});
    		
            window.applicationCache.addEventListener('updateready', function(e) {
            	console.log('Update Ready!');
            	
                if (window.applicationCache.status === window.applicationCache.UPDATEREADY) {
                    // Browser downloaded a new app cache.
                    // Swap it in and reload the page to get the new hotness.
                    window.applicationCache.swapCache();
                
                    Ext.defer(function() {
                    	var cofirmation = Ext.Msg.confirm(
							'Update Available', 
							'Update for WG Messenger is available, do you want to see it now?', 
							function(answer) {
								if (answer === 'yes') {
									window.location.reload();
								}
				    	});
                    }, 1000);    
                } else {
                    // Manifest didn't changed. Nothing new to server.
                }
            }, false);
        }
    },
    
    /**
     * Check whether current session is using a demo account
     * @param workgroupname string override workgroupname
     * @param username string override username
     */
    isDemoAccount: function(workgroupname, username) {
    	
    	var userNode = '';
    	if (workgroupname && username) {
    		userNode = username + '-' + workgroupname;
    	} else if (StropheBackend.connection) {
    		userNode = Strophe.getNodeFromJid(StropheBackend.connection.jid);
    	} else if (StropheBackend.connectedJid)  {
    		userNode = StropheBackend.connectedJid;
    	} else {
    		return false;
    	}
		
		return (userNode.search(/-demo$/) !== -1);
    },
    
    setTimedLoading: function(component, text, timeout) {
    	if (!text) {
    		text = true;
    	}
    	
    	if (!timeout) {
    		// Default timeout for loading
    		timeout = 3000;
    	}
    	
    	if (timeout > -1) {
    		setTimeout(function() {
	    		component.setLoading(false);
	    	}, timeout);
    	}
	    
    	return component.setLoading(text);
    }
}

// Defining sound manager
soundManager.url = base_url + 'assets/app-assets/lib/soundmanager/swf/';
soundManager.onready(function() {
	soundManager.createSound({
		id: 'incomingchat',
		url: base_url + 'assets/app-assets/sounds/incoming.wav'
	});
	soundManager.createSound({
		id: 'chatrequest',
		url: base_url + 'assets/app-assets/sounds/chat_request.wav'
	});
	soundManager.createSound({
		id: 'buzz',
		url: base_url + 'assets/app-assets/sounds/bell.wav'
	});
});

Ext.Loader.setConfig({
	enabled: true,
	paths: {
		'Ext': base_url + '/assets/ext-4.0-copy/src',
		'Ext.ux': base_url + '/assets/ext-4.0-copy/examples/ux',
		'WG': base_url + '/assets/app'
	},
	// Disable the cache if you want to use breakpoints
	// disableCaching: false
});

/**
 * Add required class to function below
 * to avoid synchronous autoload
 */
Ext.require([
	'Ext.Img',
	'Ext.form.Panel',
	'Ext.form.field.ComboBox',
	'Ext.form.field.Checkbox',
	'Ext.form.field.VTypes',
	'Ext.grid.feature.Grouping',
	'Ext.selection.CheckboxModel',
	'Ext.util.Observable',
	'Ext.container.Viewport',
	'Ext.layout.container.Border',
	'Ext.layout.container.Accordion',
	'Ext.EventManager',
	'Ext.Date',
	'Ext.window.MessageBox',
	'Ext.ux.statusbar.StatusBar',
	'Ext.ux.TabScrollerMenu',
	'Ext.ux.Spotlight',
	'Ext.view.Table',
	'Ext.ux.statusbar.StatusBar',
	'Ext.ux.Spotlight',
	
	'WG.view.MainPanel',
	'WG.view.LoginPanel',
	'WG.controller.Main',
	'WG.controller.Workgroup',
	'WG.controller.UserPreferences',
	'WG.controller.Chats',
	'WG.controller.ConferenceChats',
	'WG.controller.WorkgroupChats',
	'WG.controller.Tutorial',
	'WG.controller.workgroup.ChatHistories',
	'WG.controller.workgroup.ChatCategories',
	'WG.controller.workgroup.ChatTransfer',
	'WG.controller.workgroup.PersonalMacros',
	'WG.controller.workgroup.OfferProgress',
	'WG.controller.workgroup.InitiateMessage',
	'WG.controller.workgroup.ChatTransfer',
	'WG.model.Metadata',
	'WG.model.UserRequest',
	'WG.view.workgroup.ChatMacroMenu'
]);

var WGMessenger = {};
Ext.onReady(function() {
	
	// Prepare selectable grid text
	Ext.override(Ext.view.Table, {
		afterRender: function() {
		    var me = this;
		
		    me.callParent();
		    me.mon(me.el, {
		        scroll: me.fireBodyScroll,
		        scope: me
		    });
		    if (!me.featuresMC &&
		        (me.featuresMC.findIndex('ftype', 'unselectable') >= 0)) {
		            me.el.unselectable();
		        }
		
		        me.attachEventsForFeatures();
		    }
		}
	);
	
	// Add advanced Vtype
	// Add the additional 'advanced' VTypes
    Ext.apply(Ext.form.field.VTypes, {
        daterange: function(val, field) {
            var date = field.parseDate(val);

            if (!date) {
                return false;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = field.up('form').down('#' + field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = field.up('form').down('#' + field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }
            
            /*
             * Always return true since we're only using this vtype to set the
             * min/max allowed values (these are tested for after the vtype test)
             */
            return true;
        },

        daterangeText: 'Start date must be less than end date',

        password: function(val, field) {
            if (field.initialPassField) {
                var pwd = field.up('form').down('#' + field.initialPassField);
                return (val == pwd.getValue());
            }
            return true;
        },

        passwordText: 'Passwords do not match'
    });
	
	WGMessenger_GLOBAL.messageBus = new Ext.util.Observable();
	WGMessenger_GLOBAL.template.chatAreaTemplate = new Ext.XTemplate(
		'<tpl for=".">',
			'<span class="wgchat-message-item">',
			'<tpl if="this.isNotif(notif) === false">',
				'<span class="wgchat-timestamp {me}">({time})</span>',
				'<span class="wgchat-name {me}">{name}:</span>',
				'<span class="wgchat-body">{[this.formatUrl(values.body)]}</span>',
			'</tpl>',
			'<tpl if="this.isNotif(notif) === true">',
				'<span class="wgchat-notif">{notif}</span>',
			'</tpl>',
			'</span>',
		'</tpl>',
		{
			disableFormats: true,
			
			isNotif: function(notif) {
				return notif !== null;
			},
			
			// from: http://stackoverflow.com/questions/37684/how-to-replace-plain-urls-with-links
			formatUrl: function(text) {
				var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    			return text.replace(exp,"<a href=\"$1\" target=\"_blank\">$1</a>");
			}
		}
	);
	
	Ext.tip.QuickTipManager.init();
	
	$(window).focus(function() {
        WGMessenger_GLOBAL.isWindowActive = true;
    });

    $(window).blur(function() {
        WGMessenger_GLOBAL.isWindowActive = false;
    });
    
    // User exit confirmation
	function confirmExit() {
		if (WGMessenger_GLOBAL.isLoggingOut || WGMessenger_GLOBAL.isTesting) {
			return;
		}
		
		return 'Exiting WG Messenger will logoff your session';
	}
	function forceLogoff() {
		xmppApi.logout();
	}
	window.onbeforeunload = confirmExit;
	window.onunload = forceLogoff;
	
	WGMessenger_GLOBAL.checkMessengerUpdate();
});