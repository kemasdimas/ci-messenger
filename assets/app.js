Ext.application({
	name: 'WG',
	appFolder: 'app',
	
	controllers: [
		'Main',
		'UserPreferences',
		'Workgroup',
		'workgroup.PersonalMacros',
		'workgroup.ChatHistories',
		'workgroup.InitiateMessage',
		'workgroup.OfferProgress',
		'workgroup.ChatCategories',
		'workgroup.ChatTransfer',
        'Chats',
        'ConferenceChats',
        'WorkgroupChats',
        'Tutorial'
    ],
	
	launch: function() {
		Ext.create('Ext.container.Viewport', {
			id: 'main-viewport',
			layout: 'fit',
			
			items: [
				{ 
					xtype: 'panel',
					layout: 'fit',
					itemId: 'main-container',
					border: 0,
					
					items: [
						{
							xtype: 'loginpanel'
						}
					],
					
					bbar: Ext.create('Ext.ux.StatusBar', {
				        id: 'basic-statusbar',
				        
				        items: [
				        	{
				        		text: 'Report Issue',
				        		iconCls: 'wgchat-icon-alert',
				        		handler: function() {
				        			if (UserVoice) {
				        				UserVoice.showPopupWidget();	
				        			}
				        		}
				        	},
				        	'-',
				            StropheBackend.resource
				        ]
				    })
				}
			],
		});
		
		// Initiate chat macro menu for message template in workgroup chat
		WGMessenger_GLOBAL.chatMacroMenu = Ext.create('WG.view.workgroup.ChatMacroMenu');
		
		// Remove preloader
		var loadingMask = Ext.get('loading-mask');
	    var loading = Ext.get('loading');
	    
	    if (loading) {
	    	//  Hide loading message
	   		loading.fadeOut({ duration: 0.5, remove: true });
	    }
	    
	    if (loadingMask) {
	    	//  Hide loading mask
		    loadingMask.fadeOut({ duration: 0.5, remove: true });
	    }
		
	    // Check desktop notification permission
	    if (window.webkitNotifications) {
    		if (window.webkitNotifications.checkPermission() != 0) {
    			Ext.defer(function() {
    				Ext.Msg.alert('Request Permission', 'Please allow desktop notification permission', function() {	
    					window.webkitNotifications.requestPermission();
    				});	
    			}, 1000);
    		}
    	} else {
    		// No desktop notification support
    		// Warn user
    		Ext.example.msg('No Desktop Notification', 
    			'Harap gunakan Google Chrome untuk menikmati fitur "Desktop Notification", ' + 
    			'agar Anda tetap dapat memantau pesan yang masuk ketika melakukan aktifitas lain' + 
    			'<span class="msg-right">(klik pop up ini untuk melanjutkan)</span>', -1);	
    	}
	}
});
