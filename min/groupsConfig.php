<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    'app_css' => array(
    	'//messengerci/assets/css/ext/resources/css/ext-all.css', 
    	'//messengerci/assets/css/ext/ux/statusbar/css/statusbar.css', 
    	'//messengerci/assets/css/ext/tabs/tabs-adv.css', 
    	'//messengerci/assets/css/ext/ux/css/TabScrollerMenu.css', 
    	'//messengerci/assets/css/ext/shared/example.css', 
    	'//messengerci/assets/css/app.css', 
    	'//messengerci/assets/css/preloader.css'
    ),
    'app_js' => array(
    	'//messengerci/assets/app-assets/lib/jquery/jquery-1.7.1.min.js', 
    	'//messengerci/assets/app-assets/lib/jquery/jquery.cookie.js', 
    	'//messengerci/assets/app-assets/lib/soundmanager/script/soundmanager2-nodebug-jsmin.js', 
    	'//messengerci/assets/app-assets/lib/strophe/strophe.min.js', 
    	'//messengerci/assets/app-assets/lib/strophe/wgmessengerstrophe.js', 
    	'//messengerci/assets/ext-4.0-copy/ext.js', 
    	'//messengerci/assets/app-assets/lib/ext/examples.js', 
    	'//messengerci/assets/app-precond.js', 
    	'//messengerci/assets/app-all.js', 
    	'//messengerci/assets/app-assets/lib/xmppApi.js'
    ),
);