<?php 
/**
 * WG Messenger configuration file
 */

$config['messenger_version'] 		= 'WG Messenger Web 1.0.3a';
$config['messenger_xmpp_domain']	= 'xmpp.wgchat.com';

// TODO: value(s) below must be taken from database!
$config['messenger_bosh_url'] = 'http://xmpp.wgchat.com:8080/http-bind/';