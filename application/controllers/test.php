<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// Comment out a line below to enable testing
		/*
		die('NOT ALLOWED!');
		// */
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = array(
			// 'server_uri'	=> 'http://localhost/bosh2',
			// 'server_uri'	=> 'http://kemass-macbook-air.local:7070/http-bind/',
			'server_uri'	=> 'http://kemass-macbook-air.local:8080/http-bind/',
			'xmpp_domain'	=> 'kemass-macbook-air.local',
			'app_version'	=> 'WG Messenger Web TEST'
		);
		
		$this->load->view('messenger_test', $data);
	}
	
	public function testlive()
	{
		$data = array(
			'server_uri'	=> 'http://kemass-macbook-air.local:8080/http-bind/',
			'xmpp_domain'	=> 'xmpp.wgchat.com',
			'app_version'	=> 'WG Messenger Web LIVETEST'
		);
		
		$this->load->view('messenger_staging', $data);
	}
	
	public function strophe()
	{
		$data = array(
			// 'server_uri'	=> 'http://localhost/bosh2',
			// 'server_uri'	=> 'http://kemass-macbook-air.local:7070/http-bind/',
			'server_uri'	=> 'http://kemass-macbook-air.local:8080/http-bind/',
			'xmpp_domain'	=> 'kemass-macbook-air.local',
			'app_version'	=> 'WG Messenger Web TEST'
		);
		
		$this->load->view('strophe_test', $data);
	}
	
	public function production()
	{
		$data = array(
			'server_uri'	=> 'http://nde1.wgchat.com:5280/http-bind/',
			'xmpp_domain'	=> 'xmpp.wgchat.com',
			'app_version'	=> 'WG Messenger Web PRODUCTION2'
		);
		
		$this->load->view('messenger_production', $data);
	}
	
	public function demo($username = 'cs1')
	{
		$data = array(
			'server_uri'	=> 'http://kemass-macbook-air.local:8080/http-bind/',
			'xmpp_domain'	=> 'xmpp.wgchat.com',
			'app_version'	=> 'WG Messenger Web LIVE DEMO',
			
			'workgroup_demo'	=> 'demo',
			'username_demo'		=> $username
		);
		
		$this->load->view('messenger_staging', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */