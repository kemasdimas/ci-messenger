<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messenger extends CI_Controller {
	public function index()
	{
		$data = array(
			'server_uri'	=> $this->config->item('messenger_bosh_url'),
			'xmpp_domain'	=> $this->config->item('messenger_xmpp_domain'),
			'app_version'	=> $this->config->item('messenger_version')
		);
		
		$this->load->view('messenger_production', $data);
	}
	
	public function demo($username = 'cs1')
	{
		$data = array(
			'server_uri'	=> $this->config->item('messenger_bosh_url'),
			'xmpp_domain'	=> $this->config->item('messenger_xmpp_domain'),
			'app_version'	=> $this->config->item('messenger_version'),
			
			'workgroup_demo'	=> 'demo',
			'username_demo'		=> $username
		);
		
		$this->load->view('messenger_demo', $data);
	}
}