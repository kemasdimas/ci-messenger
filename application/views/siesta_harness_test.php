<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/ext/resources/css/ext-all.css">
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/siesta/resources/css/siesta-all.css">
		
	    <script type="text/javascript" src="<?php echo site_url('assets'); ?>/ext-4.0/ext-all-debug.js"></script>
	    
	    <!-- TEST SUITE JAVASCRIPT -->
	    <script type="text/javascript" src="<?php echo site_url('assets'); ?>/siesta/siesta-all.js"></script>
        
        <script type="text/javascript" src="<?php echo site_url('assets'); ?>/siesta/mytest/test-harness.js"></script>
	</head>
	<body>
	</body>
</html>