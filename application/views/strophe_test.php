<!DOCTYPE html>
<html lang="en">
	<head>
	</head>
	<body>
		<script type="text/javascript">
	    	var base_url 	= '<?php echo base_url(); ?>';
	    	var Messenger_GLOBAL = {
	    		xmpp_domain: '<?php echo $xmpp_domain; ?>',
	    		server_uri: '<?php echo $server_uri; ?>',
	    		app_version: '<?php echo $app_version; ?>'
	    	};
		</script>
		
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/jquery/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/jquery/jquery.cookie.js"></script>
		
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/strophe/strophe.js"></script>
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/strophe/wgmessengerstrophe.js"></script>
		
		<script type="text/javascript">
			var increaseRid = function() {
				StropheBackend.connection.rid++;
				console.log('RID: ' + StropheBackend.connection.rid);
			};
			
			var decreaseRid = function() {
				StropheBackend.connection.rid--;
				console.log('RID: ' + StropheBackend.connection.rid);
			};
			
			var callback = function(status, msg) {
				console.log('CONNECTION STATUS: ' + status + ' - ' + msg);
				
				if (status == Strophe.Status.ATTACHED) {
					
					console.log('Attached Successfully!');
					StropheBackend.sendPing();
					
					var rosterQuery = $iq({
			            type: 'get',
			            id: StropheBackend.connection.getUniqueId()
			        }).c('query', {
			            xmlns: 'jabber:iq:roster'
			        });
			        
			        StropheBackend.send(rosterQuery);
				} else if (status == Strophe.Status.CONNECTED) {
					
					StropheBackend.sendPing();
				} else if (status == 2) {
					
					console.log('CONNFAIL');
					console.error("Connection CONNFAIL: \nRID: " + Strophe.connection.rid);
				} else if (status == 6) {
					
					console.log('DISCONNECTED');
					console.error('Stored RID: ' + $.cookie(Strophe.getNodeFromJid(StropheBackend.connectedJid)));
				} else if (status == 99) {
					console.error('ATTACHMENT FAILED!!!');
				}
			};
		
			var attachConnection = function() {
				StropheBackend.attachConnection(callback);
			};
		
			var username = 'test1';
			var password = 'test1';
			
			StropheBackend.timeout = 30;
			StropheBackend.openConnection(username, password, callback);
		</script>
		
		<a href="javascript:void(0)" onclick="increaseRid();">Increase RID</a>
		<a href="javascript:void(0)" onclick="decreaseRid();">Decrease RID</a>
		<a href="javascript:void(0)" onclick="StropheBackend.sendPing();">Send PING!</a>
		<a href="javascript:void(0)" onclick="StropheBackend.handleRidUpdate();">Update Cookie</a>
		<a href="javascript:void(0)" onclick="attachConnection();">Attach Connection</a>
	</body>
</html>