<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		
		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<title>WG Messenger</title>
		
		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link id="favicon" rel="icon" type="image/png" href="<?php echo site_url('favicon.png'); ?>" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('min/g=app_css'); ?>">
	</head>
	<body>
		<div id="loading-mask"></div>
	    <div id="loading">
	    	<span id="loading-message">Loading, please wait...</span>
	    </div>
	    
	    <script type="text/javascript">
	    	var base_url 	= '<?php echo base_url(); ?>';
	    	var Messenger_GLOBAL = {
	    		xmpp_domain: '<?php echo $xmpp_domain; ?>',
	    		server_uri: '<?php echo $server_uri; ?>',
	    		app_version: '<?php echo $app_version; ?>'
	    	};
	    	
	    	<?php if (isset($workgroup_demo)) : ?>
    		var demo_GLOBAL = {
    			workgroup_demo: '<?php echo $workgroup_demo; ?>',
	    		username_demo: '<?php echo $username_demo; ?>'
    		};
	    	<?php endif; ?>
	    	
		    setPreloaderText = function(text) {
		    	document.getElementById('loading-message').innerHTML = text;
		    };
		     
		    setPreloaderText('Loading Core API...');
		</script>
		<script type="text/javascript" src="<?php echo site_url('min/g=app_js'); ?>"></script>
	</body>
</html>