<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		
		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<title>WG Messenger</title>
		
		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link id="favicon" rel="icon" type="image/png" href="<?php echo site_url('favicon.png'); ?>" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/ext/resources/css/ext-all.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/ext/ux/statusbar/css/statusbar.css">
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/ext/tabs/tabs-adv.css">
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/ext/ux/css/TabScrollerMenu.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/ext/shared/example.css">
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/app.css">
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/preloader.css">
	</head>
	<body>
		<div id="loading-mask"></div>
	    <div id="loading">
	    	<span id="loading-message">Loading, please wait...</span>
	    </div>
	    
	    <script type="text/javascript">
	    	var base_url 	= '<?php echo base_url(); ?>';
	    	var Messenger_GLOBAL = {
	    		xmpp_domain: '<?php echo $xmpp_domain; ?>',
	    		server_uri: '<?php echo $server_uri; ?>',
	    		app_version: '<?php echo $app_version; ?>'
	    	};
	    	
		    setPreloaderText = function(text) {
		    	document.getElementById('loading-message').innerHTML = text;
		    };
		     
		    setPreloaderText('Loading Core API...');
		</script>
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/jquery/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/soundmanager/script/soundmanager2-nodebug-jsmin.js"></script>
		
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/strophe/strophe.js"></script>
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/strophe/wgmessengerstrophe.js"></script>
		
	    <script type="text/javascript" src="<?php echo site_url('assets'); ?>/ext-4.0-copy/ext-dev.js"></script>
	    <script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/ext/examples.js"></script>
	    
	    <script type="text/javascript">
		     setPreloaderText('Loading Application...');
		</script>
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-precond-test.js"></script>
		<script type="text/javascript" src="<?php echo site_url('assets'); ?>/app.js"></script>
	    <script type="text/javascript" src="<?php echo site_url('assets'); ?>/app-assets/lib/xmppApi.js"></script>
	</body>
</html>